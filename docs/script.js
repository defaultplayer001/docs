'use strict';

// The ReadTheDocs theme auto scrolls to the first expanded nav item,
// which is just plain bad UX, so we're monkey wrenching it.
const scrollFunc = Element.prototype.scrollIntoView;

Element.prototype.scrollIntoView = () => {
    console.log('Prevent scrolling on page load');
};

document.addEventListener('DOMContentLoaded', () => {
    setTimeout(() => {
        Element.prototype.scrollIntoView = scrollFunc;
    }, 1000);
});
