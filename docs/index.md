# Welcome

This a comprehensive documentation on modding Deus Ex.

The information on this site is largely a consolidation of these excellent resources:  

- [Beyond Unreal](https://beyondunrealwiki.github.io)  
- [Tactical Ops Archive](https://tactical-ops.eu/info-unrealed-guide.php)  
- [Tack's Deus Ex Lab](http://offtopicproductions.com/tacks)   

And these people who helped build this particular documentation:  
- Artifechs  
- Defaultmom001  
- Greasel  
- Rubber Ducky WCCC  
- TheAstroPath  


## Getting started
- [Installing the Deus Ex SDK](guides/installing.md)
- [Creating a custom package](guides/custom-packages.md)
- [Creating a custom character](guides/custom-characters.md)


## Videos
- [Unreal Editor Beginner's Guide](https://youtube.com/watch?v=ok3eDssid7s) by Shivaxi  
