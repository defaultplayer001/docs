# Installing the Deus Ex SDK

## Linux

### Wine

Throughout these guides, whenever running an `.exe` is mentioned, run it through Wine with these environment variables set:

    export WINEPREFIX="$HOME/.deus-ex-sdk"
    export WINEARCH=win32

This means that your `C:\DeusEx` folder will be located at `~/.deus-ex-sdk/drive_c/DeusEx`

Create the Wine prefix and install the necessary dependencies:

    wine wineboot
    winetricks mfc42


#### Known issues

- When opening UnrealEd, there might be some floating black windows, which can't initially be closed. To close these windows, you need to invoke them from the menu, click inside them and then click close.  
- When UnrealEd crashes, it can take the entire desktop environment down with it. Save often.  
- Fields in the properties dialog sometimes have `Use` and `Clear` buttons, but they might be invisible.  


### Virtualised environment

When using virtualisation, such as Gnome Boxes or VirtualBox, the recommended setup is a Windows 7 image with mouse intergration turned off.  


## Install Deus Ex

Since it's the SDK's preferred directory, install Deus Ex at `C:\DeusEx`.

If you value your sanity, download <a href="/downloads/DeusExe-v8.1.zip" download>Marijn Kentie's launcher</a> and <a href="/downloads/dxglr20.zip" download>Chris Dohnal's OpenGL renderer</a> and unzip them in the `C:\DeusEx\System` folder. Select the OpenGL renderer the first time the game starts.  

If the game is too dark, you can open the console by pressing `T`, removing the `Say` prompt and type `preferences`. Then under `Rendering > OpenGL Support` set `OneXBlending` to `False`. 


## Install the SDK

Install/extract the SDK into `C:\DeusEx` as well. There are several options to choose from. As they all have benefits and shortcomings, it's fairly common to install all 3 of them and switch as needed.

### Official SDK

<a href="/downloads/DeusExSDK1112f.exe" download>DeusExSDK1112f.exe</a>

Installs to `DeusEx/System/UnrealEd.exe`  

The good:  
- It's the most compatible option. Everything works as intended.

The bad:  
- Frequent crashes  
- Wine/Linux: All viewports have to be floating windows. If they're docked, mouse movement goes haywire.  
- Default file extension in the file browser is .unr, should be .dx  


### DX editing pack

<a href="/downloads/dxeditingpack_2_2_full_6218.exe" download>dxeditingpack_2_2_full_6218.exe</a>

Installs to `DeusEx/Ued2/UnrealEd.exe`  

The good:  
- Stable, doesn't crash often  
- Viewports work  
- Browser works  

The bad:  
- Some text strings are in Russian  
- Some windows (e.g. "Advanced Options") don't show anything  
- Some actors are invisible  


### Patched UnrealEd 2.2 from OldUnreal

<a href="/downloads/UED2_2_For_DeusEx.7z" download>UED2_2_For_DeusEx.7z</a>

Installs to `DeusEx/UED22/unrealed.exe`  

The good:  
- Stable, doesn't crash often  
- Viewports work  
- All windows and dialogs work  

The bad:  
- Browser doesn't work  
- Some actors are invisible  
