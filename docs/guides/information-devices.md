# Information devices

In order to display text using data cubes, books and newspapers, an external text resource is needed.


## Prerequisites  

- [Custom packages](custom-packages.md)


## Create a folder

Create a `Text` folder inside your package, like so:

    DeusEx
    └─ MyPackage
       └─ Text


## Create a text file

Create a new text file called `16_MyText.txt` inside your `Text` folder. The 16 is your [mission number](mission-numbers.md) 

Text has a few HTML-like formatting tags available:

| | |
| --- | --- |
| `<P>` | A paragraph |
| `<PLAYERNAME>` | The player's chosen real name |
| `<JC>` | Centered text |
| `<DC=0-255,0-255,0-255>` | Text colour |
| `<COMMENT>` | A comment that will not be visible in-game |

A common practice is to close all tags except the `<P>` tag, like so:

    <P><JC>This is a centered title</JC>
    <P>This is a longer sentence with a <DC=255,0,0>red</DC> word in it
    <COMMENT>This text will not be visible in-game</COMMENT>


## Create an import file

Create a new file called `TextImport.uc` inside your `DeusEx/MyPackage/Classes` folder with this content:

    class TextImport expands Object abstract;

    #exec DEUSEXTEXT IMPORT FILE=Text\16_MyText.txt


## Rebuild your package

Delete the `DeusEx/System/MyPackage.u` file if it's present and run this command from within the `DeusEx/System` folder:

    UCC.exe make


## Add the information device to your map

1. Open UnrealEd.  
2. Bring up the actor browser from `View > Actor Class Browser`.  
3. Navigate to the data cube, book or newspaper you want in the `Decoration > DeusExDecoration > InformationDevices` section.  
4. Right click where you want to place the actor and select `Add <actor> here`.  
5. Double click the actor (or right click and select `Properties`).  
6. Expand the `InformationDevices` section.  
7. In the `TextPackage` field, type `MyPackage`. 
8. In the `textTag` field, type `16_MyText`. 

!!! Note

    The `imageClass` field refers to a `DataVault` image, which will be transferred to the player upon reading. To use an existing image, you can click on one from the actor browser in the `Inventory > DataVaultImage` section, and then click the `Use` button in the `imageClass` field of your information device.
