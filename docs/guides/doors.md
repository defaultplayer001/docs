# Doors

A door in Deus Ex is an Unreal "mover" brush with a few extra features.


## Prerequisites  
- [Brushes](brushes.md)  
- [Textures](textures.md)  


## Create the template
1. Right click the `Cube` brush to bring up the cube builder menu.  
2. Type in height, width and breadth as 128x64x4.  
3. Click `Build`.  
4. Move the cube brush to a place in your map where it doesn't overlap with anything.  
5. If you're making a swinging door, move the pivot point to where the hinge would be.  
6. Click `Add`.  
7. Apply the textures you want to the brush.  

## Create the selection box
1. Create a new selection box with the `Cube` button.  
2. Make sure it encompasses the template door.
3. Move the pivot point of the selection box to there you want the pivot point of the door.
4. Click `Intersect`

You can safely delete the template brush now, if you like.  

## Create the mover brush
1. Move the new selection box to where you want the door to be.  
2. Right click the `Mover` brush and select `DeusExMover`. 
3. Move the selection box out of the way to reveal the newly created `Mover` brush.  

The mover brush will not be rendered in the editor, so all you see is the purple outline. It will show up like normal once in-game, though.

## Set up the keyframes  
1. Right click the mover brush and select `Movers > Key 1`.  
2. Rotate/move the door into the desired rotation/position.  
3. Right click the mover brush again and select `Movers > Key 0 (Base)`. 

## Set up the door properties

!!! Note

    Under Linux/Wine, the `Use` and `Clear` buttons next to the property fields might be invisible.

1. Open the properties for the mover brush, either by double clicking, pressing F4, or by right clicking and selecting `Properties`.  
2. Expand the `DeusExMover` section and change `bIsDoor` to `True`.  
3. Navigate to the sounds you want the door to make in the `Sound Browser` under the `MoverSFX`. You can double click sounds to preview them.    
4. Expand the `MoverSounds` section under the mover brush properties.  
5. Select the opening sound you want in the browser and click `Use` next to the appropriate field in the mover brush properties.  
6. If your door moves between light and dark areas, you might want to switch on the `Mover > bDynamicLightMover` field.  
7. It might also be a good idea to set `Mover > MoverEncroachType` to `ME_IgnoreWhenEncroach` to let the door pass through the player when opened.   

## Finishing up
The door should be ready now. Build BSP and lighting and press `Play Map!` to check it out. 
