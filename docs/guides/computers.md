# Computers

This guide will cover adding personal computers, security consoles and public terminals to your map. You will find these actors in the `Actor Class Browser` under `Decoration > DeusExDecoration > ElectronicDevices > Computers`.  


## Prerequisites
- [Actors](actors.md)


## Properties

    Field name                      Description
    ──────────────────────────────────────────────────────────────────────
    Computers
    ├─ ComputerNode ··············· The node¹ this computer belongs to
    ├─ lockoutDelay ··············· Seconds to lock out the player after a failed hack attempt
    ├─ nodeName ··················· ???
    ├─ specialOptions ············· Buttons that trigger actions
    │  ├─ bTriggerOnceOnly ········ Prevent retriggering the event
    │  ├─ Text ···················· Button text
    │  ├─ TriggerEvent ············ Name of the event to be triggered
    │  ├─ TriggerText ············· ???
    │  ├─ UnTriggerEvent ·········· ???
    │  └─ userName ················ User that triggered the event
    ├─ TextPackage ················ ???
    ├─ titleString ················ ???
    ├─ titleTexture ··············· ???
    └─ UserList ··················· List of accounts that can log in to this computer
       ├─ accessLevel ············· Hacking skill needed to access this account 
       ├─ Password ················ Account password
       └─ userName ················ Account username

¹ More about [nodes](#nodes)


## Personal computers

## Public terminals

## Security consoles
