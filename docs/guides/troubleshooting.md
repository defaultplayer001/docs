# Troubleshooting

## My game is too dark

1. Install <a href="../../assets/downloads/DeusExe-v8.1.zip" download>Marijn Kentie's launcher</a>.  
2. Unpack <a href="../../assets/downloads/dxglr20.zip" download>Chris Dohnal's OpenGL renderer</a> into your `DeusEx/System` folder.
3. Select the OpenGL renderer the first time the game starts (you can also change renderer in the game's settings).  
4. If it's still too dark, open the console by pressing `T`, remove the `Say` prompt and type `preferences`.  Then under `Rendering > OpenGL Support` set `OneXBlending` to `False`.  


## My conversation doesn't start when I interact with the NPC

1. Is the NPC's `BindName` the same as the `Owner` of the conversation? 
2. Are there any actors referenced by `BindName` in the conversation that are not present in the map, or more than 800 units away?  
3. Is your custom character's `BindName` something other that `JCDenton`?  


## My conversation is either silent or "hangs" and doesn't progress

Check out the [audio section](conversations.md#adding-audio-to-a-speech-event) of the conversations guide.  


## UCC cannot compile my conversation

1. Are any events that refer to labels missing their target label?
