# Classes

## DeusExPlayer

### Properties

Category            | Variable                              | Description     
------------------- | ------------------------------------- | ----------- 
Health              | `int` HealthHead
                    | `int` HealthTorso
                    | `int` HealthLegLeft
                    | `int` HealthLegRight
                    | `int` HealthArmLeft
                    | `int` HealthArmRight
Conversation        | `bool` bIsSpeaking                    | Are we speaking now?
                    | `bool` bWasSpeaking                   | Were we speaking last frame?
                    | `string` lastPhoneme                  | Phoneme last spoken
                    | `string` nextPhoneme                  | Phoneme to speak next
                    | `float` animTimer[4]                  | Misc. timers for ambient anims (blink, head, etc.)
                    | `String` BindName                     | Used to bind conversations
                    | `String` BarkBindName                 | Used to bind Barks!
                    | `String` FamiliarName                 | For display in Conversations
                    | `String` UnfamiliarName               | For display in Conversations
                    | `Object` ConListItems                 | List of ConListItems for this Actor
                    | `float` LastConEndTime                | Time when last conversation ended
                    | `float` ConStartInterval              | Amount of time required between two convos
Fire                | `bool` bOnFire
                    | `float` burnTimer
AI                  | `float` AIHorizontalFov               | Degrees
                    | `float` AspectRatio                   | Horizontal/vertical ratio
                    | `float` AngularResolution             | Degrees
                    | `float` MinAngularSize                | Tan(AngularResolution)^2
                    | `float` VisibilityThreshold           | Lowest visible brightness (0-1)
                    | `float` SmellThreshold                | Lowest smellable odor (0-1)
                    | `Name` Alliance                       | Alliance tag
                    | `Rotator` AIAddViewRotation           | Rotation added to view rotation for AICanSee()
                    | `float` VisUpdateTime
                    | `float` CurrentVisibility
                    | `float` LastVisibility
Detection           | `bool` bBlockSight                    | True if pawns can't see through this actor.
                    | `bool` bDetectable                    | True if this actor can be detected (by sight, sound, etc).
                    | `bool` bTransient                     | True if this actor should be destroyed when it goes into stasis
                    | `bool` bIgnore                        | True if this actor should be generally ignored; compliance is voluntary
Misc                | `float` LastRenderTime
                    | `float` DistanceFromPlayer
                    | `bool` bOwned
                    | `bool` bVisionImportant               | Added to make vision aug run faster. 
                    | `String` TruePlayerName
                    | `int` PlayerSkin

### Methods

Method              | Parameter                             | Description
------------------- | ------------------------------------- | -----------
Constructor         | `Pawn` Player
ClientMessage       | `string` msg                          | The message to be displayed
                    | `Name` type                           | The type of message (possible values ???)
                    | `bool` bBeep                          | ???
