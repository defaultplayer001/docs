# Textures

## Airfield

### Brick  
<figure><img src="Airfield/Brick/DrtyBrwnBlock.jpg" alt="DrtyBrwnBlock" loading="lazy"><figcaption>DrtyBrwnBlock</figcaption></figure>

### Concrete  
<figure><img src="Airfield/Concrete/AF_PaveArrow.jpg" alt="AF_PaveArrow" loading="lazy"><figcaption>AF_PaveArrow</figcaption></figure>
<figure><img src="Airfield/Concrete/AF_PaveLines.jpg" alt="AF_PaveLines" loading="lazy"><figcaption>AF_PaveLines</figcaption></figure>
<figure><img src="Airfield/Concrete/AF_PaveStripe.jpg" alt="AF_PaveStripe" loading="lazy"><figcaption>AF_PaveStripe</figcaption></figure>
<figure><img src="Airfield/Concrete/AF_interwall_A.jpg" alt="AF_interwall_A" loading="lazy"><figcaption>AF_interwall_A</figcaption></figure>
<figure><img src="Airfield/Concrete/AF_interwall_B.jpg" alt="AF_interwall_B" loading="lazy"><figcaption>AF_interwall_B</figcaption></figure>
<figure><img src="Airfield/Concrete/Street_A.jpg" alt="Street_A" loading="lazy"><figcaption>Street_A</figcaption></figure>
<figure><img src="Airfield/Concrete/Street_B.jpg" alt="Street_B" loading="lazy"><figcaption>Street_B</figcaption></figure>
<figure><img src="Airfield/Concrete/Street_C.jpg" alt="Street_C" loading="lazy"><figcaption>Street_C</figcaption></figure>

### Metal  
<figure><img src="Airfield/Metal/747PlatSteel_A.jpg" alt="747PlatSteel_A" loading="lazy"><figcaption>747PlatSteel_A</figcaption></figure>
<figure><img src="Airfield/Metal/747_bigwngsides.jpg" alt="747_bigwngsides" loading="lazy"><figcaption>747_bigwngsides</figcaption></figure>
<figure><img src="Airfield/Metal/747_black.jpg" alt="747_black" loading="lazy"><figcaption>747_black</figcaption></figure>
<figure><img src="Airfield/Metal/747_engnback.jpg" alt="747_engnback" loading="lazy"><figcaption>747_engnback</figcaption></figure>
<figure><img src="Airfield/Metal/747_engnexhst.jpg" alt="747_engnexhst" loading="lazy"><figcaption>747_engnexhst</figcaption></figure>
<figure><img src="Airfield/Metal/747_engnsides.jpg" alt="747_engnsides" loading="lazy"><figcaption>747_engnsides</figcaption></figure>
<figure><img src="Airfield/Metal/747_front_win.jpg" alt="747_front_win" loading="lazy"><figcaption>747_front_win</figcaption></figure>
<figure><img src="Airfield/Metal/747_intake.jpg" alt="747_intake" loading="lazy"><figcaption>747_intake</figcaption></figure>
<figure><img src="Airfield/Metal/747_main_gnrc.jpg" alt="747_main_gnrc" loading="lazy"><figcaption>747_main_gnrc</figcaption></figure>
<figure><img src="Airfield/Metal/747_main_gnrc_b.jpg" alt="747_main_gnrc_b" loading="lazy"><figcaption>747_main_gnrc_b</figcaption></figure>
<figure><img src="Airfield/Metal/747_metal.jpg" alt="747_metal" loading="lazy"><figcaption>747_metal</figcaption></figure>
<figure><img src="Airfield/Metal/747_rearwlkwy.jpg" alt="747_rearwlkwy" loading="lazy"><figcaption>747_rearwlkwy</figcaption></figure>
<figure><img src="Airfield/Metal/747_rearwlkwysd.jpg" alt="747_rearwlkwysd" loading="lazy"><figcaption>747_rearwlkwysd</figcaption></figure>
<figure><img src="Airfield/Metal/747_rearwng.jpg" alt="747_rearwng" loading="lazy"><figcaption>747_rearwng</figcaption></figure>
<figure><img src="Airfield/Metal/747_red.jpg" alt="747_red" loading="lazy"><figcaption>747_red</figcaption></figure>
<figure><img src="Airfield/Metal/747_red_yllw.jpg" alt="747_red_yllw" loading="lazy"><figcaption>747_red_yllw</figcaption></figure>
<figure><img src="Airfield/Metal/747_tail.jpg" alt="747_tail" loading="lazy"><figcaption>747_tail</figcaption></figure>
<figure><img src="Airfield/Metal/747_tail_main.jpg" alt="747_tail_main" loading="lazy"><figcaption>747_tail_main</figcaption></figure>
<figure><img src="Airfield/Metal/747_top_windw.jpg" alt="747_top_windw" loading="lazy"><figcaption>747_top_windw</figcaption></figure>
<figure><img src="Airfield/Metal/747_wheelside.jpg" alt="747_wheelside" loading="lazy"><figcaption>747_wheelside</figcaption></figure>
<figure><img src="Airfield/Metal/747_wing_big.jpg" alt="747_wing_big" loading="lazy"><figcaption>747_wing_big</figcaption></figure>
<figure><img src="Airfield/Metal/747_wing_big_fr.jpg" alt="747_wing_big_fr" loading="lazy"><figcaption>747_wing_big_fr</figcaption></figure>
<figure><img src="Airfield/Metal/747ceilnsec_A1.jpg" alt="747ceilnsec_A1" loading="lazy"><figcaption>747ceilnsec_A1</figcaption></figure>
<figure><img src="Airfield/Metal/747ceilnsec_B.jpg" alt="747ceilnsec_B" loading="lazy"><figcaption>747ceilnsec_B</figcaption></figure>
<figure><img src="Airfield/Metal/747ceilnsec_C.jpg" alt="747ceilnsec_C" loading="lazy"><figcaption>747ceilnsec_C</figcaption></figure>
<figure><img src="Airfield/Metal/747ceilnsec_C1.jpg" alt="747ceilnsec_C1" loading="lazy"><figcaption>747ceilnsec_C1</figcaption></figure>
<figure><img src="Airfield/Metal/747ceilnsec_C2.jpg" alt="747ceilnsec_C2" loading="lazy"><figcaption>747ceilnsec_C2</figcaption></figure>
<figure><img src="Airfield/Metal/747door_sec_B.jpg" alt="747door_sec_B" loading="lazy"><figcaption>747door_sec_B</figcaption></figure>
<figure><img src="Airfield/Metal/747door_sec_C.jpg" alt="747door_sec_C" loading="lazy"><figcaption>747door_sec_C</figcaption></figure>
<figure><img src="Airfield/Metal/747gripFloor_A.jpg" alt="747gripFloor_A" loading="lazy"><figcaption>747gripFloor_A</figcaption></figure>
<figure><img src="Airfield/Metal/747wall_sec_A.jpg" alt="747wall_sec_A" loading="lazy"><figcaption>747wall_sec_A</figcaption></figure>
<figure><img src="Airfield/Metal/747wall_sec_B.jpg" alt="747wall_sec_B" loading="lazy"><figcaption>747wall_sec_B</figcaption></figure>
<figure><img src="Airfield/Metal/747wall_sec_C.jpg" alt="747wall_sec_C" loading="lazy"><figcaption>747wall_sec_C</figcaption></figure>
<figure><img src="Airfield/Metal/747wall_sec_C1.jpg" alt="747wall_sec_C1" loading="lazy"><figcaption>747wall_sec_C1</figcaption></figure>
<figure><img src="Airfield/Metal/747windosec_A.jpg" alt="747windosec_A" loading="lazy"><figcaption>747windosec_A</figcaption></figure>
<figure><img src="Airfield/Metal/747windosec_C.jpg" alt="747windosec_C" loading="lazy"><figcaption>747windosec_C</figcaption></figure>
<figure><img src="Airfield/Metal/AF-HangrWall_A.jpg" alt="AF-HangrWall_A" loading="lazy"><figcaption>AF-HangrWall_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_BlueMtl_rst.jpg" alt="AF_BlueMtl_rst" loading="lazy"><figcaption>AF_BlueMtl_rst</figcaption></figure>
<figure><img src="Airfield/Metal/AF_CommPanel_A.jpg" alt="AF_CommPanel_A" loading="lazy"><figcaption>AF_CommPanel_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_CommPanel_B.jpg" alt="AF_CommPanel_B" loading="lazy"><figcaption>AF_CommPanel_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_DoorFrame_A.jpg" alt="AF_DoorFrame_A" loading="lazy"><figcaption>AF_DoorFrame_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_DoorFrame_B.jpg" alt="AF_DoorFrame_B" loading="lazy"><figcaption>AF_DoorFrame_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_Door_01.jpg" alt="AF_Door_01" loading="lazy"><figcaption>AF_Door_01</figcaption></figure>
<figure><img src="Airfield/Metal/AF_EastGate.jpg" alt="AF_EastGate" loading="lazy"><figcaption>AF_EastGate</figcaption></figure>
<figure><img src="Airfield/Metal/AF_FootLocker.jpg" alt="AF_FootLocker" loading="lazy"><figcaption>AF_FootLocker</figcaption></figure>
<figure><img src="Airfield/Metal/AF_GrayMetal_A.jpg" alt="AF_GrayMetal_A" loading="lazy"><figcaption>AF_GrayMetal_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_Hang_B_cnr2.jpg" alt="AF_Hang_B_cnr2" loading="lazy"><figcaption>AF_Hang_B_cnr2</figcaption></figure>
<figure><img src="Airfield/Metal/AF_Hang_B_end2.jpg" alt="AF_Hang_B_end2" loading="lazy"><figcaption>AF_Hang_B_end2</figcaption></figure>
<figure><img src="Airfield/Metal/AF_HangerDoor_A.jpg" alt="AF_HangerDoor_A" loading="lazy"><figcaption>AF_HangerDoor_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_HangerDoor_B.jpg" alt="AF_HangerDoor_B" loading="lazy"><figcaption>AF_HangerDoor_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_IronWOriv_A.jpg" alt="AF_IronWOriv_A" loading="lazy"><figcaption>AF_IronWOriv_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_LiftMetl_A.jpg" alt="AF_LiftMetl_A" loading="lazy"><figcaption>AF_LiftMetl_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_MetalDoor_B.jpg" alt="AF_MetalDoor_B" loading="lazy"><figcaption>AF_MetalDoor_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_MetalDoor_C.jpg" alt="AF_MetalDoor_C" loading="lazy"><figcaption>AF_MetalDoor_C</figcaption></figure>
<figure><img src="Airfield/Metal/AF_MetlWall_C.jpg" alt="AF_MetlWall_C" loading="lazy"><figcaption>AF_MetlWall_C</figcaption></figure>
<figure><img src="Airfield/Metal/AF_RedMtl_rst.jpg" alt="AF_RedMtl_rst" loading="lazy"><figcaption>AF_RedMtl_rst</figcaption></figure>
<figure><img src="Airfield/Metal/AF_Semitrailer.jpg" alt="AF_Semitrailer" loading="lazy"><figcaption>AF_Semitrailer</figcaption></figure>
<figure><img src="Airfield/Metal/AF_SouthGate.jpg" alt="AF_SouthGate" loading="lazy"><figcaption>AF_SouthGate</figcaption></figure>
<figure><img src="Airfield/Metal/AF_StorCeiln.jpg" alt="AF_StorCeiln" loading="lazy"><figcaption>AF_StorCeiln</figcaption></figure>
<figure><img src="Airfield/Metal/AF_StorEnds_A.jpg" alt="AF_StorEnds_A" loading="lazy"><figcaption>AF_StorEnds_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_StorEnds_B.jpg" alt="AF_StorEnds_B" loading="lazy"><figcaption>AF_StorEnds_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_StorFloors.jpg" alt="AF_StorFloors" loading="lazy"><figcaption>AF_StorFloors</figcaption></figure>
<figure><img src="Airfield/Metal/AF_StorSide_A.jpg" alt="AF_StorSide_A" loading="lazy"><figcaption>AF_StorSide_A</figcaption></figure>
<figure><img src="Airfield/Metal/AF_StorSide_B.jpg" alt="AF_StorSide_B" loading="lazy"><figcaption>AF_StorSide_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_WindFrame_B.jpg" alt="AF_WindFrame_B" loading="lazy"><figcaption>AF_WindFrame_B</figcaption></figure>
<figure><img src="Airfield/Metal/AF_interwall_C.jpg" alt="AF_interwall_C" loading="lazy"><figcaption>AF_interwall_C</figcaption></figure>
<figure><img src="Airfield/Metal/AF_interwall_D.jpg" alt="AF_interwall_D" loading="lazy"><figcaption>AF_interwall_D</figcaption></figure>
<figure><img src="Airfield/Metal/HangMtl_rst.jpg" alt="HangMtl_rst" loading="lazy"><figcaption>HangMtl_rst</figcaption></figure>

### Misc  
<figure><img src="Airfield/Misc/tanleatherseat.jpg" alt="tanleatherseat" loading="lazy"><figcaption>tanleatherseat</figcaption></figure>

### Paper  
<figure><img src="Airfield/Paper/AF_BookTops.jpg" alt="AF_BookTops" loading="lazy"><figcaption>AF_BookTops</figcaption></figure>

### Signs  
<figure><img src="Airfield/Signs/AF_747hangar.jpg" alt="AF_747hangar" loading="lazy"><figcaption>AF_747hangar</figcaption></figure>
<figure><img src="Airfield/Signs/AF_BoatHouse.jpg" alt="AF_BoatHouse" loading="lazy"><figcaption>AF_BoatHouse</figcaption></figure>
<figure><img src="Airfield/Signs/AF_DoorBroken.jpg" alt="AF_DoorBroken" loading="lazy"><figcaption>AF_DoorBroken</figcaption></figure>
<figure><img src="Airfield/Signs/AF_Elevator.jpg" alt="AF_Elevator" loading="lazy"><figcaption>AF_Elevator</figcaption></figure>
<figure><img src="Airfield/Signs/DoNoEnter_Sign.jpg" alt="DoNoEnter_Sign" loading="lazy"><figcaption>DoNoEnter_Sign</figcaption></figure>

### Sky  
<figure><img src="Airfield/Sky/AF_Buildn_SBa1.jpg" alt="AF_Buildn_SBa1" loading="lazy"><figcaption>AF_Buildn_SBa1</figcaption></figure>
<figure><img src="Airfield/Sky/AF_Buildn_SBa2.jpg" alt="AF_Buildn_SBa2" loading="lazy"><figcaption>AF_Buildn_SBa2</figcaption></figure>
<figure><img src="Airfield/Sky/AF_ConTower.jpg" alt="AF_ConTower" loading="lazy"><figcaption>AF_ConTower</figcaption></figure>
<figure><img src="Airfield/Sky/AF_skybox1Mbs.jpg" alt="AF_skybox1Mbs" loading="lazy"><figcaption>AF_skybox1Mbs</figcaption></figure>
<figure><img src="Airfield/Sky/AF_skybox2Mbs.jpg" alt="AF_skybox2Mbs" loading="lazy"><figcaption>AF_skybox2Mbs</figcaption></figure>
<figure><img src="Airfield/Sky/AF_skybox3Mbs.jpg" alt="AF_skybox3Mbs" loading="lazy"><figcaption>AF_skybox3Mbs</figcaption></figure>
<figure><img src="Airfield/Sky/AF_skybox4Mbs.jpg" alt="AF_skybox4Mbs" loading="lazy"><figcaption>AF_skybox4Mbs</figcaption></figure>
<figure><img src="Airfield/Sky/AF_skybox5Mbs.jpg" alt="AF_skybox5Mbs" loading="lazy"><figcaption>AF_skybox5Mbs</figcaption></figure>
<figure><img src="Airfield/Sky/AF_skybox6Mbs.jpg" alt="AF_skybox6Mbs" loading="lazy"><figcaption>AF_skybox6Mbs</figcaption></figure>
<figure><img src="Airfield/Sky/Buildn_SBa1end.jpg" alt="Buildn_SBa1end" loading="lazy"><figcaption>Buildn_SBa1end</figcaption></figure>
<figure><img src="Airfield/Sky/Buildn_SBa2end.jpg" alt="Buildn_SBa2end" loading="lazy"><figcaption>Buildn_SBa2end</figcaption></figure>

### Stone  
<figure><img src="Airfield/Stone/AF_Cindrbloc_B.jpg" alt="AF_Cindrbloc_B" loading="lazy"><figcaption>AF_Cindrbloc_B</figcaption></figure>
<figure><img src="Airfield/Stone/AF_Wall_01.jpg" alt="AF_Wall_01" loading="lazy"><figcaption>AF_Wall_01</figcaption></figure>
<figure><img src="Airfield/Stone/AF_Wall_03.jpg" alt="AF_Wall_03" loading="lazy"><figcaption>AF_Wall_03</figcaption></figure>
<figure><img src="Airfield/Stone/AF_Wall_04.jpg" alt="AF_Wall_04" loading="lazy"><figcaption>AF_Wall_04</figcaption></figure>
<figure><img src="Airfield/Stone/AF_Wall_05.jpg" alt="AF_Wall_05" loading="lazy"><figcaption>AF_Wall_05</figcaption></figure>
<figure><img src="Airfield/Stone/AF_Wall_06.jpg" alt="AF_Wall_06" loading="lazy"><figcaption>AF_Wall_06</figcaption></figure>

### Textile  
<figure><img src="Airfield/Textile/747carp_sec_B.jpg" alt="747carp_sec_B" loading="lazy"><figcaption>747carp_sec_B</figcaption></figure>
<figure><img src="Airfield/Textile/747carp_sec_C.jpg" alt="747carp_sec_C" loading="lazy"><figcaption>747carp_sec_C</figcaption></figure>
<figure><img src="Airfield/Textile/AF_Crew_Bed.jpg" alt="AF_Crew_Bed" loading="lazy"><figcaption>AF_Crew_Bed</figcaption></figure>
<figure><img src="Airfield/Textile/AF_Dartboard.jpg" alt="AF_Dartboard" loading="lazy"><figcaption>AF_Dartboard</figcaption></figure>

### Tiles  
<figure><img src="Airfield/Tiles/AF_CrewFloor_A.jpg" alt="AF_CrewFloor_A" loading="lazy"><figcaption>AF_CrewFloor_A</figcaption></figure>

### Tire  
<figure><img src="Airfield/Tire/747_wheeltrd.jpg" alt="747_wheeltrd" loading="lazy"><figcaption>747_wheeltrd</figcaption></figure>

### Wood  
<figure><img src="Airfield/Wood/PoolTable.jpg" alt="PoolTable" loading="lazy"><figcaption>PoolTable</figcaption></figure>## BatteryPark

### Brick  
<figure><img src="BatteryPark/Brick/BP_BrickWalksX.jpg" alt="BP_BrickWalksX" loading="lazy"><figcaption>BP_BrickWalksX</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_CC_Entry.jpg" alt="BP_CC_Entry" loading="lazy"><figcaption>BP_CC_Entry</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_CC_Name.jpg" alt="BP_CC_Name" loading="lazy"><figcaption>BP_CC_Name</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_CC_TopTrim.jpg" alt="BP_CC_TopTrim" loading="lazy"><figcaption>BP_CC_TopTrim</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_CC_Wall_A.jpg" alt="BP_CC_Wall_A" loading="lazy"><figcaption>BP_CC_Wall_A</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_CC_sWall_A.jpg" alt="BP_CC_sWall_A" loading="lazy"><figcaption>BP_CC_sWall_A</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_EaglePlaque.jpg" alt="BP_EaglePlaque" loading="lazy"><figcaption>BP_EaglePlaque</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_HexWalk.jpg" alt="BP_HexWalk" loading="lazy"><figcaption>BP_HexWalk</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_PebWalk_sB.jpg" alt="BP_PebWalk_sB" loading="lazy"><figcaption>BP_PebWalk_sB</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_WW2CemMem_A.jpg" alt="BP_WW2CemMem_A" loading="lazy"><figcaption>BP_WW2CemMem_A</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_WW2CemMem_B.jpg" alt="BP_WW2CemMem_B" loading="lazy"><figcaption>BP_WW2CemMem_B</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_WW2CemMem_C.jpg" alt="BP_WW2CemMem_C" loading="lazy"><figcaption>BP_WW2CemMem_C</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_WW2CemMem_D.jpg" alt="BP_WW2CemMem_D" loading="lazy"><figcaption>BP_WW2CemMem_D</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_WW2CemMem_E.jpg" alt="BP_WW2CemMem_E" loading="lazy"><figcaption>BP_WW2CemMem_E</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_sBrickWalk.jpg" alt="BP_sBrickWalk" loading="lazy"><figcaption>BP_sBrickWalk</figcaption></figure>
<figure><img src="BatteryPark/Brick/BP_sHexWalk.jpg" alt="BP_sHexWalk" loading="lazy"><figcaption>BP_sHexWalk</figcaption></figure>
<figure><img src="BatteryPark/Brick/CGCD2.jpg" alt="CGCD2" loading="lazy"><figcaption>CGCD2</figcaption></figure>
<figure><img src="BatteryPark/Brick/CGCD2b.jpg" alt="CGCD2b" loading="lazy"><figcaption>CGCD2b</figcaption></figure>
<figure><img src="BatteryPark/Brick/DBSwalk2.jpg" alt="DBSwalk2" loading="lazy"><figcaption>DBSwalk2</figcaption></figure>
<figure><img src="BatteryPark/Brick/Pebwalk2.jpg" alt="Pebwalk2" loading="lazy"><figcaption>Pebwalk2</figcaption></figure>
<figure><img src="BatteryPark/Brick/WallwithVines.jpg" alt="WallwithVines" loading="lazy"><figcaption>WallwithVines</figcaption></figure>

### Foliage  
<figure><img src="BatteryPark/Foliage/Ngrass.jpg" alt="Ngrass" loading="lazy"><figcaption>Ngrass</figcaption></figure>
<figure><img src="BatteryPark/Foliage/NgrassB.jpg" alt="NgrassB" loading="lazy"><figcaption>NgrassB</figcaption></figure>

### Glass  
<figure><img src="BatteryPark/Glass/BP_LightLens1.jpg" alt="BP_LightLens1" loading="lazy"><figcaption>BP_LightLens1</figcaption></figure>
<figure><img src="BatteryPark/Glass/BP_LightLens2.jpg" alt="BP_LightLens2" loading="lazy"><figcaption>BP_LightLens2</figcaption></figure>
<figure><img src="BatteryPark/Glass/BP_LightLens3.jpg" alt="BP_LightLens3" loading="lazy"><figcaption>BP_LightLens3</figcaption></figure>

### Metal  
<figure><img src="BatteryPark/Metal/BP_BPmap.jpg" alt="BP_BPmap" loading="lazy"><figcaption>BP_BPmap</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_BPsign.jpg" alt="BP_BPsign" loading="lazy"><figcaption>BP_BPsign</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_BlackFence.jpg" alt="BP_BlackFence" loading="lazy"><figcaption>BP_BlackFence</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_CC_LdoorL.jpg" alt="BP_CC_LdoorL" loading="lazy"><figcaption>BP_CC_LdoorL</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_CC_LdoorU.jpg" alt="BP_CC_LdoorU" loading="lazy"><figcaption>BP_CC_LdoorU</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_CC_Plaque_A.jpg" alt="BP_CC_Plaque_A" loading="lazy"><figcaption>BP_CC_Plaque_A</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_CC_Plaque_B.jpg" alt="BP_CC_Plaque_B" loading="lazy"><figcaption>BP_CC_Plaque_B</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_CC_RdoorL.jpg" alt="BP_CC_RdoorL" loading="lazy"><figcaption>BP_CC_RdoorL</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_CC_RdoorU.jpg" alt="BP_CC_RdoorU" loading="lazy"><figcaption>BP_CC_RdoorU</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_K_War_Sign.jpg" alt="BP_K_War_Sign" loading="lazy"><figcaption>BP_K_War_Sign</figcaption></figure>
<figure><img src="BatteryPark/Metal/BP_NoDOG_sign.jpg" alt="BP_NoDOG_sign" loading="lazy"><figcaption>BP_NoDOG_sign</figcaption></figure>
<figure><img src="BatteryPark/Metal/ChainLength.jpg" alt="ChainLength" loading="lazy"><figcaption>ChainLength</figcaption></figure>
<figure><img src="BatteryPark/Metal/ChainLinks.jpg" alt="ChainLinks" loading="lazy"><figcaption>ChainLinks</figcaption></figure>
<figure><img src="BatteryPark/Metal/DcorgMet.jpg" alt="DcorgMet" loading="lazy"><figcaption>DcorgMet</figcaption></figure>
<figure><img src="BatteryPark/Metal/DcorgMet2.jpg" alt="DcorgMet2" loading="lazy"><figcaption>DcorgMet2</figcaption></figure>
<figure><img src="BatteryPark/Metal/ssign2ndave.jpg" alt="ssign2ndave" loading="lazy"><figcaption>ssign2ndave</figcaption></figure>
<figure><img src="BatteryPark/Metal/ssignbatprk1.jpg" alt="ssignbatprk1" loading="lazy"><figcaption>ssignbatprk1</figcaption></figure>
<figure><img src="BatteryPark/Metal/sub_girder1.jpg" alt="sub_girder1" loading="lazy"><figcaption>sub_girder1</figcaption></figure>
<figure><img src="BatteryPark/Metal/sub_girder2.jpg" alt="sub_girder2" loading="lazy"><figcaption>sub_girder2</figcaption></figure>
<figure><img src="BatteryPark/Metal/subceiling.jpg" alt="subceiling" loading="lazy"><figcaption>subceiling</figcaption></figure>

### Sky  
<figure><img src="BatteryPark/Sky/BPskyboxBldg_1.jpg" alt="BPskyboxBldg_1" loading="lazy"><figcaption>BPskyboxBldg_1</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxBldg_3.jpg" alt="BPskyboxBldg_3" loading="lazy"><figcaption>BPskyboxBldg_3</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxBldg_4.jpg" alt="BPskyboxBldg_4" loading="lazy"><figcaption>BPskyboxBldg_4</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxBldg_5.jpg" alt="BPskyboxBldg_5" loading="lazy"><figcaption>BPskyboxBldg_5</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxBldg_6.jpg" alt="BPskyboxBldg_6" loading="lazy"><figcaption>BPskyboxBldg_6</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxBldg_7.jpg" alt="BPskyboxBldg_7" loading="lazy"><figcaption>BPskyboxBldg_7</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxElis_2.jpg" alt="BPskyboxElis_2" loading="lazy"><figcaption>BPskyboxElis_2</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxGen_1.jpg" alt="BPskyboxGen_1" loading="lazy"><figcaption>BPskyboxGen_1</figcaption></figure>
<figure><img src="BatteryPark/Sky/BPskyboxLib_1.jpg" alt="BPskyboxLib_1" loading="lazy"><figcaption>BPskyboxLib_1</figcaption></figure>
<figure><img src="BatteryPark/Sky/NYskybox1Mbs.jpg" alt="NYskybox1Mbs" loading="lazy"><figcaption>NYskybox1Mbs</figcaption></figure>
<figure><img src="BatteryPark/Sky/NYskybox2Mbs.jpg" alt="NYskybox2Mbs" loading="lazy"><figcaption>NYskybox2Mbs</figcaption></figure>
<figure><img src="BatteryPark/Sky/NYskybox3Mbs.jpg" alt="NYskybox3Mbs" loading="lazy"><figcaption>NYskybox3Mbs</figcaption></figure>

### Textile  
<figure><img src="BatteryPark/Textile/mattress1.jpg" alt="mattress1" loading="lazy"><figcaption>mattress1</figcaption></figure>

### Tile  
<figure><img src="BatteryPark/Tile/subpillar.jpg" alt="subpillar" loading="lazy"><figcaption>subpillar</figcaption></figure>
<figure><img src="BatteryPark/Tile/subway1.jpg" alt="subway1" loading="lazy"><figcaption>subway1</figcaption></figure>
<figure><img src="BatteryPark/Tile/subway1b.jpg" alt="subway1b" loading="lazy"><figcaption>subway1b</figcaption></figure>
<figure><img src="BatteryPark/Tile/subway2.jpg" alt="subway2" loading="lazy"><figcaption>subway2</figcaption></figure>

### Wood  
<figure><img src="BatteryPark/Wood/BP_PierPoles_A.jpg" alt="BP_PierPoles_A" loading="lazy"><figcaption>BP_PierPoles_A</figcaption></figure>
<figure><img src="BatteryPark/Wood/DGW2.jpg" alt="DGW2" loading="lazy"><figcaption>DGW2</figcaption></figure>
<figure><img src="BatteryPark/Wood/DGW2b.jpg" alt="DGW2b" loading="lazy"><figcaption>DGW2b</figcaption></figure>
<figure><img src="BatteryPark/Wood/subwayflr.jpg" alt="subwayflr" loading="lazy"><figcaption>subwayflr</figcaption></figure>## Catacombs

### Glass  
<figure><img src="Catacombs/Glass/pa_pde_sign.jpg" alt="pa_pde_sign" loading="lazy"><figcaption>pa_pde_sign</figcaption></figure>
<figure><img src="Catacombs/Glass/pa_sqrlight.jpg" alt="pa_sqrlight" loading="lazy"><figcaption>pa_sqrlight</figcaption></figure>
<figure><img src="Catacombs/Glass/pa_strplight.jpg" alt="pa_strplight" loading="lazy"><figcaption>pa_strplight</figcaption></figure>

### Misc  
<figure><img src="Catacombs/Misc/AIPrototype_A01.jpg" alt="AIPrototype_A01" loading="lazy"><figcaption>AIPrototype_A01</figcaption></figure>
<figure><img src="Catacombs/Misc/AIPrototype_A02.jpg" alt="AIPrototype_A02" loading="lazy"><figcaption>AIPrototype_A02</figcaption></figure>
<figure><img src="Catacombs/Misc/AIPrototype_A03.jpg" alt="AIPrototype_A03" loading="lazy"><figcaption>AIPrototype_A03</figcaption></figure>
<figure><img src="Catacombs/Misc/AIPrototype_A04.jpg" alt="AIPrototype_A04" loading="lazy"><figcaption>AIPrototype_A04</figcaption></figure>
<figure><img src="Catacombs/Misc/AIPrototype_A05.jpg" alt="AIPrototype_A05" loading="lazy"><figcaption>AIPrototype_A05</figcaption></figure>
<figure><img src="Catacombs/Misc/AIPrototype_A06.jpg" alt="AIPrototype_A06" loading="lazy"><figcaption>AIPrototype_A06</figcaption></figure>
<figure><img src="Catacombs/Misc/AIPrototype_A07.jpg" alt="AIPrototype_A07" loading="lazy"><figcaption>AIPrototype_A07</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_A.jpg" alt="pa_TrainSign_A" loading="lazy"><figcaption>pa_TrainSign_A</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_B.jpg" alt="pa_TrainSign_B" loading="lazy"><figcaption>pa_TrainSign_B</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_C.jpg" alt="pa_TrainSign_C" loading="lazy"><figcaption>pa_TrainSign_C</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_D.jpg" alt="pa_TrainSign_D" loading="lazy"><figcaption>pa_TrainSign_D</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_E.jpg" alt="pa_TrainSign_E" loading="lazy"><figcaption>pa_TrainSign_E</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_F.jpg" alt="pa_TrainSign_F" loading="lazy"><figcaption>pa_TrainSign_F</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_TrainSign_G.jpg" alt="pa_TrainSign_G" loading="lazy"><figcaption>pa_TrainSign_G</figcaption></figure>
<figure><img src="Catacombs/Misc/pa_metroflr.jpg" alt="pa_metroflr" loading="lazy"><figcaption>pa_metroflr</figcaption></figure>

### Stone  
<figure><img src="Catacombs/Stone/Cath_Interior_A.jpg" alt="Cath_Interior_A" loading="lazy"><figcaption>Cath_Interior_A</figcaption></figure>
<figure><img src="Catacombs/Stone/Cath_Interior_B.jpg" alt="Cath_Interior_B" loading="lazy"><figcaption>Cath_Interior_B</figcaption></figure>
<figure><img src="Catacombs/Stone/Cath_Interior_C.jpg" alt="Cath_Interior_C" loading="lazy"><figcaption>Cath_Interior_C</figcaption></figure>
<figure><img src="Catacombs/Stone/Cath_Stonebloc.jpg" alt="Cath_Stonebloc" loading="lazy"><figcaption>Cath_Stonebloc</figcaption></figure>
<figure><img src="Catacombs/Stone/CtCmbBlstDr_1.jpg" alt="CtCmbBlstDr_1" loading="lazy"><figcaption>CtCmbBlstDr_1</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_11.jpg" alt="Ctcomb_11" loading="lazy"><figcaption>Ctcomb_11</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_12.jpg" alt="Ctcomb_12" loading="lazy"><figcaption>Ctcomb_12</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_13.jpg" alt="Ctcomb_13" loading="lazy"><figcaption>Ctcomb_13</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_14.jpg" alt="Ctcomb_14" loading="lazy"><figcaption>Ctcomb_14</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_2.jpg" alt="Ctcomb_2" loading="lazy"><figcaption>Ctcomb_2</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_3.jpg" alt="Ctcomb_3" loading="lazy"><figcaption>Ctcomb_3</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_4.jpg" alt="Ctcomb_4" loading="lazy"><figcaption>Ctcomb_4</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_5.jpg" alt="Ctcomb_5" loading="lazy"><figcaption>Ctcomb_5</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_6.jpg" alt="Ctcomb_6" loading="lazy"><figcaption>Ctcomb_6</figcaption></figure>
<figure><img src="Catacombs/Stone/Ctcomb_8.jpg" alt="Ctcomb_8" loading="lazy"><figcaption>Ctcomb_8</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_01.jpg" alt="Paris_Wall_01" loading="lazy"><figcaption>Paris_Wall_01</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_02.jpg" alt="Paris_Wall_02" loading="lazy"><figcaption>Paris_Wall_02</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_03.jpg" alt="Paris_Wall_03" loading="lazy"><figcaption>Paris_Wall_03</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_04.jpg" alt="Paris_Wall_04" loading="lazy"><figcaption>Paris_Wall_04</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_05.jpg" alt="Paris_Wall_05" loading="lazy"><figcaption>Paris_Wall_05</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_06.jpg" alt="Paris_Wall_06" loading="lazy"><figcaption>Paris_Wall_06</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_07.jpg" alt="Paris_Wall_07" loading="lazy"><figcaption>Paris_Wall_07</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_08.jpg" alt="Paris_Wall_08" loading="lazy"><figcaption>Paris_Wall_08</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_09.jpg" alt="Paris_Wall_09" loading="lazy"><figcaption>Paris_Wall_09</figcaption></figure>
<figure><img src="Catacombs/Stone/Paris_Wall_10.jpg" alt="Paris_Wall_10" loading="lazy"><figcaption>Paris_Wall_10</figcaption></figure>
<figure><img src="Catacombs/Stone/SkullWall_01.jpg" alt="SkullWall_01" loading="lazy"><figcaption>SkullWall_01</figcaption></figure>
<figure><img src="Catacombs/Stone/SkullWall_02.jpg" alt="SkullWall_02" loading="lazy"><figcaption>SkullWall_02</figcaption></figure>
<figure><img src="Catacombs/Stone/SkullWall_05.jpg" alt="SkullWall_05" loading="lazy"><figcaption>SkullWall_05</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_TrainWall_A.jpg" alt="pa_TrainWall_A" loading="lazy"><figcaption>pa_TrainWall_A</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_b01_sign.jpg" alt="pa_b01_sign" loading="lazy"><figcaption>pa_b01_sign</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_b02_sign.jpg" alt="pa_b02_sign" loading="lazy"><figcaption>pa_b02_sign</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_b03_sign.jpg" alt="pa_b03_sign" loading="lazy"><figcaption>pa_b03_sign</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_metrotile.jpg" alt="pa_metrotile" loading="lazy"><figcaption>pa_metrotile</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_pitcmnt.jpg" alt="pa_pitcmnt" loading="lazy"><figcaption>pa_pitcmnt</figcaption></figure>
<figure><img src="Catacombs/Stone/pa_pitwall.jpg" alt="pa_pitwall" loading="lazy"><figcaption>pa_pitwall</figcaption></figure>

### Wood  
<figure><img src="Catacombs/Wood/ClenWoodPanel_A.jpg" alt="ClenWoodPanel_A" loading="lazy"><figcaption>ClenWoodPanel_A</figcaption></figure>
<figure><img src="Catacombs/Wood/pa_catgrydor.jpg" alt="pa_catgrydor" loading="lazy"><figcaption>pa_catgrydor</figcaption></figure>## Cmd_tunnels

### Metal  
<figure><img src="Cmd_tunnels/Metal/CMDT_pipeend.jpg" alt="CMDT_pipeend" loading="lazy"><figcaption>CMDT_pipeend</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Cmd_tnl_pnl_1.jpg" alt="Cmd_tnl_pnl_1" loading="lazy"><figcaption>Cmd_tnl_pnl_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Error_bttn_1.jpg" alt="Error_bttn_1" loading="lazy"><figcaption>Error_bttn_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/GenRoomSign_1.jpg" alt="GenRoomSign_1" loading="lazy"><figcaption>GenRoomSign_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Hzrd_bttn_1.jpg" alt="Hzrd_bttn_1" loading="lazy"><figcaption>Hzrd_bttn_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Hzrd_bttn_2.jpg" alt="Hzrd_bttn_2" loading="lazy"><figcaption>Hzrd_bttn_2</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Hzrd_bttn_3.jpg" alt="Hzrd_bttn_3" loading="lazy"><figcaption>Hzrd_bttn_3</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Hzrd_sign_1.jpg" alt="Hzrd_sign_1" loading="lazy"><figcaption>Hzrd_sign_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Hzrd_sign_2.jpg" alt="Hzrd_sign_2" loading="lazy"><figcaption>Hzrd_sign_2</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Hzrd_sign_3.jpg" alt="Hzrd_sign_3" loading="lazy"><figcaption>Hzrd_sign_3</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/MA_sign.jpg" alt="MA_sign" loading="lazy"><figcaption>MA_sign</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Ractivesign_1.jpg" alt="Ractivesign_1" loading="lazy"><figcaption>Ractivesign_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Rset_bttn_1.jpg" alt="Rset_bttn_1" loading="lazy"><figcaption>Rset_bttn_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/StorageRm_sign.jpg" alt="StorageRm_sign" loading="lazy"><figcaption>StorageRm_sign</figcaption></figure>
<figure><img src="Cmd_tunnels/Metal/Yellowpipe_1.jpg" alt="Yellowpipe_1" loading="lazy"><figcaption>Yellowpipe_1</figcaption></figure>

### Misc  
<figure><img src="Cmd_tunnels/Misc/Ractivespill_1.jpg" alt="Ractivespill_1" loading="lazy"><figcaption>Ractivespill_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Misc/Ractivespill_2.jpg" alt="Ractivespill_2" loading="lazy"><figcaption>Ractivespill_2</figcaption></figure>
<figure><img src="Cmd_tunnels/Misc/Ractivespill_3.jpg" alt="Ractivespill_3" loading="lazy"><figcaption>Ractivespill_3</figcaption></figure>
<figure><img src="Cmd_tunnels/Misc/Ractivespill_4.jpg" alt="Ractivespill_4" loading="lazy"><figcaption>Ractivespill_4</figcaption></figure>

### Wall_Objects  
<figure><img src="Cmd_tunnels/Wall_Objects/Cmd_tnl_wall_1.jpg" alt="Cmd_tnl_wall_1" loading="lazy"><figcaption>Cmd_tnl_wall_1</figcaption></figure>
<figure><img src="Cmd_tunnels/Wall_Objects/Cmd_tnl_wall_2.jpg" alt="Cmd_tnl_wall_2" loading="lazy"><figcaption>Cmd_tnl_wall_2</figcaption></figure>
<figure><img src="Cmd_tunnels/Wall_Objects/Cmd_tnl_wall_3.jpg" alt="Cmd_tnl_wall_3" loading="lazy"><figcaption>Cmd_tnl_wall_3</figcaption></figure>## Constructor

### Metal  
<figure><img src="Constructor/Metal/UC_Panel_01.jpg" alt="UC_Panel_01" loading="lazy"><figcaption>UC_Panel_01</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_02.jpg" alt="UC_Panel_02" loading="lazy"><figcaption>UC_Panel_02</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_03.jpg" alt="UC_Panel_03" loading="lazy"><figcaption>UC_Panel_03</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_04.jpg" alt="UC_Panel_04" loading="lazy"><figcaption>UC_Panel_04</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_05.jpg" alt="UC_Panel_05" loading="lazy"><figcaption>UC_Panel_05</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_06.jpg" alt="UC_Panel_06" loading="lazy"><figcaption>UC_Panel_06</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_07.jpg" alt="UC_Panel_07" loading="lazy"><figcaption>UC_Panel_07</figcaption></figure>
<figure><img src="Constructor/Metal/UC_Panel_08.jpg" alt="UC_Panel_08" loading="lazy"><figcaption>UC_Panel_08</figcaption></figure>
<figure><img src="Constructor/Metal/VBC_Hlgram_01.jpg" alt="VBC_Hlgram_01" loading="lazy"><figcaption>VBC_Hlgram_01</figcaption></figure>
<figure><img src="Constructor/Metal/VBC_Hlgram_02.jpg" alt="VBC_Hlgram_02" loading="lazy"><figcaption>VBC_Hlgram_02</figcaption></figure>

### Misc  
<figure><img src="Constructor/Misc/UC_Tube_01.jpg" alt="UC_Tube_01" loading="lazy"><figcaption>UC_Tube_01</figcaption></figure>## CoreTexBrick

### Brick  
<figure><img src="CoreTexBrick/Brick/DrtyGrayBrick_B.jpg" alt="DrtyGrayBrick_B" loading="lazy"><figcaption>DrtyGrayBrick_B</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/DrtyGrayWalks_A.jpg" alt="DrtyGrayWalks_A" loading="lazy"><figcaption>DrtyGrayWalks_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/DrtyReddBrick_A.jpg" alt="DrtyReddBrick_A" loading="lazy"><figcaption>DrtyReddBrick_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/DrtyReddBrick_B.jpg" alt="DrtyReddBrick_B" loading="lazy"><figcaption>DrtyReddBrick_B</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/DrtyReddBrick_C.jpg" alt="DrtyReddBrick_C" loading="lazy"><figcaption>DrtyReddBrick_C</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/MultColrBrick_A.jpg" alt="MultColrBrick_A" loading="lazy"><figcaption>MultColrBrick_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/PantWhitBrick.jpg" alt="PantWhitBrick" loading="lazy"><figcaption>PantWhitBrick</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/RuffBrwnBrick_A.jpg" alt="RuffBrwnBrick_A" loading="lazy"><figcaption>RuffBrwnBrick_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/RuffBrwnBrick_b.jpg" alt="RuffBrwnBrick_b" loading="lazy"><figcaption>RuffBrwnBrick_b</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/RuffWhitBrick_A.jpg" alt="RuffWhitBrick_A" loading="lazy"><figcaption>RuffWhitBrick_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/RuffWhitBrick_B.jpg" alt="RuffWhitBrick_B" loading="lazy"><figcaption>RuffWhitBrick_B</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/SqarReddBrick_A.jpg" alt="SqarReddBrick_A" loading="lazy"><figcaption>SqarReddBrick_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/SqarReddBrick_B.jpg" alt="SqarReddBrick_B" loading="lazy"><figcaption>SqarReddBrick_B</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/StanGrayCindr_A.jpg" alt="StanGrayCindr_A" loading="lazy"><figcaption>StanGrayCindr_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/StanGrayCindr_B.jpg" alt="StanGrayCindr_B" loading="lazy"><figcaption>StanGrayCindr_B</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/Wall_A.jpg" alt="Wall_A" loading="lazy"><figcaption>Wall_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/Wall_B.jpg" alt="Wall_B" loading="lazy"><figcaption>Wall_B</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/flrtile_A.jpg" alt="flrtile_A" loading="lazy"><figcaption>flrtile_A</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/redbrckpttrn_a.jpg" alt="redbrckpttrn_a" loading="lazy"><figcaption>redbrckpttrn_a</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/rough_greywall.jpg" alt="rough_greywall" loading="lazy"><figcaption>rough_greywall</figcaption></figure>
<figure><img src="CoreTexBrick/Brick/whitemarble_brk.jpg" alt="whitemarble_brk" loading="lazy"><figcaption>whitemarble_brk</figcaption></figure>## CoreTexCeramic

### Ceramic  
<figure><img src="CoreTexCeramic/Ceramic/CermBeigFloor_A.jpg" alt="CermBeigFloor_A" loading="lazy"><figcaption>CermBeigFloor_A</figcaption></figure>
<figure><img src="CoreTexCeramic/Ceramic/CermRomnFloor_C.jpg" alt="CermRomnFloor_C" loading="lazy"><figcaption>CermRomnFloor_C</figcaption></figure>
<figure><img src="CoreTexCeramic/Ceramic/CermRomnFloor_D.jpg" alt="CermRomnFloor_D" loading="lazy"><figcaption>CermRomnFloor_D</figcaption></figure>
<figure><img src="CoreTexCeramic/Ceramic/CermTileFloor_A.jpg" alt="CermTileFloor_A" loading="lazy"><figcaption>CermTileFloor_A</figcaption></figure>
<figure><img src="CoreTexCeramic/Ceramic/Clay ReddRoofT.jpg" alt="Clay ReddRoofT" loading="lazy"><figcaption>Clay ReddRoofT</figcaption></figure>
<figure><img src="CoreTexCeramic/Ceramic/ClayBrwnRoofT_A.jpg" alt="ClayBrwnRoofT_A" loading="lazy"><figcaption>ClayBrwnRoofT_A</figcaption></figure>
<figure><img src="CoreTexCeramic/Ceramic/DamgGrenTile_A.jpg" alt="DamgGrenTile_A" loading="lazy"><figcaption>DamgGrenTile_A</figcaption></figure>

### Tiles  
<figure><img src="CoreTexCeramic/Tiles/CermTileFloor_A.jpg" alt="CermTileFloor_A" loading="lazy"><figcaption>CermTileFloor_A</figcaption></figure>## CoreTexConcrete

### Concrete  
<figure><img src="CoreTexConcrete/Concrete/ClenBlakStret_A.jpg" alt="ClenBlakStret_A" loading="lazy"><figcaption>ClenBlakStret_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenBlakStret_B.jpg" alt="ClenBlakStret_B" loading="lazy"><figcaption>ClenBlakStret_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGrayCemnt_A.jpg" alt="ClenGrayCemnt_A" loading="lazy"><figcaption>ClenGrayCemnt_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGrayCemnt_B.jpg" alt="ClenGrayCemnt_B" loading="lazy"><figcaption>ClenGrayCemnt_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGrayCemnt_C.jpg" alt="ClenGrayCemnt_C" loading="lazy"><figcaption>ClenGrayCemnt_C</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGrayCemnt_D.jpg" alt="ClenGrayCemnt_D" loading="lazy"><figcaption>ClenGrayCemnt_D</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGrayCemnt_E.jpg" alt="ClenGrayCemnt_E" loading="lazy"><figcaption>ClenGrayCemnt_E</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGreyStret_B.jpg" alt="ClenGreyStret_B" loading="lazy"><figcaption>ClenGreyStret_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/ClenGreyStret_C.jpg" alt="ClenGreyStret_C" loading="lazy"><figcaption>ClenGreyStret_C</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyBrwnBlock_A.jpg" alt="DrtyBrwnBlock_A" loading="lazy"><figcaption>DrtyBrwnBlock_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyBrwnCemnt_A.jpg" alt="DrtyBrwnCemnt_A" loading="lazy"><figcaption>DrtyBrwnCemnt_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyBrwnSwalk_A.jpg" alt="DrtyBrwnSwalk_A" loading="lazy"><figcaption>DrtyBrwnSwalk_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyCastCemnt_A.jpg" alt="DrtyCastCemnt_A" loading="lazy"><figcaption>DrtyCastCemnt_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyGrayBlock_A.jpg" alt="DrtyGrayBlock_A" loading="lazy"><figcaption>DrtyGrayBlock_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyGrayCemnt_A.jpg" alt="DrtyGrayCemnt_A" loading="lazy"><figcaption>DrtyGrayCemnt_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyGraySwalk_A.jpg" alt="DrtyGraySwalk_A" loading="lazy"><figcaption>DrtyGraySwalk_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyHeliCemnt_A.jpg" alt="DrtyHeliCemnt_A" loading="lazy"><figcaption>DrtyHeliCemnt_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/DrtyHeliCemnt_B.jpg" alt="DrtyHeliCemnt_B" loading="lazy"><figcaption>DrtyHeliCemnt_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/RoadBlacktop_B.jpg" alt="RoadBlacktop_B" loading="lazy"><figcaption>RoadBlacktop_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/RoadBlacktop__A.jpg" alt="RoadBlacktop__A" loading="lazy"><figcaption>RoadBlacktop__A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/RoadBlacktop__C.jpg" alt="RoadBlacktop__C" loading="lazy"><figcaption>RoadBlacktop__C</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/RuffDarkAspha_A.jpg" alt="RuffDarkAspha_A" loading="lazy"><figcaption>RuffDarkAspha_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/RuffDarkAspha_B.jpg" alt="RuffDarkAspha_B" loading="lazy"><figcaption>RuffDarkAspha_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SGCemntB_Arrow1.jpg" alt="SGCemntB_Arrow1" loading="lazy"><figcaption>SGCemntB_Arrow1</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SGCemntB_Line1.jpg" alt="SGCemntB_Line1" loading="lazy"><figcaption>SGCemntB_Line1</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SGCemntB_Line2.jpg" alt="SGCemntB_Line2" loading="lazy"><figcaption>SGCemntB_Line2</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SGCemntB_Line3.jpg" alt="SGCemntB_Line3" loading="lazy"><figcaption>SGCemntB_Line3</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SGCemntC_Line3.jpg" alt="SGCemntC_Line3" loading="lazy"><figcaption>SGCemntC_Line3</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SGCemntC_crsswk.jpg" alt="SGCemntC_crsswk" loading="lazy"><figcaption>SGCemntC_crsswk</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SmthGrayCemnt_A.jpg" alt="SmthGrayCemnt_A" loading="lazy"><figcaption>SmthGrayCemnt_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SmthGrayCemnt_B.jpg" alt="SmthGrayCemnt_B" loading="lazy"><figcaption>SmthGrayCemnt_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SmthGrayCemnt_C.jpg" alt="SmthGrayCemnt_C" loading="lazy"><figcaption>SmthGrayCemnt_C</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/SmthGrayCemnt_E.jpg" alt="SmthGrayCemnt_E" loading="lazy"><figcaption>SmthGrayCemnt_E</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/StanGrenCemet_A.jpg" alt="StanGrenCemet_A" loading="lazy"><figcaption>StanGrenCemet_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/VertPatnConcr_A.jpg" alt="VertPatnConcr_A" loading="lazy"><figcaption>VertPatnConcr_A</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/VertPatnConcr_B.jpg" alt="VertPatnConcr_B" loading="lazy"><figcaption>VertPatnConcr_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/VertPatnConcr_C.jpg" alt="VertPatnConcr_C" loading="lazy"><figcaption>VertPatnConcr_C</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/VertPatnConcr_D.jpg" alt="VertPatnConcr_D" loading="lazy"><figcaption>VertPatnConcr_D</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/flatconcrete_a.jpg" alt="flatconcrete_a" loading="lazy"><figcaption>flatconcrete_a</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/grungysdwlk_a.jpg" alt="grungysdwlk_a" loading="lazy"><figcaption>grungysdwlk_a</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/un_heli_cncrt_B.jpg" alt="un_heli_cncrt_B" loading="lazy"><figcaption>un_heli_cncrt_B</figcaption></figure>
<figure><img src="CoreTexConcrete/Concrete/un_heli_cncrt_a.jpg" alt="un_heli_cncrt_a" loading="lazy"><figcaption>un_heli_cncrt_a</figcaption></figure>## CoreTexDetail

### Detail  
<figure><img src="CoreTexDetail/Detail/DCracks_A.jpg" alt="DCracks_A" loading="lazy"><figcaption>DCracks_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DFabric_A.jpg" alt="DFabric_A" loading="lazy"><figcaption>DFabric_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DFabric_B.jpg" alt="DFabric_B" loading="lazy"><figcaption>DFabric_B</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DGouges_A.jpg" alt="DGouges_A" loading="lazy"><figcaption>DGouges_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DMetal_A.jpg" alt="DMetal_A" loading="lazy"><figcaption>DMetal_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DMetal_B.jpg" alt="DMetal_B" loading="lazy"><figcaption>DMetal_B</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DPitted_A.jpg" alt="DPitted_A" loading="lazy"><figcaption>DPitted_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DPitted_B.jpg" alt="DPitted_B" loading="lazy"><figcaption>DPitted_B</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DPitted_C.jpg" alt="DPitted_C" loading="lazy"><figcaption>DPitted_C</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DScanline.jpg" alt="DScanline" loading="lazy"><figcaption>DScanline</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DScratchs_A.jpg" alt="DScratchs_A" loading="lazy"><figcaption>DScratchs_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DStone_A.jpg" alt="DStone_A" loading="lazy"><figcaption>DStone_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DStone_B.jpg" alt="DStone_B" loading="lazy"><figcaption>DStone_B</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DStone_C.jpg" alt="DStone_C" loading="lazy"><figcaption>DStone_C</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DStone_D.jpg" alt="DStone_D" loading="lazy"><figcaption>DStone_D</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DWoodFine_A.jpg" alt="DWoodFine_A" loading="lazy"><figcaption>DWoodFine_A</figcaption></figure>
<figure><img src="CoreTexDetail/Detail/DWoodRuff_A.jpg" alt="DWoodRuff_A" loading="lazy"><figcaption>DWoodRuff_A</figcaption></figure>## CoreTexEarth

### Earth  
<figure><img src="CoreTexEarth/Earth/ChipRockGravl_A.jpg" alt="ChipRockGravl_A" loading="lazy"><figcaption>ChipRockGravl_A</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/ChipRockGravl_B.jpg" alt="ChipRockGravl_B" loading="lazy"><figcaption>ChipRockGravl_B</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/ClenLtan Adobe_.jpg" alt="ClenLtan Adobe_" loading="lazy"><figcaption>ClenLtan Adobe_</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/DrtyBrwnEarth_A.jpg" alt="DrtyBrwnEarth_A" loading="lazy"><figcaption>DrtyBrwnEarth_A</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/DrtyBrwnEarth_B.jpg" alt="DrtyBrwnEarth_B" loading="lazy"><figcaption>DrtyBrwnEarth_B</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/DrtyBrwnEarth_D.jpg" alt="DrtyBrwnEarth_D" loading="lazy"><figcaption>DrtyBrwnEarth_D</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/LakeBed_A.jpg" alt="LakeBed_A" loading="lazy"><figcaption>LakeBed_A</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/RockBrwnEarth_A.jpg" alt="RockBrwnEarth_A" loading="lazy"><figcaption>RockBrwnEarth_A</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/RockBrwnEarth_B.jpg" alt="RockBrwnEarth_B" loading="lazy"><figcaption>RockBrwnEarth_B</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/RuffDarkSoils_A.jpg" alt="RuffDarkSoils_A" loading="lazy"><figcaption>RuffDarkSoils_A</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/TunnelRock_A.jpg" alt="TunnelRock_A" loading="lazy"><figcaption>TunnelRock_A</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/TunnelRock_B.jpg" alt="TunnelRock_B" loading="lazy"><figcaption>TunnelRock_B</figcaption></figure>
<figure><img src="CoreTexEarth/Earth/un_stat_grvl.jpg" alt="un_stat_grvl" loading="lazy"><figcaption>un_stat_grvl</figcaption></figure>## CoreTexFoliage

### Foliage  
<figure><img src="CoreTexFoliage/Foliage/ClenGrenGrass_A.jpg" alt="ClenGrenGrass_A" loading="lazy"><figcaption>ClenGrenGrass_A</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/DarkGrenGrass_A.jpg" alt="DarkGrenGrass_A" loading="lazy"><figcaption>DarkGrenGrass_A</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/IvyyGrndCover_A.jpg" alt="IvyyGrndCover_A" loading="lazy"><figcaption>IvyyGrndCover_A</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/LiteGrenGrass_A.jpg" alt="LiteGrenGrass_A" loading="lazy"><figcaption>LiteGrenGrass_A</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/LiteVargBushy_A.jpg" alt="LiteVargBushy_A" loading="lazy"><figcaption>LiteVargBushy_A</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/VargGrndCover_A.jpg" alt="VargGrndCover_A" loading="lazy"><figcaption>VargGrndCover_A</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/VargGrndCover_B.jpg" alt="VargGrndCover_B" loading="lazy"><figcaption>VargGrndCover_B</figcaption></figure>
<figure><img src="CoreTexFoliage/Foliage/WildGrenGrass_A.jpg" alt="WildGrenGrass_A" loading="lazy"><figcaption>WildGrenGrass_A</figcaption></figure>## CoreTexGlass

### Glass  
<figure><img src="CoreTexGlass/Glass/07WindOpacStrek.jpg" alt="07WindOpacStrek" loading="lazy"><figcaption>07WindOpacStrek</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/BlocGrenGlass_A.jpg" alt="BlocGrenGlass_A" loading="lazy"><figcaption>BlocGrenGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/ClenGlasBlock_A.jpg" alt="ClenGlasBlock_A" loading="lazy"><figcaption>ClenGlasBlock_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/FrosBlueGlass_A.jpg" alt="FrosBlueGlass_A" loading="lazy"><figcaption>FrosBlueGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/FrosWhitGlass_A.jpg" alt="FrosWhitGlass_A" loading="lazy"><figcaption>FrosWhitGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/LiteGrenGlass_A.jpg" alt="LiteGrenGlass_A" loading="lazy"><figcaption>LiteGrenGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/Mirror_Highligh.jpg" alt="Mirror_Highligh" loading="lazy"><figcaption>Mirror_Highligh</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/OldeGlass_A.jpg" alt="OldeGlass_A" loading="lazy"><figcaption>OldeGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/OldeMultGlass_A.jpg" alt="OldeMultGlass_A" loading="lazy"><figcaption>OldeMultGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/OldeStanGlass_A.jpg" alt="OldeStanGlass_A" loading="lazy"><figcaption>OldeStanGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/OldeStanGlass_B.jpg" alt="OldeStanGlass_B" loading="lazy"><figcaption>OldeStanGlass_B</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/OldeWhitGlass_A.jpg" alt="OldeWhitGlass_A" loading="lazy"><figcaption>OldeWhitGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/PrivBlueGlass_A.jpg" alt="PrivBlueGlass_A" loading="lazy"><figcaption>PrivBlueGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/SafeGrenGlass_A.jpg" alt="SafeGrenGlass_A" loading="lazy"><figcaption>SafeGrenGlass_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/SafeGrenGlass_B.jpg" alt="SafeGrenGlass_B" loading="lazy"><figcaption>SafeGrenGlass_B</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/WindOpacStrek_A.jpg" alt="WindOpacStrek_A" loading="lazy"><figcaption>WindOpacStrek_A</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/WindOpacStrek_B.jpg" alt="WindOpacStrek_B" loading="lazy"><figcaption>WindOpacStrek_B</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/WindOpacStrek_C.jpg" alt="WindOpacStrek_C" loading="lazy"><figcaption>WindOpacStrek_C</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/WindOpacStrek_D.jpg" alt="WindOpacStrek_D" loading="lazy"><figcaption>WindOpacStrek_D</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/WindOpacStrek_E.jpg" alt="WindOpacStrek_E" loading="lazy"><figcaption>WindOpacStrek_E</figcaption></figure>
<figure><img src="CoreTexGlass/Glass/bulltprftrans_a.jpg" alt="bulltprftrans_a" loading="lazy"><figcaption>bulltprftrans_a</figcaption></figure>## CoreTexMetal
<figure><img src="CoreTexMetal/Chainlnk_01.jpg" alt="Chainlnk_01" loading="lazy"><figcaption>Chainlnk_01</figcaption></figure>

### Ladder  
<figure><img src="CoreTexMetal/Ladder/LadrBrwnMetal.jpg" alt="LadrBrwnMetal" loading="lazy"><figcaption>LadrBrwnMetal</figcaption></figure>
<figure><img src="CoreTexMetal/Ladder/ladder_a.jpg" alt="ladder_a" loading="lazy"><figcaption>ladder_a</figcaption></figure>

### Metal  
<figure><img src="CoreTexMetal/Metal/747Hang_B_cnr.jpg" alt="747Hang_B_cnr" loading="lazy"><figcaption>747Hang_B_cnr</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/747Hang_B_cnr2.jpg" alt="747Hang_B_cnr2" loading="lazy"><figcaption>747Hang_B_cnr2</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/747Hang_B_end.jpg" alt="747Hang_B_end" loading="lazy"><figcaption>747Hang_B_end</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/747Hang_B_end2.jpg" alt="747Hang_B_end2" loading="lazy"><figcaption>747Hang_B_end2</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/747Hang_B_hor.jpg" alt="747Hang_B_hor" loading="lazy"><figcaption>747Hang_B_hor</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/747HangerDoor_A.jpg" alt="747HangerDoor_A" loading="lazy"><figcaption>747HangerDoor_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/747HangerDoor_B.jpg" alt="747HangerDoor_B" loading="lazy"><figcaption>747HangerDoor_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Area51Wall_A.jpg" alt="Area51Wall_A" loading="lazy"><figcaption>Area51Wall_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/BoilerDetail_A.jpg" alt="BoilerDetail_A" loading="lazy"><figcaption>BoilerDetail_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/BoilerDetail_B.jpg" alt="BoilerDetail_B" loading="lazy"><figcaption>BoilerDetail_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/BoilerDetail_C.jpg" alt="BoilerDetail_C" loading="lazy"><figcaption>BoilerDetail_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/BoilerDetail_D.jpg" alt="BoilerDetail_D" loading="lazy"><figcaption>BoilerDetail_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/BoilerDetail_E.jpg" alt="BoilerDetail_E" loading="lazy"><figcaption>BoilerDetail_E</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/CT_BnRstMtl_01.jpg" alt="CT_BnRstMtl_01" loading="lazy"><figcaption>CT_BnRstMtl_01</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/CT_RdRstMtl_01.jpg" alt="CT_RdRstMtl_01" loading="lazy"><figcaption>CT_RdRstMtl_01</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenBlueGrte_A.jpg" alt="ClenBlueGrte_A" loading="lazy"><figcaption>ClenBlueGrte_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenBlueSecur_A.jpg" alt="ClenBlueSecur_A" loading="lazy"><figcaption>ClenBlueSecur_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenBrwnBwire_A.jpg" alt="ClenBrwnBwire_A" loading="lazy"><figcaption>ClenBrwnBwire_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenBrwnRwire_A.jpg" alt="ClenBrwnRwire_A" loading="lazy"><figcaption>ClenBrwnRwire_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenChainlink_A.jpg" alt="ClenChainlink_A" loading="lazy"><figcaption>ClenChainlink_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenChainlink_B.jpg" alt="ClenChainlink_B" loading="lazy"><figcaption>ClenChainlink_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenGrayMetal_A.jpg" alt="ClenGrayMetal_A" loading="lazy"><figcaption>ClenGrayMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenGrayMetal_B.jpg" alt="ClenGrayMetal_B" loading="lazy"><figcaption>ClenGrayMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenIronRivet_A.jpg" alt="ClenIronRivet_A" loading="lazy"><figcaption>ClenIronRivet_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenIronRivet_B.jpg" alt="ClenIronRivet_B" loading="lazy"><figcaption>ClenIronRivet_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenIronWOriv_A.jpg" alt="ClenIronWOriv_A" loading="lazy"><figcaption>ClenIronWOriv_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenIronWOriv_C.jpg" alt="ClenIronWOriv_C" loading="lazy"><figcaption>ClenIronWOriv_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenMetlPanel_A.jpg" alt="ClenMetlPanel_A" loading="lazy"><figcaption>ClenMetlPanel_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ClenMetlPatrn_A.jpg" alt="ClenMetlPatrn_A" loading="lazy"><figcaption>ClenMetlPatrn_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/CorgBrasMetal_A.jpg" alt="CorgBrasMetal_A" loading="lazy"><figcaption>CorgBrasMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/CorgGrayMetal_B.jpg" alt="CorgGrayMetal_B" loading="lazy"><figcaption>CorgGrayMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/CorgGrenMetal_B.jpg" alt="CorgGrenMetal_B" loading="lazy"><figcaption>CorgGrenMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/CorgSilvMetal_A.jpg" alt="CorgSilvMetal_A" loading="lazy"><figcaption>CorgSilvMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DamgBlueGrte_A.jpg" alt="DamgBlueGrte_A" loading="lazy"><figcaption>DamgBlueGrte_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DamgBlueMetal_A.jpg" alt="DamgBlueMetal_A" loading="lazy"><figcaption>DamgBlueMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DamgBlueMetal_B.jpg" alt="DamgBlueMetal_B" loading="lazy"><figcaption>DamgBlueMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DamgRustWall__A.jpg" alt="DamgRustWall__A" loading="lazy"><figcaption>DamgRustWall__A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DamgRustWall__B.jpg" alt="DamgRustWall__B" loading="lazy"><figcaption>DamgRustWall__B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DiamPlatSteel_A.jpg" alt="DiamPlatSteel_A" loading="lazy"><figcaption>DiamPlatSteel_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyBlueDumpt_A.jpg" alt="DrtyBlueDumpt_A" loading="lazy"><figcaption>DrtyBlueDumpt_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronPlain_A.jpg" alt="DrtyIronPlain_A" loading="lazy"><figcaption>DrtyIronPlain_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronPlain_B.jpg" alt="DrtyIronPlain_B" loading="lazy"><figcaption>DrtyIronPlain_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronPlain_C.jpg" alt="DrtyIronPlain_C" loading="lazy"><figcaption>DrtyIronPlain_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronRevet_A.jpg" alt="DrtyIronRevet_A" loading="lazy"><figcaption>DrtyIronRevet_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronRevet_B.jpg" alt="DrtyIronRevet_B" loading="lazy"><figcaption>DrtyIronRevet_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronWOriv_A.jpg" alt="DrtyIronWOriv_A" loading="lazy"><figcaption>DrtyIronWOriv_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronWOriv_B.jpg" alt="DrtyIronWOriv_B" loading="lazy"><figcaption>DrtyIronWOriv_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyIronWOriv_C.jpg" alt="DrtyIronWOriv_C" loading="lazy"><figcaption>DrtyIronWOriv_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetalWire_A.jpg" alt="DrtyMetalWire_A" loading="lazy"><figcaption>DrtyMetalWire_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetalWire_B.jpg" alt="DrtyMetalWire_B" loading="lazy"><figcaption>DrtyMetalWire_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlDoor_A.jpg" alt="DrtyMetlDoor_A" loading="lazy"><figcaption>DrtyMetlDoor_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlDoor_B.jpg" alt="DrtyMetlDoor_B" loading="lazy"><figcaption>DrtyMetlDoor_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlDoor_C.jpg" alt="DrtyMetlDoor_C" loading="lazy"><figcaption>DrtyMetlDoor_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlDoor_D.jpg" alt="DrtyMetlDoor_D" loading="lazy"><figcaption>DrtyMetlDoor_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlDoor_E.jpg" alt="DrtyMetlDoor_E" loading="lazy"><figcaption>DrtyMetlDoor_E</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlDoor_F.jpg" alt="DrtyMetlDoor_F" loading="lazy"><figcaption>DrtyMetlDoor_F</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlRailn_A.jpg" alt="DrtyMetlRailn_A" loading="lazy"><figcaption>DrtyMetlRailn_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtyMetlRailn_D.jpg" alt="DrtyMetlRailn_D" loading="lazy"><figcaption>DrtyMetlRailn_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/DrtySilvMetal_A.jpg" alt="DrtySilvMetal_A" loading="lazy"><figcaption>DrtySilvMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/FileCabnDrawr_A.jpg" alt="FileCabnDrawr_A" loading="lazy"><figcaption>FileCabnDrawr_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/FileCabnDrawr_B.jpg" alt="FileCabnDrawr_B" loading="lazy"><figcaption>FileCabnDrawr_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/FileCabn_Body_A.jpg" alt="FileCabn_Body_A" loading="lazy"><figcaption>FileCabn_Body_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/FileCabn_Body_B.jpg" alt="FileCabn_Body_B" loading="lazy"><figcaption>FileCabn_Body_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Galvanzd.jpg" alt="Galvanzd" loading="lazy"><figcaption>Galvanzd</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Galvanzd_B.jpg" alt="Galvanzd_B" loading="lazy"><figcaption>Galvanzd_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Galvanzd_C.jpg" alt="Galvanzd_C" loading="lazy"><figcaption>Galvanzd_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Galvanzd_D.jpg" alt="Galvanzd_D" loading="lazy"><figcaption>Galvanzd_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/GripMetlFloor_A.jpg" alt="GripMetlFloor_A" loading="lazy"><figcaption>GripMetlFloor_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HangrMetlDoor_A.jpg" alt="HangrMetlDoor_A" loading="lazy"><figcaption>HangrMetlDoor_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HangrMetlDoor_B.jpg" alt="HangrMetlDoor_B" loading="lazy"><figcaption>HangrMetlDoor_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HangrMetlWall_A.jpg" alt="HangrMetlWall_A" loading="lazy"><figcaption>HangrMetlWall_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HangrMetlWall_B.jpg" alt="HangrMetlWall_B" loading="lazy"><figcaption>HangrMetlWall_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HangrMetlWall_D.jpg" alt="HangrMetlWall_D" loading="lazy"><figcaption>HangrMetlWall_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HangrMetlWall_E.jpg" alt="HangrMetlWall_E" loading="lazy"><figcaption>HangrMetlWall_E</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Heli_LiftDetl_B.jpg" alt="Heli_LiftDetl_B" loading="lazy"><figcaption>Heli_LiftDetl_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/Heli_LiftMetl_A.jpg" alt="Heli_LiftMetl_A" loading="lazy"><figcaption>Heli_LiftMetl_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HevyMetlGrate_A.jpg" alt="HevyMetlGrate_A" loading="lazy"><figcaption>HevyMetlGrate_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HevyMetlGrate_B.jpg" alt="HevyMetlGrate_B" loading="lazy"><figcaption>HevyMetlGrate_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/HevyMetlGrate_C.jpg" alt="HevyMetlGrate_C" loading="lazy"><figcaption>HevyMetlGrate_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronReddCircl_A.jpg" alt="IronReddCircl_A" loading="lazy"><figcaption>IronReddCircl_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronReddEdges_A.jpg" alt="IronReddEdges_A" loading="lazy"><figcaption>IronReddEdges_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronReddPlain_A.jpg" alt="IronReddPlain_A" loading="lazy"><figcaption>IronReddPlain_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronReddPlain_B.jpg" alt="IronReddPlain_B" loading="lazy"><figcaption>IronReddPlain_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronYeloCircl_A.jpg" alt="IronYeloCircl_A" loading="lazy"><figcaption>IronYeloCircl_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronYeloEdges_A.jpg" alt="IronYeloEdges_A" loading="lazy"><figcaption>IronYeloEdges_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronYeloPlain_A.jpg" alt="IronYeloPlain_A" loading="lazy"><figcaption>IronYeloPlain_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/IronYeloPlain_B.jpg" alt="IronYeloPlain_B" loading="lazy"><figcaption>IronYeloPlain_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MFuelBarlTop_A.jpg" alt="MFuelBarlTop_A" loading="lazy"><figcaption>MFuelBarlTop_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MTopStorBarrl_A.jpg" alt="MTopStorBarrl_A" loading="lazy"><figcaption>MTopStorBarrl_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MTopStorBarrl_D.jpg" alt="MTopStorBarrl_D" loading="lazy"><figcaption>MTopStorBarrl_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MarshCrane_A.jpg" alt="MarshCrane_A" loading="lazy"><figcaption>MarshCrane_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MarshCrane_B.jpg" alt="MarshCrane_B" loading="lazy"><figcaption>MarshCrane_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MarshCrane_C.jpg" alt="MarshCrane_C" loading="lazy"><figcaption>MarshCrane_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetalStepEdge_A.jpg" alt="MetalStepEdge_A" loading="lazy"><figcaption>MetalStepEdge_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetalStepEdge_B.jpg" alt="MetalStepEdge_B" loading="lazy"><figcaption>MetalStepEdge_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlBoxPanel_A.jpg" alt="MetlBoxPanel_A" loading="lazy"><figcaption>MetlBoxPanel_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlBoxPanel_B.jpg" alt="MetlBoxPanel_B" loading="lazy"><figcaption>MetlBoxPanel_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlBoxPanel_C.jpg" alt="MetlBoxPanel_C" loading="lazy"><figcaption>MetlBoxPanel_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlBoxPanel_D.jpg" alt="MetlBoxPanel_D" loading="lazy"><figcaption>MetlBoxPanel_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlFuelBarrl_A.jpg" alt="MetlFuelBarrl_A" loading="lazy"><figcaption>MetlFuelBarrl_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlStorBarrl_B.jpg" alt="MetlStorBarrl_B" loading="lazy"><figcaption>MetlStorBarrl_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlStorBarrl_C.jpg" alt="MetlStorBarrl_C" loading="lazy"><figcaption>MetlStorBarrl_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlStorBarrl_D.jpg" alt="MetlStorBarrl_D" loading="lazy"><figcaption>MetlStorBarrl_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/MetlStorBarrl_E.jpg" alt="MetlStorBarrl_E" loading="lazy"><figcaption>MetlStorBarrl_E</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/NYCmanholecov_A.jpg" alt="NYCmanholecov_A" loading="lazy"><figcaption>NYCmanholecov_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/OldBrasMCvent_A.jpg" alt="OldBrasMCvent_A" loading="lazy"><figcaption>OldBrasMCvent_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/OldBrasMCvent_B.jpg" alt="OldBrasMCvent_B" loading="lazy"><figcaption>OldBrasMCvent_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/OldeIronFence_A.jpg" alt="OldeIronFence_A" loading="lazy"><figcaption>OldeIronFence_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/OldeIronFence_B.jpg" alt="OldeIronFence_B" loading="lazy"><figcaption>OldeIronFence_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/OldeIronFence_C.jpg" alt="OldeIronFence_C" loading="lazy"><figcaption>OldeIronFence_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PantBrwmScafo_A.jpg" alt="PantBrwmScafo_A" loading="lazy"><figcaption>PantBrwmScafo_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PantBrwmScafo_B.jpg" alt="PantBrwmScafo_B" loading="lazy"><figcaption>PantBrwmScafo_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PitdBlueMetal_A.jpg" alt="PitdBlueMetal_A" loading="lazy"><figcaption>PitdBlueMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PitdBlueMetal_B.jpg" alt="PitdBlueMetal_B" loading="lazy"><figcaption>PitdBlueMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PitdBrwnMetal.jpg" alt="PitdBrwnMetal" loading="lazy"><figcaption>PitdBrwnMetal</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PitdBrwnMetal_B.jpg" alt="PitdBrwnMetal_B" loading="lazy"><figcaption>PitdBrwnMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PitdSilvMetal_A.jpg" alt="PitdSilvMetal_A" loading="lazy"><figcaption>PitdSilvMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PitdSilvMetal_B.jpg" alt="PitdSilvMetal_B" loading="lazy"><figcaption>PitdSilvMetal_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/PlatMetlGrate_A.jpg" alt="PlatMetlGrate_A" loading="lazy"><figcaption>PlatMetlGrate_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/RadioActBarrl_A.jpg" alt="RadioActBarrl_A" loading="lazy"><figcaption>RadioActBarrl_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/RadioActBarrl_B.jpg" alt="RadioActBarrl_B" loading="lazy"><figcaption>RadioActBarrl_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/RustBlueWall__A.jpg" alt="RustBlueWall__A" loading="lazy"><figcaption>RustBlueWall__A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/RustMetlVents_A.jpg" alt="RustMetlVents_A" loading="lazy"><figcaption>RustMetlVents_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/SemiBackDoor_B.jpg" alt="SemiBackDoor_B" loading="lazy"><figcaption>SemiBackDoor_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipBeigMetal_A.jpg" alt="ShipBeigMetal_A" loading="lazy"><figcaption>ShipBeigMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipBrwnPlate_A.jpg" alt="ShipBrwnPlate_A" loading="lazy"><figcaption>ShipBrwnPlate_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipBrwnPlate_C.jpg" alt="ShipBrwnPlate_C" loading="lazy"><figcaption>ShipBrwnPlate_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipBrwnPlate_D.jpg" alt="ShipBrwnPlate_D" loading="lazy"><figcaption>ShipBrwnPlate_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipBrwnPlate_E.jpg" alt="ShipBrwnPlate_E" loading="lazy"><figcaption>ShipBrwnPlate_E</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipBrwnPlate_G.jpg" alt="ShipBrwnPlate_G" loading="lazy"><figcaption>ShipBrwnPlate_G</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipCombRivet_A.jpg" alt="ShipCombRivet_A" loading="lazy"><figcaption>ShipCombRivet_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipConduPipe_A.jpg" alt="ShipConduPipe_A" loading="lazy"><figcaption>ShipConduPipe_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipCreaMetal_A.jpg" alt="ShipCreaMetal_A" loading="lazy"><figcaption>ShipCreaMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipGrayMetal_A.jpg" alt="ShipGrayMetal_A" loading="lazy"><figcaption>ShipGrayMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipHorzIBeam_A.jpg" alt="ShipHorzIBeam_A" loading="lazy"><figcaption>ShipHorzIBeam_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipNamePlate_B.jpg" alt="ShipNamePlate_B" loading="lazy"><figcaption>ShipNamePlate_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipNamePlate_C.jpg" alt="ShipNamePlate_C" loading="lazy"><figcaption>ShipNamePlate_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipNamePlate_D.jpg" alt="ShipNamePlate_D" loading="lazy"><figcaption>ShipNamePlate_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipReddPlate_A.jpg" alt="ShipReddPlate_A" loading="lazy"><figcaption>ShipReddPlate_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipVertIBeam_A.jpg" alt="ShipVertIBeam_A" loading="lazy"><figcaption>ShipVertIBeam_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipVertIBeam_B.jpg" alt="ShipVertIBeam_B" loading="lazy"><figcaption>ShipVertIBeam_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipWhitMetal_A.jpg" alt="ShipWhitMetal_A" loading="lazy"><figcaption>ShipWhitMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipWhitMetal_C.jpg" alt="ShipWhitMetal_C" loading="lazy"><figcaption>ShipWhitMetal_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipWhitPlate_A.jpg" alt="ShipWhitPlate_A" loading="lazy"><figcaption>ShipWhitPlate_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipWindoTrim_A.jpg" alt="ShipWindoTrim_A" loading="lazy"><figcaption>ShipWindoTrim_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipYard_Box_A.jpg" alt="ShipYard_Box_A" loading="lazy"><figcaption>ShipYard_Box_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipYard_Box_B.jpg" alt="ShipYard_Box_B" loading="lazy"><figcaption>ShipYard_Box_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipYard_Box_C.jpg" alt="ShipYard_Box_C" loading="lazy"><figcaption>ShipYard_Box_C</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipYard_Box_D.jpg" alt="ShipYard_Box_D" loading="lazy"><figcaption>ShipYard_Box_D</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipYard_Box_E.jpg" alt="ShipYard_Box_E" loading="lazy"><figcaption>ShipYard_Box_E</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipsCeiln_B.jpg" alt="ShipsCeiln_B" loading="lazy"><figcaption>ShipsCeiln_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipsHatch_A.jpg" alt="ShipsHatch_A" loading="lazy"><figcaption>ShipsHatch_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipsHatch_A2.jpg" alt="ShipsHatch_A2" loading="lazy"><figcaption>ShipsHatch_A2</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipsHatch_B.jpg" alt="ShipsHatch_B" loading="lazy"><figcaption>ShipsHatch_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/ShipsHatch_B2.jpg" alt="ShipsHatch_B2" loading="lazy"><figcaption>ShipsHatch_B2</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/SmthBlueMetal_A.jpg" alt="SmthBlueMetal_A" loading="lazy"><figcaption>SmthBlueMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/StanGalvMetal.jpg" alt="StanGalvMetal" loading="lazy"><figcaption>StanGalvMetal</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/StanGrayMetal_A.jpg" alt="StanGrayMetal_A" loading="lazy"><figcaption>StanGrayMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/StanHangMetal_A.jpg" alt="StanHangMetal_A" loading="lazy"><figcaption>StanHangMetal_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/WireMeshWalkr_A.jpg" alt="WireMeshWalkr_A" loading="lazy"><figcaption>WireMeshWalkr_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/WireMeshWalkr_B.jpg" alt="WireMeshWalkr_B" loading="lazy"><figcaption>WireMeshWalkr_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/WthrBrnzBldgD_A.jpg" alt="WthrBrnzBldgD_A" loading="lazy"><figcaption>WthrBrnzBldgD_A</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/WthrBrnzBldgD_B.jpg" alt="WthrBrnzBldgD_B" loading="lazy"><figcaption>WthrBrnzBldgD_B</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/bathrmstall_a.jpg" alt="bathrmstall_a" loading="lazy"><figcaption>bathrmstall_a</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/bathrmstall_b.jpg" alt="bathrmstall_b" loading="lazy"><figcaption>bathrmstall_b</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/metalgrate_a.jpg" alt="metalgrate_a" loading="lazy"><figcaption>metalgrate_a</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/metalshelf_a.jpg" alt="metalshelf_a" loading="lazy"><figcaption>metalshelf_a</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/metalwall_a.jpg" alt="metalwall_a" loading="lazy"><figcaption>metalwall_a</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/racksides.jpg" alt="racksides" loading="lazy"><figcaption>racksides</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/waterfontn_a.jpg" alt="waterfontn_a" loading="lazy"><figcaption>waterfontn_a</figcaption></figure>
<figure><img src="CoreTexMetal/Metal/waterfontn_b.jpg" alt="waterfontn_b" loading="lazy"><figcaption>waterfontn_b</figcaption></figure>
<figure><img src="CoreTexMetal/PlatMetlGrate_B.jpg" alt="PlatMetlGrate_B" loading="lazy"><figcaption>PlatMetlGrate_B</figcaption></figure>## CoreTexMisc
<figure><img src="CoreTexMisc/BW_LG_SatPic.jpg" alt="BW_LG_SatPic" loading="lazy"><figcaption>BW_LG_SatPic</figcaption></figure>
<figure><img src="CoreTexMisc/BW_LG_SatPic_B.jpg" alt="BW_LG_SatPic_B" loading="lazy"><figcaption>BW_LG_SatPic_B</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_A00.jpg" alt="BW_SatPic_A00" loading="lazy"><figcaption>BW_SatPic_A00</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_A01.jpg" alt="BW_SatPic_A01" loading="lazy"><figcaption>BW_SatPic_A01</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_A02.jpg" alt="BW_SatPic_A02" loading="lazy"><figcaption>BW_SatPic_A02</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_A03.jpg" alt="BW_SatPic_A03" loading="lazy"><figcaption>BW_SatPic_A03</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_B00.jpg" alt="BW_SatPic_B00" loading="lazy"><figcaption>BW_SatPic_B00</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_B01.jpg" alt="BW_SatPic_B01" loading="lazy"><figcaption>BW_SatPic_B01</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_B02.jpg" alt="BW_SatPic_B02" loading="lazy"><figcaption>BW_SatPic_B02</figcaption></figure>
<figure><img src="CoreTexMisc/BW_SatPic_B03.jpg" alt="BW_SatPic_B03" loading="lazy"><figcaption>BW_SatPic_B03</figcaption></figure>

### Glass  
<figure><img src="CoreTexMisc/Glass/BW_LG_SatPic.jpg" alt="BW_LG_SatPic" loading="lazy"><figcaption>BW_LG_SatPic</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/BW_LG_SatPic_B.jpg" alt="BW_LG_SatPic_B" loading="lazy"><figcaption>BW_LG_SatPic_B</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/BW_SatPic_A00.jpg" alt="BW_SatPic_A00" loading="lazy"><figcaption>BW_SatPic_A00</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/BW_SatPic_A01.jpg" alt="BW_SatPic_A01" loading="lazy"><figcaption>BW_SatPic_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/BW_SatPic_A02.jpg" alt="BW_SatPic_A02" loading="lazy"><figcaption>BW_SatPic_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/BW_SatPic_A03.jpg" alt="BW_SatPic_A03" loading="lazy"><figcaption>BW_SatPic_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/BW_SatPic_B00.jpg" alt="BW_SatPic_B00" loading="lazy"><figcaption>BW_SatPic_B00</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_01.jpg" alt="ComptrPanel_01" loading="lazy"><figcaption>ComptrPanel_01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_02.jpg" alt="ComptrPanel_02" loading="lazy"><figcaption>ComptrPanel_02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_03.jpg" alt="ComptrPanel_03" loading="lazy"><figcaption>ComptrPanel_03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_04.jpg" alt="ComptrPanel_04" loading="lazy"><figcaption>ComptrPanel_04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_05.jpg" alt="ComptrPanel_05" loading="lazy"><figcaption>ComptrPanel_05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_06.jpg" alt="ComptrPanel_06" loading="lazy"><figcaption>ComptrPanel_06</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_07.jpg" alt="ComptrPanel_07" loading="lazy"><figcaption>ComptrPanel_07</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_08.jpg" alt="ComptrPanel_08" loading="lazy"><figcaption>ComptrPanel_08</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_09.jpg" alt="ComptrPanel_09" loading="lazy"><figcaption>ComptrPanel_09</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_10.jpg" alt="ComptrPanel_10" loading="lazy"><figcaption>ComptrPanel_10</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/ComptrPanel_11.jpg" alt="ComptrPanel_11" loading="lazy"><figcaption>ComptrPanel_11</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Cycle_Yelo_A01.jpg" alt="Cycle_Yelo_A01" loading="lazy"><figcaption>Cycle_Yelo_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Cycle_Yelo_A02.jpg" alt="Cycle_Yelo_A02" loading="lazy"><figcaption>Cycle_Yelo_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Cycle_Yelo_A03.jpg" alt="Cycle_Yelo_A03" loading="lazy"><figcaption>Cycle_Yelo_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Cycle_Yelo_A04.jpg" alt="Cycle_Yelo_A04" loading="lazy"><figcaption>Cycle_Yelo_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Cycle_Yelo_A05.jpg" alt="Cycle_Yelo_A05" loading="lazy"><figcaption>Cycle_Yelo_A05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Flash_Yelo_A01.jpg" alt="Flash_Yelo_A01" loading="lazy"><figcaption>Flash_Yelo_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Flash_Yelo_A02.jpg" alt="Flash_Yelo_A02" loading="lazy"><figcaption>Flash_Yelo_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Flash_Yelo_A03.jpg" alt="Flash_Yelo_A03" loading="lazy"><figcaption>Flash_Yelo_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Flash_Yelo_A04.jpg" alt="Flash_Yelo_A04" loading="lazy"><figcaption>Flash_Yelo_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/GenrFactWindo_A.jpg" alt="GenrFactWindo_A" loading="lazy"><figcaption>GenrFactWindo_A</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/GenrFactWindo_B.jpg" alt="GenrFactWindo_B" loading="lazy"><figcaption>GenrFactWindo_B</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/GenrFactWindo_C.jpg" alt="GenrFactWindo_C" loading="lazy"><figcaption>GenrFactWindo_C</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/GenrOffcWindo_A.jpg" alt="GenrOffcWindo_A" loading="lazy"><figcaption>GenrOffcWindo_A</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A00.jpg" alt="H_noise_A00" loading="lazy"><figcaption>H_noise_A00</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A01.jpg" alt="H_noise_A01" loading="lazy"><figcaption>H_noise_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A02.jpg" alt="H_noise_A02" loading="lazy"><figcaption>H_noise_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A03.jpg" alt="H_noise_A03" loading="lazy"><figcaption>H_noise_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A04.jpg" alt="H_noise_A04" loading="lazy"><figcaption>H_noise_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A05.jpg" alt="H_noise_A05" loading="lazy"><figcaption>H_noise_A05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A06.jpg" alt="H_noise_A06" loading="lazy"><figcaption>H_noise_A06</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A07.jpg" alt="H_noise_A07" loading="lazy"><figcaption>H_noise_A07</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A08.jpg" alt="H_noise_A08" loading="lazy"><figcaption>H_noise_A08</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A09.jpg" alt="H_noise_A09" loading="lazy"><figcaption>H_noise_A09</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/H_noise_A10.jpg" alt="H_noise_A10" loading="lazy"><figcaption>H_noise_A10</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/LED_Pad_A01.jpg" alt="LED_Pad_A01" loading="lazy"><figcaption>LED_Pad_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/LED_Pad_A02.jpg" alt="LED_Pad_A02" loading="lazy"><figcaption>LED_Pad_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/LED_Pad_A03.jpg" alt="LED_Pad_A03" loading="lazy"><figcaption>LED_Pad_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/LED_Pad_A04.jpg" alt="LED_Pad_A04" loading="lazy"><figcaption>LED_Pad_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_A01.jpg" alt="Med_Screen_A01" loading="lazy"><figcaption>Med_Screen_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_A02.jpg" alt="Med_Screen_A02" loading="lazy"><figcaption>Med_Screen_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_A03.jpg" alt="Med_Screen_A03" loading="lazy"><figcaption>Med_Screen_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_A04.jpg" alt="Med_Screen_A04" loading="lazy"><figcaption>Med_Screen_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_A05.jpg" alt="Med_Screen_A05" loading="lazy"><figcaption>Med_Screen_A05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_B01.jpg" alt="Med_Screen_B01" loading="lazy"><figcaption>Med_Screen_B01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_B02.jpg" alt="Med_Screen_B02" loading="lazy"><figcaption>Med_Screen_B02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_B03.jpg" alt="Med_Screen_B03" loading="lazy"><figcaption>Med_Screen_B03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_B04.jpg" alt="Med_Screen_B04" loading="lazy"><figcaption>Med_Screen_B04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Med_Screen_B05.jpg" alt="Med_Screen_B05" loading="lazy"><figcaption>Med_Screen_B05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A01.jpg" alt="News_A01" loading="lazy"><figcaption>News_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A02.jpg" alt="News_A02" loading="lazy"><figcaption>News_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A03.jpg" alt="News_A03" loading="lazy"><figcaption>News_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A04.jpg" alt="News_A04" loading="lazy"><figcaption>News_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A05.jpg" alt="News_A05" loading="lazy"><figcaption>News_A05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A06.jpg" alt="News_A06" loading="lazy"><figcaption>News_A06</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/News_A07.jpg" alt="News_A07" loading="lazy"><figcaption>News_A07</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_A01.jpg" alt="Obs_Screen_A01" loading="lazy"><figcaption>Obs_Screen_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_A02.jpg" alt="Obs_Screen_A02" loading="lazy"><figcaption>Obs_Screen_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_A03.jpg" alt="Obs_Screen_A03" loading="lazy"><figcaption>Obs_Screen_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_A04.jpg" alt="Obs_Screen_A04" loading="lazy"><figcaption>Obs_Screen_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_B01.jpg" alt="Obs_Screen_B01" loading="lazy"><figcaption>Obs_Screen_B01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_B02.jpg" alt="Obs_Screen_B02" loading="lazy"><figcaption>Obs_Screen_B02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_B03.jpg" alt="Obs_Screen_B03" loading="lazy"><figcaption>Obs_Screen_B03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_B04.jpg" alt="Obs_Screen_B04" loading="lazy"><figcaption>Obs_Screen_B04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_C01.jpg" alt="Obs_Screen_C01" loading="lazy"><figcaption>Obs_Screen_C01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_C02.jpg" alt="Obs_Screen_C02" loading="lazy"><figcaption>Obs_Screen_C02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_C03.jpg" alt="Obs_Screen_C03" loading="lazy"><figcaption>Obs_Screen_C03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_C04.jpg" alt="Obs_Screen_C04" loading="lazy"><figcaption>Obs_Screen_C04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_D01.jpg" alt="Obs_Screen_D01" loading="lazy"><figcaption>Obs_Screen_D01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_D02.jpg" alt="Obs_Screen_D02" loading="lazy"><figcaption>Obs_Screen_D02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_D03.jpg" alt="Obs_Screen_D03" loading="lazy"><figcaption>Obs_Screen_D03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_D04.jpg" alt="Obs_Screen_D04" loading="lazy"><figcaption>Obs_Screen_D04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_E01.jpg" alt="Obs_Screen_E01" loading="lazy"><figcaption>Obs_Screen_E01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_E02.jpg" alt="Obs_Screen_E02" loading="lazy"><figcaption>Obs_Screen_E02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_E03.jpg" alt="Obs_Screen_E03" loading="lazy"><figcaption>Obs_Screen_E03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Obs_Screen_E04.jpg" alt="Obs_Screen_E04" loading="lazy"><figcaption>Obs_Screen_E04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A01.jpg" alt="Stoc_A01" loading="lazy"><figcaption>Stoc_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A02.jpg" alt="Stoc_A02" loading="lazy"><figcaption>Stoc_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A03.jpg" alt="Stoc_A03" loading="lazy"><figcaption>Stoc_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A04.jpg" alt="Stoc_A04" loading="lazy"><figcaption>Stoc_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A05.jpg" alt="Stoc_A05" loading="lazy"><figcaption>Stoc_A05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A06.jpg" alt="Stoc_A06" loading="lazy"><figcaption>Stoc_A06</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_A07.jpg" alt="Stoc_A07" loading="lazy"><figcaption>Stoc_A07</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B01.jpg" alt="Stoc_B01" loading="lazy"><figcaption>Stoc_B01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B02.jpg" alt="Stoc_B02" loading="lazy"><figcaption>Stoc_B02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B03.jpg" alt="Stoc_B03" loading="lazy"><figcaption>Stoc_B03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B04.jpg" alt="Stoc_B04" loading="lazy"><figcaption>Stoc_B04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B05.jpg" alt="Stoc_B05" loading="lazy"><figcaption>Stoc_B05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B06.jpg" alt="Stoc_B06" loading="lazy"><figcaption>Stoc_B06</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Stoc_B07.jpg" alt="Stoc_B07" loading="lazy"><figcaption>Stoc_B07</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/TVMonitor_A1.jpg" alt="TVMonitor_A1" loading="lazy"><figcaption>TVMonitor_A1</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Grn_A01.jpg" alt="Type_Grn_A01" loading="lazy"><figcaption>Type_Grn_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Grn_A02.jpg" alt="Type_Grn_A02" loading="lazy"><figcaption>Type_Grn_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Grn_A03.jpg" alt="Type_Grn_A03" loading="lazy"><figcaption>Type_Grn_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Grn_A04.jpg" alt="Type_Grn_A04" loading="lazy"><figcaption>Type_Grn_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Red_A01.jpg" alt="Type_Red_A01" loading="lazy"><figcaption>Type_Red_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Red_A02.jpg" alt="Type_Red_A02" loading="lazy"><figcaption>Type_Red_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Red_A03.jpg" alt="Type_Red_A03" loading="lazy"><figcaption>Type_Red_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_Red_A04.jpg" alt="Type_Red_A04" loading="lazy"><figcaption>Type_Red_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_blu_A01.jpg" alt="Type_blu_A01" loading="lazy"><figcaption>Type_blu_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_blu_A02.jpg" alt="Type_blu_A02" loading="lazy"><figcaption>Type_blu_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_blu_A03.jpg" alt="Type_blu_A03" loading="lazy"><figcaption>Type_blu_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/Type_blu_A04.jpg" alt="Type_blu_A04" loading="lazy"><figcaption>Type_blu_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/WhiteNoise_A01.jpg" alt="WhiteNoise_A01" loading="lazy"><figcaption>WhiteNoise_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/WhiteNoise_A02.jpg" alt="WhiteNoise_A02" loading="lazy"><figcaption>WhiteNoise_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/WhiteNoise_A03.jpg" alt="WhiteNoise_A03" loading="lazy"><figcaption>WhiteNoise_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/WhiteNoise_B01.jpg" alt="WhiteNoise_B01" loading="lazy"><figcaption>WhiteNoise_B01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/WhiteNoise_B02.jpg" alt="WhiteNoise_B02" loading="lazy"><figcaption>WhiteNoise_B02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/WhiteNoise_B03.jpg" alt="WhiteNoise_B03" loading="lazy"><figcaption>WhiteNoise_B03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A00.jpg" alt="radar_A00" loading="lazy"><figcaption>radar_A00</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A01.jpg" alt="radar_A01" loading="lazy"><figcaption>radar_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A02.jpg" alt="radar_A02" loading="lazy"><figcaption>radar_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A03.jpg" alt="radar_A03" loading="lazy"><figcaption>radar_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A04.jpg" alt="radar_A04" loading="lazy"><figcaption>radar_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A05.jpg" alt="radar_A05" loading="lazy"><figcaption>radar_A05</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A06.jpg" alt="radar_A06" loading="lazy"><figcaption>radar_A06</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A07.jpg" alt="radar_A07" loading="lazy"><figcaption>radar_A07</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A08.jpg" alt="radar_A08" loading="lazy"><figcaption>radar_A08</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A09.jpg" alt="radar_A09" loading="lazy"><figcaption>radar_A09</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A10.jpg" alt="radar_A10" loading="lazy"><figcaption>radar_A10</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A11.jpg" alt="radar_A11" loading="lazy"><figcaption>radar_A11</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A12.jpg" alt="radar_A12" loading="lazy"><figcaption>radar_A12</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A13.jpg" alt="radar_A13" loading="lazy"><figcaption>radar_A13</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A14.jpg" alt="radar_A14" loading="lazy"><figcaption>radar_A14</figcaption></figure>
<figure><img src="CoreTexMisc/Glass/radar_A15.jpg" alt="radar_A15" loading="lazy"><figcaption>radar_A15</figcaption></figure>
<figure><img src="CoreTexMisc/Marker.jpg" alt="Marker" loading="lazy"><figcaption>Marker</figcaption></figure>
<figure><img src="CoreTexMisc/Marker_sky.jpg" alt="Marker_sky" loading="lazy"><figcaption>Marker_sky</figcaption></figure>
<figure><img src="CoreTexMisc/MedChart_A.jpg" alt="MedChart_A" loading="lazy"><figcaption>MedChart_A</figcaption></figure>
<figure><img src="CoreTexMisc/MedChart_C.jpg" alt="MedChart_C" loading="lazy"><figcaption>MedChart_C</figcaption></figure>

### Metal  
<figure><img src="CoreTexMisc/Metal/Area_51_Signs_A.jpg" alt="Area_51_Signs_A" loading="lazy"><figcaption>Area_51_Signs_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ArrowB&WDirec_A.jpg" alt="ArrowB&WDirec_A" loading="lazy"><figcaption>ArrowB&WDirec_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/CautHardHats_A.jpg" alt="CautHardHats_A" loading="lazy"><figcaption>CautHardHats_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/CautHearProtc_A.jpg" alt="CautHearProtc_A" loading="lazy"><figcaption>CautHearProtc_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/CautStepWatch_A.jpg" alt="CautStepWatch_A" loading="lazy"><figcaption>CautStepWatch_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Compu_A00.jpg" alt="Compu_A00" loading="lazy"><figcaption>Compu_A00</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Compu_A01.jpg" alt="Compu_A01" loading="lazy"><figcaption>Compu_A01</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Compu_A02.jpg" alt="Compu_A02" loading="lazy"><figcaption>Compu_A02</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Compu_A03.jpg" alt="Compu_A03" loading="lazy"><figcaption>Compu_A03</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Compu_A04.jpg" alt="Compu_A04" loading="lazy"><figcaption>Compu_A04</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/DangDoNoEnter_A.jpg" alt="DangDoNoEnter_A" loading="lazy"><figcaption>DangDoNoEnter_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/DangHazdSpace_A.jpg" alt="DangHazdSpace_A" loading="lazy"><figcaption>DangHazdSpace_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/DangHazdSpace_B.jpg" alt="DangHazdSpace_B" loading="lazy"><figcaption>DangHazdSpace_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/DangerNOsmoke_A.jpg" alt="DangerNOsmoke_A" loading="lazy"><figcaption>DangerNOsmoke_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Danger_HOT_A.jpg" alt="Danger_HOT_A" loading="lazy"><figcaption>Danger_HOT_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElecGreyCondu_A.jpg" alt="ElecGreyCondu_A" loading="lazy"><figcaption>ElecGreyCondu_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElecHardware_A.jpg" alt="ElecHardware_A" loading="lazy"><figcaption>ElecHardware_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElecHardware_B.jpg" alt="ElecHardware_B" loading="lazy"><figcaption>ElecHardware_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_A.jpg" alt="ElevatorShaft_A" loading="lazy"><figcaption>ElevatorShaft_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_B.jpg" alt="ElevatorShaft_B" loading="lazy"><figcaption>ElevatorShaft_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_C.jpg" alt="ElevatorShaft_C" loading="lazy"><figcaption>ElevatorShaft_C</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_D.jpg" alt="ElevatorShaft_D" loading="lazy"><figcaption>ElevatorShaft_D</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_E.jpg" alt="ElevatorShaft_E" loading="lazy"><figcaption>ElevatorShaft_E</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_F.jpg" alt="ElevatorShaft_F" loading="lazy"><figcaption>ElevatorShaft_F</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/ElevatorShaft_g.jpg" alt="ElevatorShaft_g" loading="lazy"><figcaption>ElevatorShaft_g</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Exitsign_a.jpg" alt="Exitsign_a" loading="lazy"><figcaption>Exitsign_a</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Exitsign_b.jpg" alt="Exitsign_b" loading="lazy"><figcaption>Exitsign_b</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Exitsign_c.jpg" alt="Exitsign_c" loading="lazy"><figcaption>Exitsign_c</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/FullVendMachn_A.jpg" alt="FullVendMachn_A" loading="lazy"><figcaption>FullVendMachn_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/FullVendMachn_B.jpg" alt="FullVendMachn_B" loading="lazy"><figcaption>FullVendMachn_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/KEYBOARD4.jpg" alt="KEYBOARD4" loading="lazy"><figcaption>KEYBOARD4</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/PageToolSign_A.jpg" alt="PageToolSign_A" loading="lazy"><figcaption>PageToolSign_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/PageToolSign_B.jpg" alt="PageToolSign_B" loading="lazy"><figcaption>PageToolSign_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/RadioTransmitr.jpg" alt="RadioTransmitr" loading="lazy"><figcaption>RadioTransmitr</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Railwayfloor01.jpg" alt="Railwayfloor01" loading="lazy"><figcaption>Railwayfloor01</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Railwaytrack01.jpg" alt="Railwaytrack01" loading="lazy"><figcaption>Railwaytrack01</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Railwaytrack02.jpg" alt="Railwaytrack02" loading="lazy"><figcaption>Railwaytrack02</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Railwaywall01.jpg" alt="Railwaywall01" loading="lazy"><figcaption>Railwaywall01</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/Railwaywall02.jpg" alt="Railwaywall02" loading="lazy"><figcaption>Railwaywall02</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/RestRoomSign_A.jpg" alt="RestRoomSign_A" loading="lazy"><figcaption>RestRoomSign_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/StovCookTop_A.jpg" alt="StovCookTop_A" loading="lazy"><figcaption>StovCookTop_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/StovCookTop_B.jpg" alt="StovCookTop_B" loading="lazy"><figcaption>StovCookTop_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechBoard_A1.jpg" alt="TechBoard_A1" loading="lazy"><figcaption>TechBoard_A1</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechBoard_A2.jpg" alt="TechBoard_A2" loading="lazy"><figcaption>TechBoard_A2</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechBoard_B1.jpg" alt="TechBoard_B1" loading="lazy"><figcaption>TechBoard_B1</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechBoard_B2.jpg" alt="TechBoard_B2" loading="lazy"><figcaption>TechBoard_B2</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechButnBoard_A.jpg" alt="TechButnBoard_A" loading="lazy"><figcaption>TechButnBoard_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechButnBoard_B.jpg" alt="TechButnBoard_B" loading="lazy"><figcaption>TechButnBoard_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechButnBoard_C.jpg" alt="TechButnBoard_C" loading="lazy"><figcaption>TechButnBoard_C</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/TechButnBoard_D.jpg" alt="TechButnBoard_D" loading="lazy"><figcaption>TechButnBoard_D</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/USNASRFsign_A.jpg" alt="USNASRFsign_A" loading="lazy"><figcaption>USNASRFsign_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/USNASRFsign_B.jpg" alt="USNASRFsign_B" loading="lazy"><figcaption>USNASRFsign_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/USNASRFsign_C.jpg" alt="USNASRFsign_C" loading="lazy"><figcaption>USNASRFsign_C</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/USNASRFsign_D.jpg" alt="USNASRFsign_D" loading="lazy"><figcaption>USNASRFsign_D</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/VendMachine_A.jpg" alt="VendMachine_A" loading="lazy"><figcaption>VendMachine_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/VendMachine_B.jpg" alt="VendMachine_B" loading="lazy"><figcaption>VendMachine_B</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/VendMachine_C.jpg" alt="VendMachine_C" loading="lazy"><figcaption>VendMachine_C</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/VendMachine_D.jpg" alt="VendMachine_D" loading="lazy"><figcaption>VendMachine_D</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/WarnYeloStrip_A.jpg" alt="WarnYeloStrip_A" loading="lazy"><figcaption>WarnYeloStrip_A</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/cargo01.jpg" alt="cargo01" loading="lazy"><figcaption>cargo01</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/cargo01b.jpg" alt="cargo01b" loading="lazy"><figcaption>cargo01b</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/cargo03.jpg" alt="cargo03" loading="lazy"><figcaption>cargo03</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/microwave.jpg" alt="microwave" loading="lazy"><figcaption>microwave</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/militarydogs.jpg" alt="militarydogs" loading="lazy"><figcaption>militarydogs</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/minifridge.jpg" alt="minifridge" loading="lazy"><figcaption>minifridge</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/pantry2.jpg" alt="pantry2" loading="lazy"><figcaption>pantry2</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/pantry_1.jpg" alt="pantry_1" loading="lazy"><figcaption>pantry_1</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/plaque_a.jpg" alt="plaque_a" loading="lazy"><figcaption>plaque_a</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/plaque_b.jpg" alt="plaque_b" loading="lazy"><figcaption>plaque_b</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/restrcdarea.jpg" alt="restrcdarea" loading="lazy"><figcaption>restrcdarea</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/stopscrtyarea.jpg" alt="stopscrtyarea" loading="lazy"><figcaption>stopscrtyarea</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/telephone_a.jpg" alt="telephone_a" loading="lazy"><figcaption>telephone_a</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/toaster.jpg" alt="toaster" loading="lazy"><figcaption>toaster</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/unauthrzdprsnl.jpg" alt="unauthrzdprsnl" loading="lazy"><figcaption>unauthrzdprsnl</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/western_a.jpg" alt="western_a" loading="lazy"><figcaption>western_a</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/western_b.jpg" alt="western_b" loading="lazy"><figcaption>western_b</figcaption></figure>
<figure><img src="CoreTexMisc/Metal/western_c.jpg" alt="western_c" loading="lazy"><figcaption>western_c</figcaption></figure>

### Misc  
<figure><img src="CoreTexMisc/Misc/DangerHighVolt.jpg" alt="DangerHighVolt" loading="lazy"><figcaption>DangerHighVolt</figcaption></figure>
<figure><img src="CoreTexMisc/Misc/MedChart_B.jpg" alt="MedChart_B" loading="lazy"><figcaption>MedChart_B</figcaption></figure>

### Textile  
<figure><img src="CoreTexMisc/Textile/Art_A.jpg" alt="Art_A" loading="lazy"><figcaption>Art_A</figcaption></figure>
<figure><img src="CoreTexMisc/Textile/Art_B.jpg" alt="Art_B" loading="lazy"><figcaption>Art_B</figcaption></figure>
<figure><img src="CoreTexMisc/Textile/Art_C.jpg" alt="Art_C" loading="lazy"><figcaption>Art_C</figcaption></figure>
<figure><img src="CoreTexMisc/Textile/Art_D.jpg" alt="Art_D" loading="lazy"><figcaption>Art_D</figcaption></figure>
<figure><img src="CoreTexMisc/Textile/Art_E.jpg" alt="Art_E" loading="lazy"><figcaption>Art_E</figcaption></figure>## CoreTexPaper

### Paper  
<figure><img src="CoreTexPaper/Paper/ClenBluePatrn_A.jpg" alt="ClenBluePatrn_A" loading="lazy"><figcaption>ClenBluePatrn_A</figcaption></figure>
<figure><img src="CoreTexPaper/Paper/ClenPatnWpapr_B.jpg" alt="ClenPatnWpapr_B" loading="lazy"><figcaption>ClenPatnWpapr_B</figcaption></figure>
<figure><img src="CoreTexPaper/Paper/ClenWhitPaint_A.jpg" alt="ClenWhitPaint_A" loading="lazy"><figcaption>ClenWhitPaint_A</figcaption></figure>
<figure><img src="CoreTexPaper/Paper/FiniWhitDrywa_A.jpg" alt="FiniWhitDrywa_A" loading="lazy"><figcaption>FiniWhitDrywa_A</figcaption></figure>
<figure><img src="CoreTexPaper/Paper/FrshWhitPlast_A.jpg" alt="FrshWhitPlast_A" loading="lazy"><figcaption>FrshWhitPlast_A</figcaption></figure>
<figure><img src="CoreTexPaper/Paper/UnFiWhitDrywa_A.jpg" alt="UnFiWhitDrywa_A" loading="lazy"><figcaption>UnFiWhitDrywa_A</figcaption></figure>
<figure><img src="CoreTexPaper/Paper/UnFiWhitDrywa_B.jpg" alt="UnFiWhitDrywa_B" loading="lazy"><figcaption>UnFiWhitDrywa_B</figcaption></figure>## CoreTexSky

### Horizon  
<figure><img src="CoreTexSky/Horizon/Area51_RDSky_01.jpg" alt="Area51_RDSky_01" loading="lazy"><figcaption>Area51_RDSky_01</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/Area51_RDSky_02.jpg" alt="Area51_RDSky_02" loading="lazy"><figcaption>Area51_RDSky_02</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/Area51_RDSky_03.jpg" alt="Area51_RDSky_03" loading="lazy"><figcaption>Area51_RDSky_03</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/Area51_RDSky_04.jpg" alt="Area51_RDSky_04" loading="lazy"><figcaption>Area51_RDSky_04</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/HK_City_A_A.jpg" alt="HK_City_A_A" loading="lazy"><figcaption>HK_City_A_A</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/HK_City_A_B.jpg" alt="HK_City_A_B" loading="lazy"><figcaption>HK_City_A_B</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/SunsetskyA_A.jpg" alt="SunsetskyA_A" loading="lazy"><figcaption>SunsetskyA_A</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/SunsetskyA_B.jpg" alt="SunsetskyA_B" loading="lazy"><figcaption>SunsetskyA_B</figcaption></figure>
<figure><img src="CoreTexSky/Horizon/SunsetskyA_C.jpg" alt="SunsetskyA_C" loading="lazy"><figcaption>SunsetskyA_C</figcaption></figure>

### Sky  
<figure><img src="CoreTexSky/Sky/ClenCloudBank_A.jpg" alt="ClenCloudBank_A" loading="lazy"><figcaption>ClenCloudBank_A</figcaption></figure>
<figure><img src="CoreTexSky/Sky/ClenCloudBank_B.jpg" alt="ClenCloudBank_B" loading="lazy"><figcaption>ClenCloudBank_B</figcaption></figure>
<figure><img src="CoreTexSky/Sky/ClenGreyHmoon.jpg" alt="ClenGreyHmoon" loading="lazy"><figcaption>ClenGreyHmoon</figcaption></figure>
<figure><img src="CoreTexSky/Sky/ClenOrngeHaze_A.jpg" alt="ClenOrngeHaze_A" loading="lazy"><figcaption>ClenOrngeHaze_A</figcaption></figure>
<figure><img src="CoreTexSky/Sky/ColorStars_A.jpg" alt="ColorStars_A" loading="lazy"><figcaption>ColorStars_A</figcaption></figure>
<figure><img src="CoreTexSky/Sky/FlufBlueCloud_A.jpg" alt="FlufBlueCloud_A" loading="lazy"><figcaption>FlufBlueCloud_A</figcaption></figure>
<figure><img src="CoreTexSky/Sky/FlufBlueCloud_B.jpg" alt="FlufBlueCloud_B" loading="lazy"><figcaption>FlufBlueCloud_B</figcaption></figure>
<figure><img src="CoreTexSky/Sky/NY_1.jpg" alt="NY_1" loading="lazy"><figcaption>NY_1</figcaption></figure>
<figure><img src="CoreTexSky/Sky/NY_2.jpg" alt="NY_2" loading="lazy"><figcaption>NY_2</figcaption></figure>
<figure><img src="CoreTexSky/Sky/NY_3.jpg" alt="NY_3" loading="lazy"><figcaption>NY_3</figcaption></figure>
<figure><img src="CoreTexSky/Sky/NY_4.jpg" alt="NY_4" loading="lazy"><figcaption>NY_4</figcaption></figure>
<figure><img src="CoreTexSky/Sky/RedLight_A01.jpg" alt="RedLight_A01" loading="lazy"><figcaption>RedLight_A01</figcaption></figure>
<figure><img src="CoreTexSky/Sky/RedLight_A02.jpg" alt="RedLight_A02" loading="lazy"><figcaption>RedLight_A02</figcaption></figure>
<figure><img src="CoreTexSky/Sky/RedLight_A03.jpg" alt="RedLight_A03" loading="lazy"><figcaption>RedLight_A03</figcaption></figure>
<figure><img src="CoreTexSky/Sky/RedLight_A04.jpg" alt="RedLight_A04" loading="lazy"><figcaption>RedLight_A04</figcaption></figure>
<figure><img src="CoreTexSky/Sky/RedLight_A05.jpg" alt="RedLight_A05" loading="lazy"><figcaption>RedLight_A05</figcaption></figure>
<figure><img src="CoreTexSky/Sky/RedLight_A06.jpg" alt="RedLight_A06" loading="lazy"><figcaption>RedLight_A06</figcaption></figure>
<figure><img src="CoreTexSky/Sky/SubtleCloud.jpg" alt="SubtleCloud" loading="lazy"><figcaption>SubtleCloud</figcaption></figure>
<figure><img src="CoreTexSky/Sky/starsless.jpg" alt="starsless" loading="lazy"><figcaption>starsless</figcaption></figure>
<figure><img src="CoreTexSky/Sky/starslesser.jpg" alt="starslesser" loading="lazy"><figcaption>starslesser</figcaption></figure>
<figure><img src="CoreTexSky/Sky/starslesser01.jpg" alt="starslesser01" loading="lazy"><figcaption>starslesser01</figcaption></figure>
<figure><img src="CoreTexSky/Sky/starslesser02.jpg" alt="starslesser02" loading="lazy"><figcaption>starslesser02</figcaption></figure>
<figure><img src="CoreTexSky/Sky/starslesser03.jpg" alt="starslesser03" loading="lazy"><figcaption>starslesser03</figcaption></figure>## CoreTexStone

### Stone  
<figure><img src="CoreTexStone/Stone/BeneBeigMarbl_A.jpg" alt="BeneBeigMarbl_A" loading="lazy"><figcaption>BeneBeigMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/BrecBrwnMarbl_A.jpg" alt="BrecBrwnMarbl_A" loading="lazy"><figcaption>BrecBrwnMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ClenBrwnLmstn_B.jpg" alt="ClenBrwnLmstn_B" loading="lazy"><figcaption>ClenBrwnLmstn_B</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ClenGrenMarbl_A.jpg" alt="ClenGrenMarbl_A" loading="lazy"><figcaption>ClenGrenMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ClenGrenSlate_A.jpg" alt="ClenGrenSlate_A" loading="lazy"><figcaption>ClenGrenSlate_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ClenGreyStone_A.jpg" alt="ClenGreyStone_A" loading="lazy"><figcaption>ClenGreyStone_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ClenGrntStone_A.jpg" alt="ClenGrntStone_A" loading="lazy"><figcaption>ClenGrntStone_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ClenViltMarble0.jpg" alt="ClenViltMarble0" loading="lazy"><figcaption>ClenViltMarble0</figcaption></figure>
<figure><img src="CoreTexStone/Stone/DarkBrwnMarbl_A.jpg" alt="DarkBrwnMarbl_A" loading="lazy"><figcaption>DarkBrwnMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/DevoBeigMarbl_A.jpg" alt="DevoBeigMarbl_A" loading="lazy"><figcaption>DevoBeigMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/GranBeigMarbl_B.jpg" alt="GranBeigMarbl_B" loading="lazy"><figcaption>GranBeigMarbl_B</figcaption></figure>
<figure><img src="CoreTexStone/Stone/LiteBeigMarbl_A.jpg" alt="LiteBeigMarbl_A" loading="lazy"><figcaption>LiteBeigMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/MasnBrwnMarbl_A.jpg" alt="MasnBrwnMarbl_A" loading="lazy"><figcaption>MasnBrwnMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/RockBrwnMarbl_A.jpg" alt="RockBrwnMarbl_A" loading="lazy"><figcaption>RockBrwnMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/ShnyBlacMarbl_A.jpg" alt="ShnyBlacMarbl_A" loading="lazy"><figcaption>ShnyBlacMarbl_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/StoneDoorFram_A.jpg" alt="StoneDoorFram_A" loading="lazy"><figcaption>StoneDoorFram_A</figcaption></figure>
<figure><img src="CoreTexStone/Stone/brownorngerock.jpg" alt="brownorngerock" loading="lazy"><figcaption>brownorngerock</figcaption></figure>
<figure><img src="CoreTexStone/Stone/brwn_stonwal_b.jpg" alt="brwn_stonwal_b" loading="lazy"><figcaption>brwn_stonwal_b</figcaption></figure>
<figure><img src="CoreTexStone/Stone/grey_stonewall_.jpg" alt="grey_stonewall_" loading="lazy"><figcaption>grey_stonewall_</figcaption></figure>
<figure><img src="CoreTexStone/Stone/litebrownrock.jpg" alt="litebrownrock" loading="lazy"><figcaption>litebrownrock</figcaption></figure>
<figure><img src="CoreTexStone/Stone/roughgreystne_a.jpg" alt="roughgreystne_a" loading="lazy"><figcaption>roughgreystne_a</figcaption></figure>
<figure><img src="CoreTexStone/Stone/stonewall_a.jpg" alt="stonewall_a" loading="lazy"><figcaption>stonewall_a</figcaption></figure>
<figure><img src="CoreTexStone/Stone/tan_grey.jpg" alt="tan_grey" loading="lazy"><figcaption>tan_grey</figcaption></figure>
<figure><img src="CoreTexStone/Stone/tanwall.jpg" alt="tanwall" loading="lazy"><figcaption>tanwall</figcaption></figure>
<figure><img src="CoreTexStone/Stone/un_stonsign_c.jpg" alt="un_stonsign_c" loading="lazy"><figcaption>un_stonsign_c</figcaption></figure>
<figure><img src="CoreTexStone/Stone/un_stonsign_d.jpg" alt="un_stonsign_d" loading="lazy"><figcaption>un_stonsign_d</figcaption></figure>
<figure><img src="CoreTexStone/Stone/whiterock.jpg" alt="whiterock" loading="lazy"><figcaption>whiterock</figcaption></figure>
<figure><img src="CoreTexStone/Stone/yllwstnetiles_a.jpg" alt="yllwstnetiles_a" loading="lazy"><figcaption>yllwstnetiles_a</figcaption></figure>## CoreTexStucco

### Stucco  
<figure><img src="CoreTexStucco/Stucco/ClenGreyDrWal01.jpg" alt="ClenGreyDrWal01" loading="lazy"><figcaption>ClenGreyDrWal01</figcaption></figure>
<figure><img src="CoreTexStucco/Stucco/ClenLtan Adobe_.jpg" alt="ClenLtan Adobe_" loading="lazy"><figcaption>ClenLtan Adobe_</figcaption></figure>
<figure><img src="CoreTexStucco/Stucco/RuffWhitStuco_A.jpg" alt="RuffWhitStuco_A" loading="lazy"><figcaption>RuffWhitStuco_A</figcaption></figure>
<figure><img src="CoreTexStucco/Stucco/RuffWhitStuco_B.jpg" alt="RuffWhitStuco_B" loading="lazy"><figcaption>RuffWhitStuco_B</figcaption></figure>## CoreTexTextile

### Textile  
<figure><img src="CoreTexTextile/Textile/ClenBlueGrey_A.jpg" alt="ClenBlueGrey_A" loading="lazy"><figcaption>ClenBlueGrey_A</figcaption></figure>
<figure><img src="CoreTexTextile/Textile/ClenBrwnRugg_A.jpg" alt="ClenBrwnRugg_A" loading="lazy"><figcaption>ClenBrwnRugg_A</figcaption></figure>
<figure><img src="CoreTexTextile/Textile/ClenGrayCarpt_C.jpg" alt="ClenGrayCarpt_C" loading="lazy"><figcaption>ClenGrayCarpt_C</figcaption></figure>
<figure><img src="CoreTexTextile/Textile/ClenGrenCarpt01.jpg" alt="ClenGrenCarpt01" loading="lazy"><figcaption>ClenGrenCarpt01</figcaption></figure>
<figure><img src="CoreTexTextile/Textile/OldeLaceCloth_A.jpg" alt="OldeLaceCloth_A" loading="lazy"><figcaption>OldeLaceCloth_A</figcaption></figure>
<figure><img src="CoreTexTextile/Textile/OldeLaceCloth_B.jpg" alt="OldeLaceCloth_B" loading="lazy"><figcaption>OldeLaceCloth_B</figcaption></figure>
<figure><img src="CoreTexTextile/Textile/WavePatnCarpt.jpg" alt="WavePatnCarpt" loading="lazy"><figcaption>WavePatnCarpt</figcaption></figure>## CoreTexTiles

### Tiles  
<figure><img src="CoreTexTiles/Tiles/9x9darktiles.jpg" alt="9x9darktiles" loading="lazy"><figcaption>9x9darktiles</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/ClenGrayCeiln_A.jpg" alt="ClenGrayCeiln_A" loading="lazy"><figcaption>ClenGrayCeiln_A</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/INDUbluetile.jpg" alt="INDUbluetile" loading="lazy"><figcaption>INDUbluetile</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/INDUgraytile.jpg" alt="INDUgraytile" loading="lazy"><figcaption>INDUgraytile</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/INDUgreentile.jpg" alt="INDUgreentile" loading="lazy"><figcaption>INDUgreentile</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/Terracottatile.jpg" alt="Terracottatile" loading="lazy"><figcaption>Terracottatile</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/WhitCeilnTile_A.jpg" alt="WhitCeilnTile_A" loading="lazy"><figcaption>WhitCeilnTile_A</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/WhitCeilnTile_B.jpg" alt="WhitCeilnTile_B" loading="lazy"><figcaption>WhitCeilnTile_B</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/WhitCeilnTile_C.jpg" alt="WhitCeilnTile_C" loading="lazy"><figcaption>WhitCeilnTile_C</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/brown_clay.jpg" alt="brown_clay" loading="lazy"><figcaption>brown_clay</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/diamond_floor.jpg" alt="diamond_floor" loading="lazy"><figcaption>diamond_floor</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/green_a.jpg" alt="green_a" loading="lazy"><figcaption>green_a</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/hexwall01.jpg" alt="hexwall01" loading="lazy"><figcaption>hexwall01</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/tanfloortile_a.jpg" alt="tanfloortile_a" loading="lazy"><figcaption>tanfloortile_a</figcaption></figure>
<figure><img src="CoreTexTiles/Tiles/tanfloortile_b.jpg" alt="tanfloortile_b" loading="lazy"><figcaption>tanfloortile_b</figcaption></figure>## CoreTexWallObj

### Wall_Objects  
<figure><img src="CoreTexWallObj/Wall_Objects/BeigTrimWall_B.jpg" alt="BeigTrimWall_B" loading="lazy"><figcaption>BeigTrimWall_B</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/ClenBrwnTRl_A01.jpg" alt="ClenBrwnTRl_A01" loading="lazy"><figcaption>ClenBrwnTRl_A01</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/ClenBrwnTRl_A02.jpg" alt="ClenBrwnTRl_A02" loading="lazy"><figcaption>ClenBrwnTRl_A02</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/ClenBrwnTRl_A03.jpg" alt="ClenBrwnTRl_A03" loading="lazy"><figcaption>ClenBrwnTRl_A03</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/ClenBrwnTRl_A04.jpg" alt="ClenBrwnTRl_A04" loading="lazy"><figcaption>ClenBrwnTRl_A04</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/DrtyCabntDoor_A.jpg" alt="DrtyCabntDoor_A" loading="lazy"><figcaption>DrtyCabntDoor_A</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/GrayTrimWall_B.jpg" alt="GrayTrimWall_B" loading="lazy"><figcaption>GrayTrimWall_B</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/GrayTrimWall_D.jpg" alt="GrayTrimWall_D" loading="lazy"><figcaption>GrayTrimWall_D</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/GrayWoodTWall_A.jpg" alt="GrayWoodTWall_A" loading="lazy"><figcaption>GrayWoodTWall_A</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/GrayWoodTWall_B.jpg" alt="GrayWoodTWall_B" loading="lazy"><figcaption>GrayWoodTWall_B</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/GrayWoodTWall_C.jpg" alt="GrayWoodTWall_C" loading="lazy"><figcaption>GrayWoodTWall_C</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/GrayWoodTWall_D.jpg" alt="GrayWoodTWall_D" loading="lazy"><figcaption>GrayWoodTWall_D</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/beigewall_a.jpg" alt="beigewall_a" loading="lazy"><figcaption>beigewall_a</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/beigewoodstrp_a.jpg" alt="beigewoodstrp_a" loading="lazy"><figcaption>beigewoodstrp_a</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/graycncrtwall_a.jpg" alt="graycncrtwall_a" loading="lazy"><figcaption>graycncrtwall_a</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/greenntan_a.jpg" alt="greenntan_a" loading="lazy"><figcaption>greenntan_a</figcaption></figure>
<figure><img src="CoreTexWallObj/Wall_Objects/stccowall_a.jpg" alt="stccowall_a" loading="lazy"><figcaption>stccowall_a</figcaption></figure>## CoreTexWater

### water  
<figure><img src="CoreTexWater/water/bluewater.jpg" alt="bluewater" loading="lazy"><figcaption>bluewater</figcaption></figure>
<figure><img src="CoreTexWater/water/dirtywater.jpg" alt="dirtywater" loading="lazy"><figcaption>dirtywater</figcaption></figure>## CoreTexWood

### Wood  
<figure><img src="CoreTexWood/Wood/BarnacleWood_A.jpg" alt="BarnacleWood_A" loading="lazy"><figcaption>BarnacleWood_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/BarnacleWood_B.jpg" alt="BarnacleWood_B" loading="lazy"><figcaption>BarnacleWood_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/BoatHouseWood_A.jpg" alt="BoatHouseWood_A" loading="lazy"><figcaption>BoatHouseWood_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/BoatHouseWood_C.jpg" alt="BoatHouseWood_C" loading="lazy"><figcaption>BoatHouseWood_C</figcaption></figure>
<figure><img src="CoreTexWood/Wood/BoatHouseWood_D.jpg" alt="BoatHouseWood_D" loading="lazy"><figcaption>BoatHouseWood_D</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenBrwnWdflt_A.jpg" alt="ClenBrwnWdflt_A" loading="lazy"><figcaption>ClenBrwnWdflt_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenBrwnWdflt_B.jpg" alt="ClenBrwnWdflt_B" loading="lazy"><figcaption>ClenBrwnWdflt_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenBrwnWood_A.jpg" alt="ClenBrwnWood_A" loading="lazy"><figcaption>ClenBrwnWood_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenBrwnWood_B.jpg" alt="ClenBrwnWood_B" loading="lazy"><figcaption>ClenBrwnWood_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenLiteAshen_A.jpg" alt="ClenLiteAshen_A" loading="lazy"><figcaption>ClenLiteAshen_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenMedmChery_A.jpg" alt="ClenMedmChery_A" loading="lazy"><figcaption>ClenMedmChery_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenMedmWalnt_A.jpg" alt="ClenMedmWalnt_A" loading="lazy"><figcaption>ClenMedmWalnt_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWhitPine_A.jpg" alt="ClenWhitPine_A" loading="lazy"><figcaption>ClenWhitPine_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodDoor_A.jpg" alt="ClenWoodDoor_A" loading="lazy"><figcaption>ClenWoodDoor_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodDoor_B.jpg" alt="ClenWoodDoor_B" loading="lazy"><figcaption>ClenWoodDoor_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodDoor_D.jpg" alt="ClenWoodDoor_D" loading="lazy"><figcaption>ClenWoodDoor_D</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodDoor_E.jpg" alt="ClenWoodDoor_E" loading="lazy"><figcaption>ClenWoodDoor_E</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_A.jpg" alt="ClenWoodPanel_A" loading="lazy"><figcaption>ClenWoodPanel_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_C.jpg" alt="ClenWoodPanel_C" loading="lazy"><figcaption>ClenWoodPanel_C</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_D.jpg" alt="ClenWoodPanel_D" loading="lazy"><figcaption>ClenWoodPanel_D</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_H.jpg" alt="ClenWoodPanel_H" loading="lazy"><figcaption>ClenWoodPanel_H</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_I.jpg" alt="ClenWoodPanel_I" loading="lazy"><figcaption>ClenWoodPanel_I</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_K.jpg" alt="ClenWoodPanel_K" loading="lazy"><figcaption>ClenWoodPanel_K</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenWoodPanel_N.jpg" alt="ClenWoodPanel_N" loading="lazy"><figcaption>ClenWoodPanel_N</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenYellFlrtl_A.jpg" alt="ClenYellFlrtl_A" loading="lazy"><figcaption>ClenYellFlrtl_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ClenYellFlrtl_B.jpg" alt="ClenYellFlrtl_B" loading="lazy"><figcaption>ClenYellFlrtl_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ComnOffcDoor_A.jpg" alt="ComnOffcDoor_A" loading="lazy"><figcaption>ComnOffcDoor_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ComnOffcDoor_B.jpg" alt="ComnOffcDoor_B" loading="lazy"><figcaption>ComnOffcDoor_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ComnRestDoor_A.jpg" alt="ComnRestDoor_A" loading="lazy"><figcaption>ComnRestDoor_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ComnRestDoor_B.jpg" alt="ComnRestDoor_B" loading="lazy"><figcaption>ComnRestDoor_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/ComnWoodDoor_A.jpg" alt="ComnWoodDoor_A" loading="lazy"><figcaption>ComnWoodDoor_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DamgGreyWood01.jpg" alt="DamgGreyWood01" loading="lazy"><figcaption>DamgGreyWood01</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DrtyPlyWood_A.jpg" alt="DrtyPlyWood_A" loading="lazy"><figcaption>DrtyPlyWood_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DrtyWoodCrate_A.jpg" alt="DrtyWoodCrate_A" loading="lazy"><figcaption>DrtyWoodCrate_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DrtyWoodCrate_B.jpg" alt="DrtyWoodCrate_B" loading="lazy"><figcaption>DrtyWoodCrate_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DrtyWoodCrate_C.jpg" alt="DrtyWoodCrate_C" loading="lazy"><figcaption>DrtyWoodCrate_C</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DrtyWoodPanel_A.jpg" alt="DrtyWoodPanel_A" loading="lazy"><figcaption>DrtyWoodPanel_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DrtyWoodPanel_B.jpg" alt="DrtyWoodPanel_B" loading="lazy"><figcaption>DrtyWoodPanel_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DtyBrwnRftle_A.jpg" alt="DtyBrwnRftle_A" loading="lazy"><figcaption>DtyBrwnRftle_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DtyBrwnRftle_B.jpg" alt="DtyBrwnRftle_B" loading="lazy"><figcaption>DtyBrwnRftle_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/DtyBrwnRftle_C.jpg" alt="DtyBrwnRftle_C" loading="lazy"><figcaption>DtyBrwnRftle_C</figcaption></figure>
<figure><img src="CoreTexWood/Wood/FlutBrwnPanel_A.jpg" alt="FlutBrwnPanel_A" loading="lazy"><figcaption>FlutBrwnPanel_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/LockWoodDoor_A.jpg" alt="LockWoodDoor_A" loading="lazy"><figcaption>LockWoodDoor_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/LockWoodDoor_B.jpg" alt="LockWoodDoor_B" loading="lazy"><figcaption>LockWoodDoor_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/LockWoodDoor_C.jpg" alt="LockWoodDoor_C" loading="lazy"><figcaption>LockWoodDoor_C</figcaption></figure>
<figure><img src="CoreTexWood/Wood/LockWoodDoor_D.jpg" alt="LockWoodDoor_D" loading="lazy"><figcaption>LockWoodDoor_D</figcaption></figure>
<figure><img src="CoreTexWood/Wood/OldeOakPlank_A.jpg" alt="OldeOakPlank_A" loading="lazy"><figcaption>OldeOakPlank_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/WhitePanlDoor_A.jpg" alt="WhitePanlDoor_A" loading="lazy"><figcaption>WhitePanlDoor_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/WoodDoorFrame_A.jpg" alt="WoodDoorFrame_A" loading="lazy"><figcaption>WoodDoorFrame_A</figcaption></figure>
<figure><img src="CoreTexWood/Wood/WoodDoorFrame_B.jpg" alt="WoodDoorFrame_B" loading="lazy"><figcaption>WoodDoorFrame_B</figcaption></figure>
<figure><img src="CoreTexWood/Wood/browntile.jpg" alt="browntile" loading="lazy"><figcaption>browntile</figcaption></figure>
<figure><img src="CoreTexWood/Wood/deskdrawer_a.jpg" alt="deskdrawer_a" loading="lazy"><figcaption>deskdrawer_a</figcaption></figure>
<figure><img src="CoreTexWood/Wood/wooddoor_mn_a.jpg" alt="wooddoor_mn_a" loading="lazy"><figcaption>wooddoor_mn_a</figcaption></figure>
<figure><img src="CoreTexWood/Wood/wooddoor_wmn_a.jpg" alt="wooddoor_wmn_a" loading="lazy"><figcaption>wooddoor_wmn_a</figcaption></figure>
<figure><img src="CoreTexWood/Wood/woodfloor_a.jpg" alt="woodfloor_a" loading="lazy"><figcaption>woodfloor_a</figcaption></figure>
<figure><img src="CoreTexWood/Wood/woodflrStrlight.jpg" alt="woodflrStrlight" loading="lazy"><figcaption>woodflrStrlight</figcaption></figure>
<figure><img src="CoreTexWood/Wood/woodwall_a.jpg" alt="woodwall_a" loading="lazy"><figcaption>woodwall_a</figcaption></figure>## DXFonts

### HUDMessageTrueType  
<figure><img src="DXFonts/HUDMessageTrueType/Texture2.jpg" alt="Texture2" loading="lazy"><figcaption>Texture2</figcaption></figure>
<figure><img src="DXFonts/HUDMessageTrueType/Texture3.jpg" alt="Texture3" loading="lazy"><figcaption>Texture3</figcaption></figure>

### MainMenuTrueType  
<figure><img src="DXFonts/MainMenuTrueType/Texture0.jpg" alt="Texture0" loading="lazy"><figcaption>Texture0</figcaption></figure>
<figure><img src="DXFonts/MainMenuTrueType/Texture1.jpg" alt="Texture1" loading="lazy"><figcaption>Texture1</figcaption></figure>## Dockyard

### Brick  
<figure><img src="Dockyard/Brick/lgwhtbrk01.jpg" alt="lgwhtbrk01" loading="lazy"><figcaption>lgwhtbrk01</figcaption></figure>
<figure><img src="Dockyard/Brick/lgwhtbrk01b.jpg" alt="lgwhtbrk01b" loading="lazy"><figcaption>lgwhtbrk01b</figcaption></figure>
<figure><img src="Dockyard/Brick/lgwhtbrk01c.jpg" alt="lgwhtbrk01c" loading="lazy"><figcaption>lgwhtbrk01c</figcaption></figure>
<figure><img src="Dockyard/Brick/smblbrk01.jpg" alt="smblbrk01" loading="lazy"><figcaption>smblbrk01</figcaption></figure>
<figure><img src="Dockyard/Brick/smwhtbrk01.jpg" alt="smwhtbrk01" loading="lazy"><figcaption>smwhtbrk01</figcaption></figure>
<figure><img src="Dockyard/Brick/smwhtbrk01b.jpg" alt="smwhtbrk01b" loading="lazy"><figcaption>smwhtbrk01b</figcaption></figure>
<figure><img src="Dockyard/Brick/smwhtbrk01c.jpg" alt="smwhtbrk01c" loading="lazy"><figcaption>smwhtbrk01c</figcaption></figure>
<figure><img src="Dockyard/Brick/smwhtbrk01d.jpg" alt="smwhtbrk01d" loading="lazy"><figcaption>smwhtbrk01d</figcaption></figure>
<figure><img src="Dockyard/Brick/subsign01.jpg" alt="subsign01" loading="lazy"><figcaption>subsign01</figcaption></figure>
<figure><img src="Dockyard/Brick/warewallA_64.jpg" alt="warewallA_64" loading="lazy"><figcaption>warewallA_64</figcaption></figure>

### Concrete  
<figure><img src="Dockyard/Concrete/grnd01.jpg" alt="grnd01" loading="lazy"><figcaption>grnd01</figcaption></figure>
<figure><img src="Dockyard/Concrete/warewall01.jpg" alt="warewall01" loading="lazy"><figcaption>warewall01</figcaption></figure>
<figure><img src="Dockyard/Concrete/warewall01b.jpg" alt="warewall01b" loading="lazy"><figcaption>warewall01b</figcaption></figure>
<figure><img src="Dockyard/Concrete/whtcrete.jpg" alt="whtcrete" loading="lazy"><figcaption>whtcrete</figcaption></figure>
<figure><img src="Dockyard/Concrete/whtcrete02.jpg" alt="whtcrete02" loading="lazy"><figcaption>whtcrete02</figcaption></figure>

### Glass  
<figure><img src="Dockyard/Glass/photo01.jpg" alt="photo01" loading="lazy"><figcaption>photo01</figcaption></figure>
<figure><img src="Dockyard/Glass/plaque01.jpg" alt="plaque01" loading="lazy"><figcaption>plaque01</figcaption></figure>
<figure><img src="Dockyard/Glass/plaque02.jpg" alt="plaque02" loading="lazy"><figcaption>plaque02</figcaption></figure>
<figure><img src="Dockyard/Glass/plaque03.jpg" alt="plaque03" loading="lazy"><figcaption>plaque03</figcaption></figure>
<figure><img src="Dockyard/Glass/plaque04.jpg" alt="plaque04" loading="lazy"><figcaption>plaque04</figcaption></figure>
<figure><img src="Dockyard/Glass/poster01.jpg" alt="poster01" loading="lazy"><figcaption>poster01</figcaption></figure>

### Metal  
<figure><img src="Dockyard/Metal/AmmoStorAccess.jpg" alt="AmmoStorAccess" loading="lazy"><figcaption>AmmoStorAccess</figcaption></figure>
<figure><img src="Dockyard/Metal/CranePanel_C.jpg" alt="CranePanel_C" loading="lazy"><figcaption>CranePanel_C</figcaption></figure>
<figure><img src="Dockyard/Metal/CranePanel_D.jpg" alt="CranePanel_D" loading="lazy"><figcaption>CranePanel_D</figcaption></figure>
<figure><img src="Dockyard/Metal/CranePanel_E.jpg" alt="CranePanel_E" loading="lazy"><figcaption>CranePanel_E</figcaption></figure>
<figure><img src="Dockyard/Metal/DockAreaAccess.jpg" alt="DockAreaAccess" loading="lazy"><figcaption>DockAreaAccess</figcaption></figure>
<figure><img src="Dockyard/Metal/DockBDoors_B.jpg" alt="DockBDoors_B" loading="lazy"><figcaption>DockBDoors_B</figcaption></figure>
<figure><img src="Dockyard/Metal/MainAlleyAcces.jpg" alt="MainAlleyAcces" loading="lazy"><figcaption>MainAlleyAcces</figcaption></figure>
<figure><img src="Dockyard/Metal/Mech_Stor_Area.jpg" alt="Mech_Stor_Area" loading="lazy"><figcaption>Mech_Stor_Area</figcaption></figure>
<figure><img src="Dockyard/Metal/MechsOnPatrol.jpg" alt="MechsOnPatrol" loading="lazy"><figcaption>MechsOnPatrol</figcaption></figure>
<figure><img src="Dockyard/Metal/ShipLockr.jpg" alt="ShipLockr" loading="lazy"><figcaption>ShipLockr</figcaption></figure>
<figure><img src="Dockyard/Metal/WareHousStorAc.jpg" alt="WareHousStorAc" loading="lazy"><figcaption>WareHousStorAc</figcaption></figure>
<figure><img src="Dockyard/Metal/bdoor01.jpg" alt="bdoor01" loading="lazy"><figcaption>bdoor01</figcaption></figure>
<figure><img src="Dockyard/Metal/courgmtl01.jpg" alt="courgmtl01" loading="lazy"><figcaption>courgmtl01</figcaption></figure>
<figure><img src="Dockyard/Metal/courgmtl02.jpg" alt="courgmtl02" loading="lazy"><figcaption>courgmtl02</figcaption></figure>
<figure><img src="Dockyard/Metal/crane01.jpg" alt="crane01" loading="lazy"><figcaption>crane01</figcaption></figure>
<figure><img src="Dockyard/Metal/crane02.jpg" alt="crane02" loading="lazy"><figcaption>crane02</figcaption></figure>
<figure><img src="Dockyard/Metal/crane02b.jpg" alt="crane02b" loading="lazy"><figcaption>crane02b</figcaption></figure>
<figure><img src="Dockyard/Metal/dy_sign_01.jpg" alt="dy_sign_01" loading="lazy"><figcaption>dy_sign_01</figcaption></figure>
<figure><img src="Dockyard/Metal/dy_sign_02.jpg" alt="dy_sign_02" loading="lazy"><figcaption>dy_sign_02</figcaption></figure>
<figure><img src="Dockyard/Metal/dy_sign_03.jpg" alt="dy_sign_03" loading="lazy"><figcaption>dy_sign_03</figcaption></figure>
<figure><img src="Dockyard/Metal/dy_sign_04.jpg" alt="dy_sign_04" loading="lazy"><figcaption>dy_sign_04</figcaption></figure>
<figure><img src="Dockyard/Metal/dy_sign_05.jpg" alt="dy_sign_05" loading="lazy"><figcaption>dy_sign_05</figcaption></figure>
<figure><img src="Dockyard/Metal/dy_sign_06.jpg" alt="dy_sign_06" loading="lazy"><figcaption>dy_sign_06</figcaption></figure>
<figure><img src="Dockyard/Metal/fcab01.jpg" alt="fcab01" loading="lazy"><figcaption>fcab01</figcaption></figure>
<figure><img src="Dockyard/Metal/fcab01b.jpg" alt="fcab01b" loading="lazy"><figcaption>fcab01b</figcaption></figure>
<figure><img src="Dockyard/Metal/greylink01.jpg" alt="greylink01" loading="lazy"><figcaption>greylink01</figcaption></figure>
<figure><img src="Dockyard/Metal/pipe01.jpg" alt="pipe01" loading="lazy"><figcaption>pipe01</figcaption></figure>
<figure><img src="Dockyard/Metal/pipends.jpg" alt="pipends" loading="lazy"><figcaption>pipends</figcaption></figure>
<figure><img src="Dockyard/Metal/semitrailer.jpg" alt="semitrailer" loading="lazy"><figcaption>semitrailer</figcaption></figure>
<figure><img src="Dockyard/Metal/subsign02.jpg" alt="subsign02" loading="lazy"><figcaption>subsign02</figcaption></figure>
<figure><img src="Dockyard/Metal/table01.jpg" alt="table01" loading="lazy"><figcaption>table01</figcaption></figure>
<figure><img src="Dockyard/Metal/tank01.jpg" alt="tank01" loading="lazy"><figcaption>tank01</figcaption></figure>
<figure><img src="Dockyard/Metal/vent01.jpg" alt="vent01" loading="lazy"><figcaption>vent01</figcaption></figure>

### Textile  
<figure><img src="Dockyard/Textile/boxes01.jpg" alt="boxes01" loading="lazy"><figcaption>boxes01</figcaption></figure>
<figure><img src="Dockyard/Textile/carpet01.jpg" alt="carpet01" loading="lazy"><figcaption>carpet01</figcaption></figure>
<figure><img src="Dockyard/Textile/cot01.jpg" alt="cot01" loading="lazy"><figcaption>cot01</figcaption></figure>

### Wood  
<figure><img src="Dockyard/Wood/desk01.jpg" alt="desk01" loading="lazy"><figcaption>desk01</figcaption></figure>
<figure><img src="Dockyard/Wood/desk02.jpg" alt="desk02" loading="lazy"><figcaption>desk02</figcaption></figure>
<figure><img src="Dockyard/Wood/smwhtwd01.jpg" alt="smwhtwd01" loading="lazy"><figcaption>smwhtwd01</figcaption></figure>
<figure><img src="Dockyard/Wood/smwhtwd01b.jpg" alt="smwhtwd01b" loading="lazy"><figcaption>smwhtwd01b</figcaption></figure>## Effects

### Corona  
<figure><img src="Effects/Corona/Corona_A.jpg" alt="Corona_A" loading="lazy"><figcaption>Corona_A</figcaption></figure>
<figure><img src="Effects/Corona/Corona_B.jpg" alt="Corona_B" loading="lazy"><figcaption>Corona_B</figcaption></figure>
<figure><img src="Effects/Corona/Corona_C.jpg" alt="Corona_C" loading="lazy"><figcaption>Corona_C</figcaption></figure>
<figure><img src="Effects/Corona/Corona_D.jpg" alt="Corona_D" loading="lazy"><figcaption>Corona_D</figcaption></figure>
<figure><img src="Effects/Corona/Corona_E.jpg" alt="Corona_E" loading="lazy"><figcaption>Corona_E</figcaption></figure>
<figure><img src="Effects/Corona/Corona_F.jpg" alt="Corona_F" loading="lazy"><figcaption>Corona_F</figcaption></figure>
<figure><img src="Effects/Corona/Corona_G.jpg" alt="Corona_G" loading="lazy"><figcaption>Corona_G</figcaption></figure>

### Electricity  
<figure><img src="Effects/Electricity/Blade.jpg" alt="Blade" loading="lazy"><figcaption>Blade</figcaption></figure>
<figure><img src="Effects/Electricity/Xplsn_Emp.jpg" alt="Xplsn_Emp" loading="lazy"><figcaption>Xplsn_Emp</figcaption></figure>

### Generated  
<figure><img src="Effects/Generated/Gen_Blue.jpg" alt="Gen_Blue" loading="lazy"><figcaption>Gen_Blue</figcaption></figure>
<figure><img src="Effects/Generated/Gen_DrtyBrwn.jpg" alt="Gen_DrtyBrwn" loading="lazy"><figcaption>Gen_DrtyBrwn</figcaption></figure>
<figure><img src="Effects/Generated/Gen_DrtyYllw.jpg" alt="Gen_DrtyYllw" loading="lazy"><figcaption>Gen_DrtyYllw</figcaption></figure>
<figure><img src="Effects/Generated/Gen_Orange.jpg" alt="Gen_Orange" loading="lazy"><figcaption>Gen_Orange</figcaption></figure>
<figure><img src="Effects/Generated/Gen_Red.jpg" alt="Gen_Red" loading="lazy"><figcaption>Gen_Red</figcaption></figure>
<figure><img src="Effects/Generated/Gen_Yellow.jpg" alt="Gen_Yellow" loading="lazy"><figcaption>Gen_Yellow</figcaption></figure>
<figure><img src="Effects/Generated/Gen_white.jpg" alt="Gen_white" loading="lazy"><figcaption>Gen_white</figcaption></figure>
<figure><img src="Effects/Generated/WtrDrpSmall.jpg" alt="WtrDrpSmall" loading="lazy"><figcaption>WtrDrpSmall</figcaption></figure>

### Smoke  
<figure><img src="Effects/Smoke/Gas_Poison.jpg" alt="Gas_Poison" loading="lazy"><figcaption>Gas_Poison</figcaption></figure>
<figure><img src="Effects/Smoke/Gas_Tear.jpg" alt="Gas_Tear" loading="lazy"><figcaption>Gas_Tear</figcaption></figure>
<figure><img src="Effects/Smoke/SmokePuff.jpg" alt="SmokePuff" loading="lazy"><figcaption>SmokePuff</figcaption></figure>

### UserInterface  
<figure><img src="Effects/UserInterface/DrunkFXRef.jpg" alt="DrunkFXRef" loading="lazy"><figcaption>DrunkFXRef</figcaption></figure>
<figure><img src="Effects/UserInterface/drunk_FX.jpg" alt="drunk_FX" loading="lazy"><figcaption>drunk_FX</figcaption></figure>

### water  
<figure><img src="Effects/water/Splash_A01.jpg" alt="Splash_A01" loading="lazy"><figcaption>Splash_A01</figcaption></figure>
<figure><img src="Effects/water/Splash_A02.jpg" alt="Splash_A02" loading="lazy"><figcaption>Splash_A02</figcaption></figure>
<figure><img src="Effects/water/Splash_A03.jpg" alt="Splash_A03" loading="lazy"><figcaption>Splash_A03</figcaption></figure>
<figure><img src="Effects/water/Splash_A04.jpg" alt="Splash_A04" loading="lazy"><figcaption>Splash_A04</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A01.jpg" alt="WtrFall_A01" loading="lazy"><figcaption>WtrFall_A01</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A02.jpg" alt="WtrFall_A02" loading="lazy"><figcaption>WtrFall_A02</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A03.jpg" alt="WtrFall_A03" loading="lazy"><figcaption>WtrFall_A03</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A04.jpg" alt="WtrFall_A04" loading="lazy"><figcaption>WtrFall_A04</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A05.jpg" alt="WtrFall_A05" loading="lazy"><figcaption>WtrFall_A05</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A06.jpg" alt="WtrFall_A06" loading="lazy"><figcaption>WtrFall_A06</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A07.jpg" alt="WtrFall_A07" loading="lazy"><figcaption>WtrFall_A07</figcaption></figure>
<figure><img src="Effects/water/WtrFall_A08.jpg" alt="WtrFall_A08" loading="lazy"><figcaption>WtrFall_A08</figcaption></figure>
<figure><img src="Effects/water/bluewater.jpg" alt="bluewater" loading="lazy"><figcaption>bluewater</figcaption></figure>
<figure><img src="Effects/water/water_b.jpg" alt="water_b" loading="lazy"><figcaption>water_b</figcaption></figure>## Extras

### Eggs  
<figure><img src="Extras/Eggs/Matrix_A00.jpg" alt="Matrix_A00" loading="lazy"><figcaption>Matrix_A00</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A01.jpg" alt="Matrix_A01" loading="lazy"><figcaption>Matrix_A01</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A02.jpg" alt="Matrix_A02" loading="lazy"><figcaption>Matrix_A02</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A03.jpg" alt="Matrix_A03" loading="lazy"><figcaption>Matrix_A03</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A04.jpg" alt="Matrix_A04" loading="lazy"><figcaption>Matrix_A04</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A05.jpg" alt="Matrix_A05" loading="lazy"><figcaption>Matrix_A05</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A06.jpg" alt="Matrix_A06" loading="lazy"><figcaption>Matrix_A06</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A07.jpg" alt="Matrix_A07" loading="lazy"><figcaption>Matrix_A07</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A08.jpg" alt="Matrix_A08" loading="lazy"><figcaption>Matrix_A08</figcaption></figure>
<figure><img src="Extras/Eggs/Matrix_A09.jpg" alt="Matrix_A09" loading="lazy"><figcaption>Matrix_A09</figcaption></figure>## FreeClinic

### Brick  
<figure><img src="FreeClinic/Brick/FC_AtriumWall.jpg" alt="FC_AtriumWall" loading="lazy"><figcaption>FC_AtriumWall</figcaption></figure>
<figure><img src="FreeClinic/Brick/FC_Atrium_A.jpg" alt="FC_Atrium_A" loading="lazy"><figcaption>FC_Atrium_A</figcaption></figure>
<figure><img src="FreeClinic/Brick/FC_Wall_05.jpg" alt="FC_Wall_05" loading="lazy"><figcaption>FC_Wall_05</figcaption></figure>

### Concrete  
<figure><img src="FreeClinic/Concrete/FC_PlainCeiln.jpg" alt="FC_PlainCeiln" loading="lazy"><figcaption>FC_PlainCeiln</figcaption></figure>
<figure><img src="FreeClinic/Concrete/FC_Wall_01.jpg" alt="FC_Wall_01" loading="lazy"><figcaption>FC_Wall_01</figcaption></figure>
<figure><img src="FreeClinic/Concrete/FC_Wall_02.jpg" alt="FC_Wall_02" loading="lazy"><figcaption>FC_Wall_02</figcaption></figure>
<figure><img src="FreeClinic/Concrete/FC_Wall_03.jpg" alt="FC_Wall_03" loading="lazy"><figcaption>FC_Wall_03</figcaption></figure>

### Metal  
<figure><img src="FreeClinic/Metal/FC_Sign_01.jpg" alt="FC_Sign_01" loading="lazy"><figcaption>FC_Sign_01</figcaption></figure>

### Paper  
<figure><img src="FreeClinic/Paper/FC_EyeTest.jpg" alt="FC_EyeTest" loading="lazy"><figcaption>FC_EyeTest</figcaption></figure>
<figure><img src="FreeClinic/Paper/FC_Services.jpg" alt="FC_Services" loading="lazy"><figcaption>FC_Services</figcaption></figure>
<figure><img src="FreeClinic/Paper/FC_poster_A.jpg" alt="FC_poster_A" loading="lazy"><figcaption>FC_poster_A</figcaption></figure>

### Textile  
<figure><img src="FreeClinic/Textile/FC_BedTop_A.jpg" alt="FC_BedTop_A" loading="lazy"><figcaption>FC_BedTop_A</figcaption></figure>

### Tiles  
<figure><img src="FreeClinic/Tiles/FC_MainFloor.jpg" alt="FC_MainFloor" loading="lazy"><figcaption>FC_MainFloor</figcaption></figure>## G_Station

### Earth  
<figure><img src="G_Station/Earth/RuffDarkSoils_B.jpg" alt="RuffDarkSoils_B" loading="lazy"><figcaption>RuffDarkSoils_B</figcaption></figure>
<figure><img src="G_Station/Earth/RuffDarkSoils_C.jpg" alt="RuffDarkSoils_C" loading="lazy"><figcaption>RuffDarkSoils_C</figcaption></figure>

### Metal  
<figure><img src="G_Station/Metal/ClenBlckPump_A.jpg" alt="ClenBlckPump_A" loading="lazy"><figcaption>ClenBlckPump_A</figcaption></figure>
<figure><img src="G_Station/Metal/ClenGreyVent_A.jpg" alt="ClenGreyVent_A" loading="lazy"><figcaption>ClenGreyVent_A</figcaption></figure>
<figure><img src="G_Station/Metal/DrtyPriceSign_A.jpg" alt="DrtyPriceSign_A" loading="lazy"><figcaption>DrtyPriceSign_A</figcaption></figure>
<figure><img src="G_Station/Metal/DrtyRedSiding_A.jpg" alt="DrtyRedSiding_A" loading="lazy"><figcaption>DrtyRedSiding_A</figcaption></figure>
<figure><img src="G_Station/Metal/DrtyRedSiding_C.jpg" alt="DrtyRedSiding_C" loading="lazy"><figcaption>DrtyRedSiding_C</figcaption></figure>
<figure><img src="G_Station/Metal/DrtyRedSign_A.jpg" alt="DrtyRedSign_A" loading="lazy"><figcaption>DrtyRedSign_A</figcaption></figure>
<figure><img src="G_Station/Metal/DrtyRedSign_B.jpg" alt="DrtyRedSign_B" loading="lazy"><figcaption>DrtyRedSign_B</figcaption></figure>
<figure><img src="G_Station/Metal/DrtyRedSign_C.jpg" alt="DrtyRedSign_C" loading="lazy"><figcaption>DrtyRedSign_C</figcaption></figure>
<figure><img src="G_Station/Metal/GS_ACunit_01.jpg" alt="GS_ACunit_01" loading="lazy"><figcaption>GS_ACunit_01</figcaption></figure>
<figure><img src="G_Station/Metal/GS_ACunit_02.jpg" alt="GS_ACunit_02" loading="lazy"><figcaption>GS_ACunit_02</figcaption></figure>
<figure><img src="G_Station/Metal/GS_MedKit_01.jpg" alt="GS_MedKit_01" loading="lazy"><figcaption>GS_MedKit_01</figcaption></figure>
<figure><img src="G_Station/Metal/GS_Register.jpg" alt="GS_Register" loading="lazy"><figcaption>GS_Register</figcaption></figure>
<figure><img src="G_Station/Metal/GS_W_heater.jpg" alt="GS_W_heater" loading="lazy"><figcaption>GS_W_heater</figcaption></figure>

### Sky  
<figure><img src="G_Station/Sky/sky_a_front.jpg" alt="sky_a_front" loading="lazy"><figcaption>sky_a_front</figcaption></figure>

### Stucco  
<figure><img src="G_Station/Stucco/DrtyStcoWall_A.jpg" alt="DrtyStcoWall_A" loading="lazy"><figcaption>DrtyStcoWall_A</figcaption></figure>

### Tiles  
<figure><img src="G_Station/Tiles/DrtyBrwnTile_A.jpg" alt="DrtyBrwnTile_A" loading="lazy"><figcaption>DrtyBrwnTile_A</figcaption></figure>
<figure><img src="G_Station/Tiles/DrtyCeilnTile_A.jpg" alt="DrtyCeilnTile_A" loading="lazy"><figcaption>DrtyCeilnTile_A</figcaption></figure>

### Wood  
<figure><img src="G_Station/Wood/RotnWoodPlank_A.jpg" alt="RotnWoodPlank_A" loading="lazy"><figcaption>RotnWoodPlank_A</figcaption></figure>## HK_BuildingExt

### Brick  
<figure><img src="HK_BuildingExt/Brick/ClenBrckWndow_B.jpg" alt="ClenBrckWndow_B" loading="lazy"><figcaption>ClenBrckWndow_B</figcaption></figure>
<figure><img src="HK_BuildingExt/Brick/ClenWhteWall_D.jpg" alt="ClenWhteWall_D" loading="lazy"><figcaption>ClenWhteWall_D</figcaption></figure>
<figure><img src="HK_BuildingExt/Brick/ClenWhteWall_E.jpg" alt="ClenWhteWall_E" loading="lazy"><figcaption>ClenWhteWall_E</figcaption></figure>
<figure><img src="HK_BuildingExt/Brick/DrtyGreyBrick_A.jpg" alt="DrtyGreyBrick_A" loading="lazy"><figcaption>DrtyGreyBrick_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Brick/HKM_Bricks_01.jpg" alt="HKM_Bricks_01" loading="lazy"><figcaption>HKM_Bricks_01</figcaption></figure>
<figure><img src="HK_BuildingExt/Brick/HKM_Wall_02.jpg" alt="HKM_Wall_02" loading="lazy"><figcaption>HKM_Wall_02</figcaption></figure>
<figure><img src="HK_BuildingExt/ClenGreyMtdor_B.jpg" alt="ClenGreyMtdor_B" loading="lazy"><figcaption>ClenGreyMtdor_B</figcaption></figure>
<figure><img src="HK_BuildingExt/ClenGreyWndow_A.jpg" alt="ClenGreyWndow_A" loading="lazy"><figcaption>ClenGreyWndow_A</figcaption></figure>
<figure><img src="HK_BuildingExt/ClenGreyWndow_B.jpg" alt="ClenGreyWndow_B" loading="lazy"><figcaption>ClenGreyWndow_B</figcaption></figure>

### Concrete  
<figure><img src="HK_BuildingExt/Concrete/ClenBlueWall_A.jpg" alt="ClenBlueWall_A" loading="lazy"><figcaption>ClenBlueWall_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenBrckWndow_A.jpg" alt="ClenBrckWndow_A" loading="lazy"><figcaption>ClenBrckWndow_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenBrwnWndow_A.jpg" alt="ClenBrwnWndow_A" loading="lazy"><figcaption>ClenBrwnWndow_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenBrwnWndow_B.jpg" alt="ClenBrwnWndow_B" loading="lazy"><figcaption>ClenBrwnWndow_B</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenBrwnWndow_C.jpg" alt="ClenBrwnWndow_C" loading="lazy"><figcaption>ClenBrwnWndow_C</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenGreyCncrt_A.jpg" alt="ClenGreyCncrt_A" loading="lazy"><figcaption>ClenGreyCncrt_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenGreyCncrt_B.jpg" alt="ClenGreyCncrt_B" loading="lazy"><figcaption>ClenGreyCncrt_B</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenGreySdwlk_A.jpg" alt="ClenGreySdwlk_A" loading="lazy"><figcaption>ClenGreySdwlk_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenStonWndow_A.jpg" alt="ClenStonWndow_A" loading="lazy"><figcaption>ClenStonWndow_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenStonWndow_B.jpg" alt="ClenStonWndow_B" loading="lazy"><figcaption>ClenStonWndow_B</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenVent_Wall_A.jpg" alt="ClenVent_Wall_A" loading="lazy"><figcaption>ClenVent_Wall_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenWhteWall_A.jpg" alt="ClenWhteWall_A" loading="lazy"><figcaption>ClenWhteWall_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenWhteWall_F.jpg" alt="ClenWhteWall_F" loading="lazy"><figcaption>ClenWhteWall_F</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenWhteWall_G.jpg" alt="ClenWhteWall_G" loading="lazy"><figcaption>ClenWhteWall_G</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/ClenWhteWall_H.jpg" alt="ClenWhteWall_H" loading="lazy"><figcaption>ClenWhteWall_H</figcaption></figure>
<figure><img src="HK_BuildingExt/Concrete/DamgLoftWall_A.jpg" alt="DamgLoftWall_A" loading="lazy"><figcaption>DamgLoftWall_A</figcaption></figure>

### Metal  
<figure><img src="HK_BuildingExt/Metal/ClenCmntWall__A.jpg" alt="ClenCmntWall__A" loading="lazy"><figcaption>ClenCmntWall__A</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/ClenGreyMtdor_A.jpg" alt="ClenGreyMtdor_A" loading="lazy"><figcaption>ClenGreyMtdor_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/ClenWall_Vent_A.jpg" alt="ClenWall_Vent_A" loading="lazy"><figcaption>ClenWall_Vent_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/ClenWhteWall_B.jpg" alt="ClenWhteWall_B" loading="lazy"><figcaption>ClenWhteWall_B</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/ClenWhteWall_C.jpg" alt="ClenWhteWall_C" loading="lazy"><figcaption>ClenWhteWall_C</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/PitdMetalWall_A.jpg" alt="PitdMetalWall_A" loading="lazy"><figcaption>PitdMetalWall_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/RustMetlGrate_A.jpg" alt="RustMetlGrate_A" loading="lazy"><figcaption>RustMetlGrate_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Metal/RustMetlGrate_B.jpg" alt="RustMetlGrate_B" loading="lazy"><figcaption>RustMetlGrate_B</figcaption></figure>

### Wood  
<figure><img src="HK_BuildingExt/Wood/ClenBrownWood_A.jpg" alt="ClenBrownWood_A" loading="lazy"><figcaption>ClenBrownWood_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Wood/ClenBrwnSngle_A.jpg" alt="ClenBrwnSngle_A" loading="lazy"><figcaption>ClenBrwnSngle_A</figcaption></figure>
<figure><img src="HK_BuildingExt/Wood/ClenTmpl_Wall_F.jpg" alt="ClenTmpl_Wall_F" loading="lazy"><figcaption>ClenTmpl_Wall_F</figcaption></figure>
<figure><img src="HK_BuildingExt/Wood/HKM_Wall_01.jpg" alt="HKM_Wall_01" loading="lazy"><figcaption>HKM_Wall_01</figcaption></figure>## HK_Decorations

### Ceramic  
<figure><img src="HK_Decorations/Ceramic/hakke.jpg" alt="hakke" loading="lazy"><figcaption>hakke</figcaption></figure>
<figure><img src="HK_Decorations/Ceramic/horsetile.jpg" alt="horsetile" loading="lazy"><figcaption>horsetile</figcaption></figure>
<figure><img src="HK_Decorations/Ceramic/tile2.jpg" alt="tile2" loading="lazy"><figcaption>tile2</figcaption></figure>
<figure><img src="HK_Decorations/ClenMultBaner_A.jpg" alt="ClenMultBaner_A" loading="lazy"><figcaption>ClenMultBaner_A</figcaption></figure>
<figure><img src="HK_Decorations/ClenMultBaner_B.jpg" alt="ClenMultBaner_B" loading="lazy"><figcaption>ClenMultBaner_B</figcaption></figure>

### Textile  
<figure><img src="HK_Decorations/Textile/art1.jpg" alt="art1" loading="lazy"><figcaption>art1</figcaption></figure>
<figure><img src="HK_Decorations/Textile/art2.jpg" alt="art2" loading="lazy"><figcaption>art2</figcaption></figure>
<figure><img src="HK_Decorations/Textile/art3.jpg" alt="art3" loading="lazy"><figcaption>art3</figcaption></figure>
<figure><img src="HK_Decorations/Textile/art4.jpg" alt="art4" loading="lazy"><figcaption>art4</figcaption></figure>
<figure><img src="HK_Decorations/Textile/art5.jpg" alt="art5" loading="lazy"><figcaption>art5</figcaption></figure>
<figure><img src="HK_Decorations/Textile/art6.jpg" alt="art6" loading="lazy"><figcaption>art6</figcaption></figure>
<figure><img src="HK_Decorations/Textile/art7.jpg" alt="art7" loading="lazy"><figcaption>art7</figcaption></figure>## HK_Helibase
<figure><img src="HK_Helibase/Cenfold_A.jpg" alt="Cenfold_A" loading="lazy"><figcaption>Cenfold_A</figcaption></figure>
<figure><img src="HK_Helibase/ChoprPoster_A.jpg" alt="ChoprPoster_A" loading="lazy"><figcaption>ChoprPoster_A</figcaption></figure>

### Concrete  
<figure><img src="HK_Helibase/Concrete/BathroomWall_A.jpg" alt="BathroomWall_A" loading="lazy"><figcaption>BathroomWall_A</figcaption></figure>
<figure><img src="HK_Helibase/Concrete/HelibaseWall_C.jpg" alt="HelibaseWall_C" loading="lazy"><figcaption>HelibaseWall_C</figcaption></figure>
<figure><img src="HK_Helibase/Concrete/HelibaseWall_D.jpg" alt="HelibaseWall_D" loading="lazy"><figcaption>HelibaseWall_D</figcaption></figure>
<figure><img src="HK_Helibase/Concrete/WallTile_A.jpg" alt="WallTile_A" loading="lazy"><figcaption>WallTile_A</figcaption></figure>
<figure><img src="HK_Helibase/HB_ctrlpanel.jpg" alt="HB_ctrlpanel" loading="lazy"><figcaption>HB_ctrlpanel</figcaption></figure>
<figure><img src="HK_Helibase/HelibaseLght_A.jpg" alt="HelibaseLght_A" loading="lazy"><figcaption>HelibaseLght_A</figcaption></figure>

### Metal  
<figure><img src="HK_Helibase/Metal/AF_Heli_PAD.jpg" alt="AF_Heli_PAD" loading="lazy"><figcaption>AF_Heli_PAD</figcaption></figure>
<figure><img src="HK_Helibase/Metal/DrtyBrwnWall_A.jpg" alt="DrtyBrwnWall_A" loading="lazy"><figcaption>DrtyBrwnWall_A</figcaption></figure>
<figure><img src="HK_Helibase/Metal/HelibaseWall_E.jpg" alt="HelibaseWall_E" loading="lazy"><figcaption>HelibaseWall_E</figcaption></figure>
<figure><img src="HK_Helibase/Metal/Metal_A.jpg" alt="Metal_A" loading="lazy"><figcaption>Metal_A</figcaption></figure>
<figure><img src="HK_Helibase/Metal/TankPanel_A.jpg" alt="TankPanel_A" loading="lazy"><figcaption>TankPanel_A</figcaption></figure>
<figure><img src="HK_Helibase/Metal/TankPanel_B.jpg" alt="TankPanel_B" loading="lazy"><figcaption>TankPanel_B</figcaption></figure>
<figure><img src="HK_Helibase/Metal/TankPanel_C.jpg" alt="TankPanel_C" loading="lazy"><figcaption>TankPanel_C</figcaption></figure>
<figure><img src="HK_Helibase/Metal/TankPanel_D.jpg" alt="TankPanel_D" loading="lazy"><figcaption>TankPanel_D</figcaption></figure>
<figure><img src="HK_Helibase/Metal/lockers.jpg" alt="lockers" loading="lazy"><figcaption>lockers</figcaption></figure>
<figure><img src="HK_Helibase/Pipe_A.jpg" alt="Pipe_A" loading="lazy"><figcaption>Pipe_A</figcaption></figure>
<figure><img src="HK_Helibase/Pipe_B.jpg" alt="Pipe_B" loading="lazy"><figcaption>Pipe_B</figcaption></figure>
<figure><img src="HK_Helibase/Pipe_C.jpg" alt="Pipe_C" loading="lazy"><figcaption>Pipe_C</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBLkdwn.jpg" alt="Sn_HBLkdwn" loading="lazy"><figcaption>Sn_HBLkdwn</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBRobots.jpg" alt="Sn_HBRobots" loading="lazy"><figcaption>Sn_HBRobots</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBbrks.jpg" alt="Sn_HBbrks" loading="lazy"><figcaption>Sn_HBbrks</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBelevtr.jpg" alt="Sn_HBelevtr" loading="lazy"><figcaption>Sn_HBelevtr</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBfltcd1.jpg" alt="Sn_HBfltcd1" loading="lazy"><figcaption>Sn_HBfltcd1</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBfltcd1n2.jpg" alt="Sn_HBfltcd1n2" loading="lazy"><figcaption>Sn_HBfltcd1n2</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBfltcd2.jpg" alt="Sn_HBfltcd2" loading="lazy"><figcaption>Sn_HBfltcd2</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBfltcds.jpg" alt="Sn_HBfltcds" loading="lazy"><figcaption>Sn_HBfltcds</figcaption></figure>
<figure><img src="HK_Helibase/Sn_HBhazgas.jpg" alt="Sn_HBhazgas" loading="lazy"><figcaption>Sn_HBhazgas</figcaption></figure>## HK_Interior
<figure><img src="HK_Interior/Light1.jpg" alt="Light1" loading="lazy"><figcaption>Light1</figcaption></figure>

### Metal  
<figure><img src="HK_Interior/Metal/ClenBrwnLtice_A.jpg" alt="ClenBrwnLtice_A" loading="lazy"><figcaption>ClenBrwnLtice_A</figcaption></figure>
<figure><img src="HK_Interior/Metal/ClenBrwnLtice_B.jpg" alt="ClenBrwnLtice_B" loading="lazy"><figcaption>ClenBrwnLtice_B</figcaption></figure>
<figure><img src="HK_Interior/Metal/ClenBrwnPatrn_A.jpg" alt="ClenBrwnPatrn_A" loading="lazy"><figcaption>ClenBrwnPatrn_A</figcaption></figure>
<figure><img src="HK_Interior/Metal/HexagonTile.jpg" alt="HexagonTile" loading="lazy"><figcaption>HexagonTile</figcaption></figure>

### Stone  
<figure><img src="HK_Interior/Stone/ClenBrwnLight_A.jpg" alt="ClenBrwnLight_A" loading="lazy"><figcaption>ClenBrwnLight_A</figcaption></figure>
<figure><img src="HK_Interior/Stone/ClenClub_Wall_A.jpg" alt="ClenClub_Wall_A" loading="lazy"><figcaption>ClenClub_Wall_A</figcaption></figure>
<figure><img src="HK_Interior/Stone/ClenLobyPatrn_A.jpg" alt="ClenLobyPatrn_A" loading="lazy"><figcaption>ClenLobyPatrn_A</figcaption></figure>
<figure><img src="HK_Interior/Stone/ClenPtrnMarbl_A.jpg" alt="ClenPtrnMarbl_A" loading="lazy"><figcaption>ClenPtrnMarbl_A</figcaption></figure>
<figure><img src="HK_Interior/Stone/MarbWall_01.jpg" alt="MarbWall_01" loading="lazy"><figcaption>MarbWall_01</figcaption></figure>
<figure><img src="HK_Interior/Stone/fabric1.jpg" alt="fabric1" loading="lazy"><figcaption>fabric1</figcaption></figure>
<figure><img src="HK_Interior/Stone/fishtile.jpg" alt="fishtile" loading="lazy"><figcaption>fishtile</figcaption></figure>
<figure><img src="HK_Interior/Stone/rug01.jpg" alt="rug01" loading="lazy"><figcaption>rug01</figcaption></figure>

### Stucco  
<figure><img src="HK_Interior/Stucco/ClenTmpl_Wall_B.jpg" alt="ClenTmpl_Wall_B" loading="lazy"><figcaption>ClenTmpl_Wall_B</figcaption></figure>
<figure><img src="HK_Interior/Stucco/ClenTmpl_Wall_C.jpg" alt="ClenTmpl_Wall_C" loading="lazy"><figcaption>ClenTmpl_Wall_C</figcaption></figure>

### Textile  
<figure><img src="HK_Interior/Textile/ClenChinaFlag_A.jpg" alt="ClenChinaFlag_A" loading="lazy"><figcaption>ClenChinaFlag_A</figcaption></figure>
<figure><img src="HK_Interior/Textile/ClenGrenWlppr_A.jpg" alt="ClenGrenWlppr_A" loading="lazy"><figcaption>ClenGrenWlppr_A</figcaption></figure>
<figure><img src="HK_Interior/Textile/ClenWallPtrn_A.jpg" alt="ClenWallPtrn_A" loading="lazy"><figcaption>ClenWallPtrn_A</figcaption></figure>
<figure><img src="HK_Interior/Textile/ClenWallPtrn_C.jpg" alt="ClenWallPtrn_C" loading="lazy"><figcaption>ClenWallPtrn_C</figcaption></figure>
<figure><img src="HK_Interior/Textile/FlwrCrptPatrn_A.jpg" alt="FlwrCrptPatrn_A" loading="lazy"><figcaption>FlwrCrptPatrn_A</figcaption></figure>
<figure><img src="HK_Interior/Textile/HKM_Rug_04.jpg" alt="HKM_Rug_04" loading="lazy"><figcaption>HKM_Rug_04</figcaption></figure>
<figure><img src="HK_Interior/Textile/floor2.jpg" alt="floor2" loading="lazy"><figcaption>floor2</figcaption></figure>
<figure><img src="HK_Interior/Textile/screen1.jpg" alt="screen1" loading="lazy"><figcaption>screen1</figcaption></figure>

### Wood  
<figure><img src="HK_Interior/Wood/ClenBrownScren_.jpg" alt="ClenBrownScren_" loading="lazy"><figcaption>ClenBrownScren_</figcaption></figure>
<figure><img src="HK_Interior/Wood/ClenBrwnFloor_A.jpg" alt="ClenBrwnFloor_A" loading="lazy"><figcaption>ClenBrwnFloor_A</figcaption></figure>
<figure><img src="HK_Interior/Wood/ClenBrwnScren_A.jpg" alt="ClenBrwnScren_A" loading="lazy"><figcaption>ClenBrwnScren_A</figcaption></figure>
<figure><img src="HK_Interior/Wood/ClenBrwnScren_B.jpg" alt="ClenBrwnScren_B" loading="lazy"><figcaption>ClenBrwnScren_B</figcaption></figure>
<figure><img src="HK_Interior/Wood/ClenTmpl_Wall_E.jpg" alt="ClenTmpl_Wall_E" loading="lazy"><figcaption>ClenTmpl_Wall_E</figcaption></figure>
<figure><img src="HK_Interior/Wood/ClenTmpl_Wall_G.jpg" alt="ClenTmpl_Wall_G" loading="lazy"><figcaption>ClenTmpl_Wall_G</figcaption></figure>
<figure><img src="HK_Interior/Wood/ClenTmpl_Wall_I.jpg" alt="ClenTmpl_Wall_I" loading="lazy"><figcaption>ClenTmpl_Wall_I</figcaption></figure>
<figure><img src="HK_Interior/Wood/OldBrwnDoor_A.jpg" alt="OldBrwnDoor_A" loading="lazy"><figcaption>OldBrwnDoor_A</figcaption></figure>
<figure><img src="HK_Interior/Wood/floor3.jpg" alt="floor3" loading="lazy"><figcaption>floor3</figcaption></figure>
<figure><img src="HK_Interior/Wood/fltile1.jpg" alt="fltile1" loading="lazy"><figcaption>fltile1</figcaption></figure>
<figure><img src="HK_Interior/light1b.jpg" alt="light1b" loading="lazy"><figcaption>light1b</figcaption></figure>## HK_MJ12Lab
<figure><img src="HK_MJ12Lab/HK_light_01.jpg" alt="HK_light_01" loading="lazy"><figcaption>HK_light_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/Keyboard_01.jpg" alt="Keyboard_01" loading="lazy"><figcaption>Keyboard_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/LAB_Light_01.jpg" alt="LAB_Light_01" loading="lazy"><figcaption>LAB_Light_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/LABsign_01.jpg" alt="LABsign_01" loading="lazy"><figcaption>LABsign_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/LABsign_02.jpg" alt="LABsign_02" loading="lazy"><figcaption>LABsign_02</figcaption></figure>
<figure><img src="HK_MJ12Lab/LabWall_05.jpg" alt="LabWall_05" loading="lazy"><figcaption>LabWall_05</figcaption></figure>
<figure><img src="HK_MJ12Lab/Lights_A.jpg" alt="Lights_A" loading="lazy"><figcaption>Lights_A</figcaption></figure>

### Metal  
<figure><img src="HK_MJ12Lab/Metal/CmptrWall_01.jpg" alt="CmptrWall_01" loading="lazy"><figcaption>CmptrWall_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/CmptrWall_02.jpg" alt="CmptrWall_02" loading="lazy"><figcaption>CmptrWall_02</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/CmptrWall_03.jpg" alt="CmptrWall_03" loading="lazy"><figcaption>CmptrWall_03</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/CmptrWall_04.jpg" alt="CmptrWall_04" loading="lazy"><figcaption>CmptrWall_04</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/HK_strwal_01.jpg" alt="HK_strwal_01" loading="lazy"><figcaption>HK_strwal_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/HK_strwal_02.jpg" alt="HK_strwal_02" loading="lazy"><figcaption>HK_strwal_02</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/HK_strwal_03.jpg" alt="HK_strwal_03" loading="lazy"><figcaption>HK_strwal_03</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/HK_strwal_04.jpg" alt="HK_strwal_04" loading="lazy"><figcaption>HK_strwal_04</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/LabFlr_01.jpg" alt="LabFlr_01" loading="lazy"><figcaption>LabFlr_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/LabWall_02.jpg" alt="LabWall_02" loading="lazy"><figcaption>LabWall_02</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/LabWall_04.jpg" alt="LabWall_04" loading="lazy"><figcaption>LabWall_04</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/LabWall_06.jpg" alt="LabWall_06" loading="lazy"><figcaption>LabWall_06</figcaption></figure>
<figure><img src="HK_MJ12Lab/Metal/LabWall_07.jpg" alt="LabWall_07" loading="lazy"><figcaption>LabWall_07</figcaption></figure>
<figure><img src="HK_MJ12Lab/RedHallArch.jpg" alt="RedHallArch" loading="lazy"><figcaption>RedHallArch</figcaption></figure>
<figure><img src="HK_MJ12Lab/RedWall_01.jpg" alt="RedWall_01" loading="lazy"><figcaption>RedWall_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/Screen_01.jpg" alt="Screen_01" loading="lazy"><figcaption>Screen_01</figcaption></figure>
<figure><img src="HK_MJ12Lab/UnlitBlue_A.jpg" alt="UnlitBlue_A" loading="lazy"><figcaption>UnlitBlue_A</figcaption></figure>
<figure><img src="HK_MJ12Lab/cp01.jpg" alt="cp01" loading="lazy"><figcaption>cp01</figcaption></figure>
<figure><img src="HK_MJ12Lab/cp02.jpg" alt="cp02" loading="lazy"><figcaption>cp02</figcaption></figure>
<figure><img src="HK_MJ12Lab/cp03.jpg" alt="cp03" loading="lazy"><figcaption>cp03</figcaption></figure>## HK_Signs
<figure><img src="HK_Signs/ChnaHandSign_A.jpg" alt="ChnaHandSign_A" loading="lazy"><figcaption>ChnaHandSign_A</figcaption></figure>
<figure><img src="HK_Signs/ClenBlueSign_A.jpg" alt="ClenBlueSign_A" loading="lazy"><figcaption>ClenBlueSign_A</figcaption></figure>
<figure><img src="HK_Signs/ClenNeonSign_B.jpg" alt="ClenNeonSign_B" loading="lazy"><figcaption>ClenNeonSign_B</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_B.jpg" alt="ClenReddSign_B" loading="lazy"><figcaption>ClenReddSign_B</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_C.jpg" alt="ClenReddSign_C" loading="lazy"><figcaption>ClenReddSign_C</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_E.jpg" alt="ClenReddSign_E" loading="lazy"><figcaption>ClenReddSign_E</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_G.jpg" alt="ClenReddSign_G" loading="lazy"><figcaption>ClenReddSign_G</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_H.jpg" alt="ClenReddSign_H" loading="lazy"><figcaption>ClenReddSign_H</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_K.jpg" alt="ClenReddSign_K" loading="lazy"><figcaption>ClenReddSign_K</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_T.jpg" alt="ClenReddSign_T" loading="lazy"><figcaption>ClenReddSign_T</figcaption></figure>
<figure><img src="HK_Signs/ClenReddSign_V.jpg" alt="ClenReddSign_V" loading="lazy"><figcaption>ClenReddSign_V</figcaption></figure>
<figure><img src="HK_Signs/ClenblwhtSign_A.jpg" alt="ClenblwhtSign_A" loading="lazy"><figcaption>ClenblwhtSign_A</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_02.jpg" alt="HK_Sign_02" loading="lazy"><figcaption>HK_Sign_02</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_05.jpg" alt="HK_Sign_05" loading="lazy"><figcaption>HK_Sign_05</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_06.jpg" alt="HK_Sign_06" loading="lazy"><figcaption>HK_Sign_06</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_07.jpg" alt="HK_Sign_07" loading="lazy"><figcaption>HK_Sign_07</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_08.jpg" alt="HK_Sign_08" loading="lazy"><figcaption>HK_Sign_08</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_09.jpg" alt="HK_Sign_09" loading="lazy"><figcaption>HK_Sign_09</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_10.jpg" alt="HK_Sign_10" loading="lazy"><figcaption>HK_Sign_10</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_11.jpg" alt="HK_Sign_11" loading="lazy"><figcaption>HK_Sign_11</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_12.jpg" alt="HK_Sign_12" loading="lazy"><figcaption>HK_Sign_12</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_13.jpg" alt="HK_Sign_13" loading="lazy"><figcaption>HK_Sign_13</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_14.jpg" alt="HK_Sign_14" loading="lazy"><figcaption>HK_Sign_14</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_15.jpg" alt="HK_Sign_15" loading="lazy"><figcaption>HK_Sign_15</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_18.jpg" alt="HK_Sign_18" loading="lazy"><figcaption>HK_Sign_18</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_23.jpg" alt="HK_Sign_23" loading="lazy"><figcaption>HK_Sign_23</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_24.jpg" alt="HK_Sign_24" loading="lazy"><figcaption>HK_Sign_24</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_25.jpg" alt="HK_Sign_25" loading="lazy"><figcaption>HK_Sign_25</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_28.jpg" alt="HK_Sign_28" loading="lazy"><figcaption>HK_Sign_28</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_Kiosk1.jpg" alt="HK_Sign_Kiosk1" loading="lazy"><figcaption>HK_Sign_Kiosk1</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_LM.jpg" alt="HK_Sign_LM" loading="lazy"><figcaption>HK_Sign_LM</figcaption></figure>
<figure><img src="HK_Signs/HK_Sign_LM2.jpg" alt="HK_Sign_LM2" loading="lazy"><figcaption>HK_Sign_LM2</figcaption></figure>
<figure><img src="HK_Signs/LiteGrenSign_A.jpg" alt="LiteGrenSign_A" loading="lazy"><figcaption>LiteGrenSign_A</figcaption></figure>
<figure><img src="HK_Signs/LiteGrenSign_B.jpg" alt="LiteGrenSign_B" loading="lazy"><figcaption>LiteGrenSign_B</figcaption></figure>

### Metal  
<figure><img src="HK_Signs/Metal/Elevator_Butns.jpg" alt="Elevator_Butns" loading="lazy"><figcaption>Elevator_Butns</figcaption></figure>
<figure><img src="HK_Signs/Metal/Elevator_down.jpg" alt="Elevator_down" loading="lazy"><figcaption>Elevator_down</figcaption></figure>
<figure><img src="HK_Signs/Metal/Elevator_up.jpg" alt="Elevator_up" loading="lazy"><figcaption>Elevator_up</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_01.jpg" alt="HK_Sign_01" loading="lazy"><figcaption>HK_Sign_01</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_03.jpg" alt="HK_Sign_03" loading="lazy"><figcaption>HK_Sign_03</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_04.jpg" alt="HK_Sign_04" loading="lazy"><figcaption>HK_Sign_04</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_19.jpg" alt="HK_Sign_19" loading="lazy"><figcaption>HK_Sign_19</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_21.jpg" alt="HK_Sign_21" loading="lazy"><figcaption>HK_Sign_21</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_22.jpg" alt="HK_Sign_22" loading="lazy"><figcaption>HK_Sign_22</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_CNRD.jpg" alt="HK_Sign_CNRD" loading="lazy"><figcaption>HK_Sign_CNRD</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_CNRD1.jpg" alt="HK_Sign_CNRD1" loading="lazy"><figcaption>HK_Sign_CNRD1</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_QTLS.jpg" alt="HK_Sign_QTLS" loading="lazy"><figcaption>HK_Sign_QTLS</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_QTLS3.jpg" alt="HK_Sign_QTLS3" loading="lazy"><figcaption>HK_Sign_QTLS3</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_QTLS4.jpg" alt="HK_Sign_QTLS4" loading="lazy"><figcaption>HK_Sign_QTLS4</figcaption></figure>
<figure><img src="HK_Signs/Metal/HK_Sign_QTLS5.jpg" alt="HK_Sign_QTLS5" loading="lazy"><figcaption>HK_Sign_QTLS5</figcaption></figure>
<figure><img src="HK_Signs/Metal/TnchRoadSign_A.jpg" alt="TnchRoadSign_A" loading="lazy"><figcaption>TnchRoadSign_A</figcaption></figure>
<figure><img src="HK_Signs/Metal/no_reentry.jpg" alt="no_reentry" loading="lazy"><figcaption>no_reentry</figcaption></figure>

### Textile  
<figure><img src="HK_Signs/Textile/ClenReddSign_U.jpg" alt="ClenReddSign_U" loading="lazy"><figcaption>ClenReddSign_U</figcaption></figure>

### Wood  
<figure><img src="HK_Signs/Wood/ClenReddSign_A.jpg" alt="ClenReddSign_A" loading="lazy"><figcaption>ClenReddSign_A</figcaption></figure>
<figure><img src="HK_Signs/Wood/HK_Sign_17.jpg" alt="HK_Sign_17" loading="lazy"><figcaption>HK_Sign_17</figcaption></figure>## HK_VersaLife

### Concrete  
<figure><img src="HK_VersaLife/Concrete/logo1.jpg" alt="logo1" loading="lazy"><figcaption>logo1</figcaption></figure>
<figure><img src="HK_VersaLife/Concrete/logo1b.jpg" alt="logo1b" loading="lazy"><figcaption>logo1b</figcaption></figure>

### Metal  
<figure><img src="HK_VersaLife/Metal/drawers01.jpg" alt="drawers01" loading="lazy"><figcaption>drawers01</figcaption></figure>

### Textile  
<figure><img src="HK_VersaLife/Textile/carpet1b.jpg" alt="carpet1b" loading="lazy"><figcaption>carpet1b</figcaption></figure>
<figure><img src="HK_VersaLife/Textile/secflor1.jpg" alt="secflor1" loading="lazy"><figcaption>secflor1</figcaption></figure>
<figure><img src="HK_VersaLife/bwash_a01.jpg" alt="bwash_a01" loading="lazy"><figcaption>bwash_a01</figcaption></figure>
<figure><img src="HK_VersaLife/bwash_a02.jpg" alt="bwash_a02" loading="lazy"><figcaption>bwash_a02</figcaption></figure>
<figure><img src="HK_VersaLife/bwash_a03.jpg" alt="bwash_a03" loading="lazy"><figcaption>bwash_a03</figcaption></figure>
<figure><img src="HK_VersaLife/bwash_a04.jpg" alt="bwash_a04" loading="lazy"><figcaption>bwash_a04</figcaption></figure>
<figure><img src="HK_VersaLife/bwash_a05.jpg" alt="bwash_a05" loading="lazy"><figcaption>bwash_a05</figcaption></figure>
<figure><img src="HK_VersaLife/bwash_a06.jpg" alt="bwash_a06" loading="lazy"><figcaption>bwash_a06</figcaption></figure>
<figure><img src="HK_VersaLife/cube1.jpg" alt="cube1" loading="lazy"><figcaption>cube1</figcaption></figure>
<figure><img src="HK_VersaLife/logo1c.jpg" alt="logo1c" loading="lazy"><figcaption>logo1c</figcaption></figure>
<figure><img src="HK_VersaLife/zensand.jpg" alt="zensand" loading="lazy"><figcaption>zensand</figcaption></figure>## Hangar18

### Sky  
<figure><img src="Hangar18/Sky/Sunset_A.jpg" alt="Sunset_A" loading="lazy"><figcaption>Sunset_A</figcaption></figure>
<figure><img src="Hangar18/Sky/Sunset_B.jpg" alt="Sunset_B" loading="lazy"><figcaption>Sunset_B</figcaption></figure>## InfoPortraits
<figure><img src="InfoPortraits/AlexJacobson.jpg" alt="AlexJacobson" loading="lazy"><figcaption>AlexJacobson</figcaption></figure>
<figure><img src="InfoPortraits/AnnaNavarre.jpg" alt="AnnaNavarre" loading="lazy"><figcaption>AnnaNavarre</figcaption></figure>
<figure><img src="InfoPortraits/BobPage.jpg" alt="BobPage" loading="lazy"><figcaption>BobPage</figcaption></figure>
<figure><img src="InfoPortraits/BobPageAug.jpg" alt="BobPageAug" loading="lazy"><figcaption>BobPageAug</figcaption></figure>
<figure><img src="InfoPortraits/Daedalus.jpg" alt="Daedalus" loading="lazy"><figcaption>Daedalus</figcaption></figure>
<figure><img src="InfoPortraits/GarySavage.jpg" alt="GarySavage" loading="lazy"><figcaption>GarySavage</figcaption></figure>
<figure><img src="InfoPortraits/GuntherHermann.jpg" alt="GuntherHermann" loading="lazy"><figcaption>GuntherHermann</figcaption></figure>
<figure><img src="InfoPortraits/Icarus.jpg" alt="Icarus" loading="lazy"><figcaption>Icarus</figcaption></figure>
<figure><img src="InfoPortraits/JaimeReyes.jpg" alt="JaimeReyes" loading="lazy"><figcaption>JaimeReyes</figcaption></figure>
<figure><img src="InfoPortraits/Jock.jpg" alt="Jock" loading="lazy"><figcaption>Jock</figcaption></figure>
<figure><img src="InfoPortraits/MorganEverett.jpg" alt="MorganEverett" loading="lazy"><figcaption>MorganEverett</figcaption></figure>
<figure><img src="InfoPortraits/PaulDenton_1.jpg" alt="PaulDenton_1" loading="lazy"><figcaption>PaulDenton_1</figcaption></figure>
<figure><img src="InfoPortraits/PaulDenton_2.jpg" alt="PaulDenton_2" loading="lazy"><figcaption>PaulDenton_2</figcaption></figure>
<figure><img src="InfoPortraits/PaulDenton_3.jpg" alt="PaulDenton_3" loading="lazy"><figcaption>PaulDenton_3</figcaption></figure>
<figure><img src="InfoPortraits/PaulDenton_4.jpg" alt="PaulDenton_4" loading="lazy"><figcaption>PaulDenton_4</figcaption></figure>
<figure><img src="InfoPortraits/PaulDenton_5.jpg" alt="PaulDenton_5" loading="lazy"><figcaption>PaulDenton_5</figcaption></figure>
<figure><img src="InfoPortraits/SamCarter.jpg" alt="SamCarter" loading="lazy"><figcaption>SamCarter</figcaption></figure>
<figure><img src="InfoPortraits/StantonDowd.jpg" alt="StantonDowd" loading="lazy"><figcaption>StantonDowd</figcaption></figure>
<figure><img src="InfoPortraits/TracerTong.jpg" alt="TracerTong" loading="lazy"><figcaption>TracerTong</figcaption></figure>
<figure><img src="InfoPortraits/WaltonSimons.jpg" alt="WaltonSimons" loading="lazy"><figcaption>WaltonSimons</figcaption></figure>
<figure><img src="InfoPortraits/helios.jpg" alt="helios" loading="lazy"><figcaption>helios</figcaption></figure>## MJ12_lab

### Concrete  
<figure><img src="MJ12_lab/Concrete/Uob_Cncrthzrd_a.jpg" alt="Uob_Cncrthzrd_a" loading="lazy"><figcaption>Uob_Cncrthzrd_a</figcaption></figure>

### Misc  
<figure><img src="MJ12_lab/Misc/FileCab_siding.jpg" alt="FileCab_siding" loading="lazy"><figcaption>FileCab_siding</figcaption></figure>
<figure><img src="MJ12_lab/Misc/Uob_sign_armory.jpg" alt="Uob_sign_armory" loading="lazy"><figcaption>Uob_sign_armory</figcaption></figure>
<figure><img src="MJ12_lab/Misc/Uob_sign_botmnt.jpg" alt="Uob_sign_botmnt" loading="lazy"><figcaption>Uob_sign_botmnt</figcaption></figure>
<figure><img src="MJ12_lab/Misc/Uob_sign_cbk_a.jpg" alt="Uob_sign_cbk_a" loading="lazy"><figcaption>Uob_sign_cbk_a</figcaption></figure>
<figure><img src="MJ12_lab/Misc/Uob_sign_detntn.jpg" alt="Uob_sign_detntn" loading="lazy"><figcaption>Uob_sign_detntn</figcaption></figure>
<figure><img src="MJ12_lab/Misc/Uob_sign_medic.jpg" alt="Uob_sign_medic" loading="lazy"><figcaption>Uob_sign_medic</figcaption></figure>
<figure><img src="MJ12_lab/Misc/Uob_sign_nanotc.jpg" alt="Uob_sign_nanotc" loading="lazy"><figcaption>Uob_sign_nanotc</figcaption></figure>
<figure><img src="MJ12_lab/Misc/uob_sign_cage.jpg" alt="uob_sign_cage" loading="lazy"><figcaption>uob_sign_cage</figcaption></figure>
<figure><img src="MJ12_lab/Misc/uob_sign_cmdctr.jpg" alt="uob_sign_cmdctr" loading="lazy"><figcaption>uob_sign_cmdctr</figcaption></figure>
<figure><img src="MJ12_lab/Misc/uob_sign_labs.jpg" alt="uob_sign_labs" loading="lazy"><figcaption>uob_sign_labs</figcaption></figure>
<figure><img src="MJ12_lab/Misc/uob_sign_sward.jpg" alt="uob_sign_sward" loading="lazy"><figcaption>uob_sign_sward</figcaption></figure>

### Stone  
<figure><img src="MJ12_lab/Stone/Uob_Cncrthzrd_b.jpg" alt="Uob_Cncrthzrd_b" loading="lazy"><figcaption>Uob_Cncrthzrd_b</figcaption></figure>
<figure><img src="MJ12_lab/Stone/Uob_Cncrthzrd_c.jpg" alt="Uob_Cncrthzrd_c" loading="lazy"><figcaption>Uob_Cncrthzrd_c</figcaption></figure>
<figure><img src="MJ12_lab/Stone/Uob_Far_Wall_A.jpg" alt="Uob_Far_Wall_A" loading="lazy"><figcaption>Uob_Far_Wall_A</figcaption></figure>
<figure><img src="MJ12_lab/Stone/Uob_Far_Wall_C.jpg" alt="Uob_Far_Wall_C" loading="lazy"><figcaption>Uob_Far_Wall_C</figcaption></figure>

### doors  
<figure><img src="MJ12_lab/doors/Med_Door_1.jpg" alt="Med_Door_1" loading="lazy"><figcaption>Med_Door_1</figcaption></figure>
<figure><img src="MJ12_lab/doors/Prsn_Door_1.jpg" alt="Prsn_Door_1" loading="lazy"><figcaption>Prsn_Door_1</figcaption></figure>
<figure><img src="MJ12_lab/un_mj12light_a.jpg" alt="un_mj12light_a" loading="lazy"><figcaption>un_mj12light_a</figcaption></figure>
<figure><img src="MJ12_lab/un_mj12light_b.jpg" alt="un_mj12light_b" loading="lazy"><figcaption>un_mj12light_b</figcaption></figure>## Mobile_Camp

### Misc  
<figure><img src="Mobile_Camp/Misc/mc_wheeltrd.jpg" alt="mc_wheeltrd" loading="lazy"><figcaption>mc_wheeltrd</figcaption></figure>

### Mobile_Camp  
<figure><img src="Mobile_Camp/Mobile_Camp/mc_cmptr_lg_a.jpg" alt="mc_cmptr_lg_a" loading="lazy"><figcaption>mc_cmptr_lg_a</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_cmptr_lg_b.jpg" alt="mc_cmptr_lg_b" loading="lazy"><figcaption>mc_cmptr_lg_b</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_cmptrpanel_b.jpg" alt="mc_cmptrpanel_b" loading="lazy"><figcaption>mc_cmptrpanel_b</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_cmptrpanel_c.jpg" alt="mc_cmptrpanel_c" loading="lazy"><figcaption>mc_cmptrpanel_c</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_elctrcbox_a.jpg" alt="mc_elctrcbox_a" loading="lazy"><figcaption>mc_elctrcbox_a</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_lrgcmptr_A00.jpg" alt="mc_lrgcmptr_A00" loading="lazy"><figcaption>mc_lrgcmptr_A00</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_lrgcmptr_A01.jpg" alt="mc_lrgcmptr_A01" loading="lazy"><figcaption>mc_lrgcmptr_A01</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_lrgcmptr_A02.jpg" alt="mc_lrgcmptr_A02" loading="lazy"><figcaption>mc_lrgcmptr_A02</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_metal_a.jpg" alt="mc_metal_a" loading="lazy"><figcaption>mc_metal_a</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_trailer_a.jpg" alt="mc_trailer_a" loading="lazy"><figcaption>mc_trailer_a</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_trler_gnrc.jpg" alt="mc_trler_gnrc" loading="lazy"><figcaption>mc_trler_gnrc</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_trlr_exhst_a.jpg" alt="mc_trlr_exhst_a" loading="lazy"><figcaption>mc_trlr_exhst_a</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_wire_a.jpg" alt="mc_wire_a" loading="lazy"><figcaption>mc_wire_a</figcaption></figure>
<figure><img src="Mobile_Camp/Mobile_Camp/mc_wire_b.jpg" alt="mc_wire_b" loading="lazy"><figcaption>mc_wire_b</figcaption></figure>## MolePeople

### Brick  
<figure><img src="MolePeople/Brick/Nsub_wall_a.jpg" alt="Nsub_wall_a" loading="lazy"><figcaption>Nsub_wall_a</figcaption></figure>
<figure><img src="MolePeople/Brick/Nsub_wall_i.jpg" alt="Nsub_wall_i" loading="lazy"><figcaption>Nsub_wall_i</figcaption></figure>
<figure><img src="MolePeople/Brick/Nsubwaltop.jpg" alt="Nsubwaltop" loading="lazy"><figcaption>Nsubwaltop</figcaption></figure>
<figure><img src="MolePeople/Brick/sub_wall_a.jpg" alt="sub_wall_a" loading="lazy"><figcaption>sub_wall_a</figcaption></figure>
<figure><img src="MolePeople/Brick/sub_wall_b.jpg" alt="sub_wall_b" loading="lazy"><figcaption>sub_wall_b</figcaption></figure>
<figure><img src="MolePeople/Brick/subcieling1.jpg" alt="subcieling1" loading="lazy"><figcaption>subcieling1</figcaption></figure>
<figure><img src="MolePeople/Brick/subfloor1.jpg" alt="subfloor1" loading="lazy"><figcaption>subfloor1</figcaption></figure>
<figure><img src="MolePeople/Brick/subwaledg1.jpg" alt="subwaledg1" loading="lazy"><figcaption>subwaledg1</figcaption></figure>
<figure><img src="MolePeople/Brick/subwaledg2.jpg" alt="subwaledg2" loading="lazy"><figcaption>subwaledg2</figcaption></figure>
<figure><img src="MolePeople/Brick/tilwall1.jpg" alt="tilwall1" loading="lazy"><figcaption>tilwall1</figcaption></figure>

### Concrete  
<figure><img src="MolePeople/Concrete/MP_doortrim.jpg" alt="MP_doortrim" loading="lazy"><figcaption>MP_doortrim</figcaption></figure>
<figure><img src="MolePeople/Concrete/Nsub_trac_a.jpg" alt="Nsub_trac_a" loading="lazy"><figcaption>Nsub_trac_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/Nsub_trac_b.jpg" alt="Nsub_trac_b" loading="lazy"><figcaption>Nsub_trac_b</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_ceiling_a.jpg" alt="phbs_ceiling_a" loading="lazy"><figcaption>phbs_ceiling_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_floor_a.jpg" alt="phbs_floor_a" loading="lazy"><figcaption>phbs_floor_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallwall_a.jpg" alt="phbs_hallwall_a" loading="lazy"><figcaption>phbs_hallwall_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallwall_b.jpg" alt="phbs_hallwall_b" loading="lazy"><figcaption>phbs_hallwall_b</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_a.jpg" alt="phbs_hallway_a" loading="lazy"><figcaption>phbs_hallway_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_b.jpg" alt="phbs_hallway_b" loading="lazy"><figcaption>phbs_hallway_b</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_c.jpg" alt="phbs_hallway_c" loading="lazy"><figcaption>phbs_hallway_c</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_d.jpg" alt="phbs_hallway_d" loading="lazy"><figcaption>phbs_hallway_d</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_e.jpg" alt="phbs_hallway_e" loading="lazy"><figcaption>phbs_hallway_e</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_f.jpg" alt="phbs_hallway_f" loading="lazy"><figcaption>phbs_hallway_f</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_g.jpg" alt="phbs_hallway_g" loading="lazy"><figcaption>phbs_hallway_g</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_h.jpg" alt="phbs_hallway_h" loading="lazy"><figcaption>phbs_hallway_h</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_i.jpg" alt="phbs_hallway_i" loading="lazy"><figcaption>phbs_hallway_i</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_j.jpg" alt="phbs_hallway_j" loading="lazy"><figcaption>phbs_hallway_j</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_k.jpg" alt="phbs_hallway_k" loading="lazy"><figcaption>phbs_hallway_k</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_l.jpg" alt="phbs_hallway_l" loading="lazy"><figcaption>phbs_hallway_l</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_m.jpg" alt="phbs_hallway_m" loading="lazy"><figcaption>phbs_hallway_m</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_n.jpg" alt="phbs_hallway_n" loading="lazy"><figcaption>phbs_hallway_n</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hallway_o.jpg" alt="phbs_hallway_o" loading="lazy"><figcaption>phbs_hallway_o</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hllwybrnt.jpg" alt="phbs_hllwybrnt" loading="lazy"><figcaption>phbs_hllwybrnt</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_hlwybrnt_a.jpg" alt="phbs_hlwybrnt_a" loading="lazy"><figcaption>phbs_hlwybrnt_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_lrgepipe_a.jpg" alt="phbs_lrgepipe_a" loading="lazy"><figcaption>phbs_lrgepipe_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_pipe_a.jpg" alt="phbs_pipe_a" loading="lazy"><figcaption>phbs_pipe_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_str_wall_a.jpg" alt="phbs_str_wall_a" loading="lazy"><figcaption>phbs_str_wall_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_a.jpg" alt="phbs_wall_a" loading="lazy"><figcaption>phbs_wall_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_b.jpg" alt="phbs_wall_b" loading="lazy"><figcaption>phbs_wall_b</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_c.jpg" alt="phbs_wall_c" loading="lazy"><figcaption>phbs_wall_c</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_d.jpg" alt="phbs_wall_d" loading="lazy"><figcaption>phbs_wall_d</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_e.jpg" alt="phbs_wall_e" loading="lazy"><figcaption>phbs_wall_e</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_f.jpg" alt="phbs_wall_f" loading="lazy"><figcaption>phbs_wall_f</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_g.jpg" alt="phbs_wall_g" loading="lazy"><figcaption>phbs_wall_g</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_h.jpg" alt="phbs_wall_h" loading="lazy"><figcaption>phbs_wall_h</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_i.jpg" alt="phbs_wall_i" loading="lazy"><figcaption>phbs_wall_i</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_j.jpg" alt="phbs_wall_j" loading="lazy"><figcaption>phbs_wall_j</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_k.jpg" alt="phbs_wall_k" loading="lazy"><figcaption>phbs_wall_k</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_l.jpg" alt="phbs_wall_l" loading="lazy"><figcaption>phbs_wall_l</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_m.jpg" alt="phbs_wall_m" loading="lazy"><figcaption>phbs_wall_m</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wall_n.jpg" alt="phbs_wall_n" loading="lazy"><figcaption>phbs_wall_n</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wallhole_a.jpg" alt="phbs_wallhole_a" loading="lazy"><figcaption>phbs_wallhole_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wterwall_a.jpg" alt="phbs_wterwall_a" loading="lazy"><figcaption>phbs_wterwall_a</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wterwall_b.jpg" alt="phbs_wterwall_b" loading="lazy"><figcaption>phbs_wterwall_b</figcaption></figure>
<figure><img src="MolePeople/Concrete/phbs_wterwall_c.jpg" alt="phbs_wterwall_c" loading="lazy"><figcaption>phbs_wterwall_c</figcaption></figure>

### Glass  
<figure><img src="MolePeople/Glass/NsubMiror_c.jpg" alt="NsubMiror_c" loading="lazy"><figcaption>NsubMiror_c</figcaption></figure>

### Ladder  
<figure><img src="MolePeople/Ladder/phbs_ladder_a.jpg" alt="phbs_ladder_a" loading="lazy"><figcaption>phbs_ladder_a</figcaption></figure>

### Metal  
<figure><img src="MolePeople/Metal/NYmanholecov.jpg" alt="NYmanholecov" loading="lazy"><figcaption>NYmanholecov</figcaption></figure>
<figure><img src="MolePeople/Metal/WirePanel.jpg" alt="WirePanel" loading="lazy"><figcaption>WirePanel</figcaption></figure>
<figure><img src="MolePeople/Metal/bolts1.jpg" alt="bolts1" loading="lazy"><figcaption>bolts1</figcaption></figure>
<figure><img src="MolePeople/Metal/booth1.jpg" alt="booth1" loading="lazy"><figcaption>booth1</figcaption></figure>
<figure><img src="MolePeople/Metal/mp_StairFront.jpg" alt="mp_StairFront" loading="lazy"><figcaption>mp_StairFront</figcaption></figure>
<figure><img src="MolePeople/Metal/riser1.jpg" alt="riser1" loading="lazy"><figcaption>riser1</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_beam_a.jpg" alt="sub_beam_a" loading="lazy"><figcaption>sub_beam_a</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_beam_a2.jpg" alt="sub_beam_a2" loading="lazy"><figcaption>sub_beam_a2</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_beam_b.jpg" alt="sub_beam_b" loading="lazy"><figcaption>sub_beam_b</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_floor_a.jpg" alt="sub_floor_a" loading="lazy"><figcaption>sub_floor_a</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_pole_a.jpg" alt="sub_pole_a" loading="lazy"><figcaption>sub_pole_a</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_pole_b.jpg" alt="sub_pole_b" loading="lazy"><figcaption>sub_pole_b</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_wall_c.jpg" alt="sub_wall_c" loading="lazy"><figcaption>sub_wall_c</figcaption></figure>
<figure><img src="MolePeople/Metal/sub_wall_c2.jpg" alt="sub_wall_c2" loading="lazy"><figcaption>sub_wall_c2</figcaption></figure>

### Signs  
<figure><img src="MolePeople/Signs/BigTopsCig_sign.jpg" alt="BigTopsCig_sign" loading="lazy"><figcaption>BigTopsCig_sign</figcaption></figure>
<figure><img src="MolePeople/Signs/JadeDragon_sign.jpg" alt="JadeDragon_sign" loading="lazy"><figcaption>JadeDragon_sign</figcaption></figure>
<figure><img src="MolePeople/Signs/NYsubmap.jpg" alt="NYsubmap" loading="lazy"><figcaption>NYsubmap</figcaption></figure>
<figure><img src="MolePeople/Signs/NoJokeCig_sign.jpg" alt="NoJokeCig_sign" loading="lazy"><figcaption>NoJokeCig_sign</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_aL.jpg" alt="Nsub_sign_aL" loading="lazy"><figcaption>Nsub_sign_aL</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_aR.jpg" alt="Nsub_sign_aR" loading="lazy"><figcaption>Nsub_sign_aR</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_bL.jpg" alt="Nsub_sign_bL" loading="lazy"><figcaption>Nsub_sign_bL</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_bR.jpg" alt="Nsub_sign_bR" loading="lazy"><figcaption>Nsub_sign_bR</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_cL.jpg" alt="Nsub_sign_cL" loading="lazy"><figcaption>Nsub_sign_cL</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_cR.jpg" alt="Nsub_sign_cR" loading="lazy"><figcaption>Nsub_sign_cR</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_e.jpg" alt="Nsub_sign_e" loading="lazy"><figcaption>Nsub_sign_e</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_fL.jpg" alt="Nsub_sign_fL" loading="lazy"><figcaption>Nsub_sign_fL</figcaption></figure>
<figure><img src="MolePeople/Signs/Nsub_sign_fR.jpg" alt="Nsub_sign_fR" loading="lazy"><figcaption>Nsub_sign_fR</figcaption></figure>
<figure><img src="MolePeople/Signs/S45cigs_sign.jpg" alt="S45cigs_sign" loading="lazy"><figcaption>S45cigs_sign</figcaption></figure>
<figure><img src="MolePeople/Signs/Super45a_sign.jpg" alt="Super45a_sign" loading="lazy"><figcaption>Super45a_sign</figcaption></figure>
<figure><img src="MolePeople/Signs/Super45b_sign.jpg" alt="Super45b_sign" loading="lazy"><figcaption>Super45b_sign</figcaption></figure>
<figure><img src="MolePeople/Signs/sub_logo_a.jpg" alt="sub_logo_a" loading="lazy"><figcaption>sub_logo_a</figcaption></figure>## Moon

### Concrete  
<figure><img src="Moon/Concrete/concrete7b.jpg" alt="concrete7b" loading="lazy"><figcaption>concrete7b</figcaption></figure>

### Earth  
<figure><img src="Moon/Earth/Moonscape7.jpg" alt="Moonscape7" loading="lazy"><figcaption>Moonscape7</figcaption></figure>

### Foliage  
<figure><img src="Moon/Foliage/vines.jpg" alt="vines" loading="lazy"><figcaption>vines</figcaption></figure>

### Glass  
<figure><img src="Moon/Glass/Tube_A01.jpg" alt="Tube_A01" loading="lazy"><figcaption>Tube_A01</figcaption></figure>
<figure><img src="Moon/Glass/Tube_A02.jpg" alt="Tube_A02" loading="lazy"><figcaption>Tube_A02</figcaption></figure>
<figure><img src="Moon/Glass/Tube_A03.jpg" alt="Tube_A03" loading="lazy"><figcaption>Tube_A03</figcaption></figure>
<figure><img src="Moon/Glass/Tube_A04.jpg" alt="Tube_A04" loading="lazy"><figcaption>Tube_A04</figcaption></figure>
<figure><img src="Moon/Glass/Tube_A05.jpg" alt="Tube_A05" loading="lazy"><figcaption>Tube_A05</figcaption></figure>
<figure><img src="Moon/Glass/Tube_A06.jpg" alt="Tube_A06" loading="lazy"><figcaption>Tube_A06</figcaption></figure>

### Metal  
<figure><img src="Moon/Metal/roof1.jpg" alt="roof1" loading="lazy"><figcaption>roof1</figcaption></figure>
<figure><img src="Moon/Metal/tile1.jpg" alt="tile1" loading="lazy"><figcaption>tile1</figcaption></figure>
<figure><img src="Moon/Metal/wall1.jpg" alt="wall1" loading="lazy"><figcaption>wall1</figcaption></figure>
<figure><img src="Moon/Metal/wall2.jpg" alt="wall2" loading="lazy"><figcaption>wall2</figcaption></figure>
<figure><img src="Moon/Metal/wall2b.jpg" alt="wall2b" loading="lazy"><figcaption>wall2b</figcaption></figure>
<figure><img src="Moon/Metal/wall3.jpg" alt="wall3" loading="lazy"><figcaption>wall3</figcaption></figure>

### Textile  
<figure><img src="Moon/Textile/floorcorn1.jpg" alt="floorcorn1" loading="lazy"><figcaption>floorcorn1</figcaption></figure>## NYCBar

### Brick  
<figure><img src="NYCBar/Brick/Bricks_b.jpg" alt="Bricks_b" loading="lazy"><figcaption>Bricks_b</figcaption></figure>
<figure><img src="NYCBar/Brick/RuffBrwnBrick_b.jpg" alt="RuffBrwnBrick_b" loading="lazy"><figcaption>RuffBrwnBrick_b</figcaption></figure>

### Concrete  
<figure><img src="NYCBar/Concrete/Uob_Concrete.jpg" alt="Uob_Concrete" loading="lazy"><figcaption>Uob_Concrete</figcaption></figure>

### Glass  
<figure><img src="NYCBar/Glass/FrosWhitGlass_A.jpg" alt="FrosWhitGlass_A" loading="lazy"><figcaption>FrosWhitGlass_A</figcaption></figure>
<figure><img src="NYCBar/Glass/WindOpacStrek_A.jpg" alt="WindOpacStrek_A" loading="lazy"><figcaption>WindOpacStrek_A</figcaption></figure>

### Metal  
<figure><img src="NYCBar/Metal/NYC_GalvMetl_A.jpg" alt="NYC_GalvMetl_A" loading="lazy"><figcaption>NYC_GalvMetl_A</figcaption></figure>
<figure><img src="NYCBar/Metal/NYC_GrayMetal_A.jpg" alt="NYC_GrayMetal_A" loading="lazy"><figcaption>NYC_GrayMetal_A</figcaption></figure>
<figure><img src="NYCBar/Metal/stall1b.jpg" alt="stall1b" loading="lazy"><figcaption>stall1b</figcaption></figure>
<figure><img src="NYCBar/Metal/trough1.jpg" alt="trough1" loading="lazy"><figcaption>trough1</figcaption></figure>

### Misc  
<figure><img src="NYCBar/Misc/BarSign_A.jpg" alt="BarSign_A" loading="lazy"><figcaption>BarSign_A</figcaption></figure>
<figure><img src="NYCBar/Misc/BarSign_Bb.jpg" alt="BarSign_Bb" loading="lazy"><figcaption>BarSign_Bb</figcaption></figure>
<figure><img src="NYCBar/Misc/PoolTabLite_A.jpg" alt="PoolTabLite_A" loading="lazy"><figcaption>PoolTabLite_A</figcaption></figure>
<figure><img src="NYCBar/Misc/PoolTabLite_B.jpg" alt="PoolTabLite_B" loading="lazy"><figcaption>PoolTabLite_B</figcaption></figure>
<figure><img src="NYCBar/Misc/outoforder1.jpg" alt="outoforder1" loading="lazy"><figcaption>outoforder1</figcaption></figure>

### Stone  
<figure><img src="NYCBar/Stone/NYCstonBloc_A.jpg" alt="NYCstonBloc_A" loading="lazy"><figcaption>NYCstonBloc_A</figcaption></figure>

### Textile  
<figure><img src="NYCBar/Textile/WornCouchFab_A.jpg" alt="WornCouchFab_A" loading="lazy"><figcaption>WornCouchFab_A</figcaption></figure>

### Tile  
<figure><img src="NYCBar/Tile/NYC_ceilin_B.jpg" alt="NYC_ceilin_B" loading="lazy"><figcaption>NYC_ceilin_B</figcaption></figure>
<figure><img src="NYCBar/Tile/NYC_ceilin_B1.jpg" alt="NYC_ceilin_B1" loading="lazy"><figcaption>NYC_ceilin_B1</figcaption></figure>
<figure><img src="NYCBar/Tile/NYCentryTile_A.jpg" alt="NYCentryTile_A" loading="lazy"><figcaption>NYCentryTile_A</figcaption></figure>

### Wood  
<figure><img src="NYCBar/Wood/BoatHouseWood_B.jpg" alt="BoatHouseWood_B" loading="lazy"><figcaption>BoatHouseWood_B</figcaption></figure>
<figure><img src="NYCBar/Wood/ClenLiteAshen_A.jpg" alt="ClenLiteAshen_A" loading="lazy"><figcaption>ClenLiteAshen_A</figcaption></figure>
<figure><img src="NYCBar/Wood/ClenMedmWalnt_A.jpg" alt="ClenMedmWalnt_A" loading="lazy"><figcaption>ClenMedmWalnt_A</figcaption></figure>
<figure><img src="NYCBar/Wood/ComnOffcDoor_B.jpg" alt="ComnOffcDoor_B" loading="lazy"><figcaption>ComnOffcDoor_B</figcaption></figure>
<figure><img src="NYCBar/Wood/LockBarWDoor_B.jpg" alt="LockBarWDoor_B" loading="lazy"><figcaption>LockBarWDoor_B</figcaption></figure>
<figure><img src="NYCBar/Wood/NYC_Bar_01.jpg" alt="NYC_Bar_01" loading="lazy"><figcaption>NYC_Bar_01</figcaption></figure>
<figure><img src="NYCBar/Wood/PoolTable_A.jpg" alt="PoolTable_A" loading="lazy"><figcaption>PoolTable_A</figcaption></figure>
<figure><img src="NYCBar/Wood/un_woodwall_a.jpg" alt="un_woodwall_a" loading="lazy"><figcaption>un_woodwall_a</figcaption></figure>
<figure><img src="NYCBar/Wood/un_woodwall_b.jpg" alt="un_woodwall_b" loading="lazy"><figcaption>un_woodwall_b</figcaption></figure>## NewYorkCity

### Brick  
<figure><img src="NewYorkCity/Brick/2ndAveSub_1.jpg" alt="2ndAveSub_1" loading="lazy"><figcaption>2ndAveSub_1</figcaption></figure>
<figure><img src="NewYorkCity/Brick/2ndAveSub_1b.jpg" alt="2ndAveSub_1b" loading="lazy"><figcaption>2ndAveSub_1b</figcaption></figure>
<figure><img src="NewYorkCity/Brick/2ndAveSub_brk.jpg" alt="2ndAveSub_brk" loading="lazy"><figcaption>2ndAveSub_brk</figcaption></figure>
<figure><img src="NewYorkCity/Brick/2ndAveSub_pil.jpg" alt="2ndAveSub_pil" loading="lazy"><figcaption>2ndAveSub_pil</figcaption></figure>
<figure><img src="NewYorkCity/Brick/BP_CCBrick_01.jpg" alt="BP_CCBrick_01" loading="lazy"><figcaption>BP_CCBrick_01</figcaption></figure>
<figure><img src="NewYorkCity/Brick/DrtyBrwnBlock_A.jpg" alt="DrtyBrwnBlock_A" loading="lazy"><figcaption>DrtyBrwnBlock_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_DoorTrim_H.jpg" alt="NSF_DoorTrim_H" loading="lazy"><figcaption>NSF_DoorTrim_H</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_DoorTrim_V.jpg" alt="NSF_DoorTrim_V" loading="lazy"><figcaption>NSF_DoorTrim_V</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_WallTrim_B.jpg" alt="NSF_WallTrim_B" loading="lazy"><figcaption>NSF_WallTrim_B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_WallTrim_C.jpg" alt="NSF_WallTrim_C" loading="lazy"><figcaption>NSF_WallTrim_C</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_Wall_A.jpg" alt="NSF_Wall_A" loading="lazy"><figcaption>NSF_Wall_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_Wall_B.jpg" alt="NSF_Wall_B" loading="lazy"><figcaption>NSF_Wall_B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_Wall_C.jpg" alt="NSF_Wall_C" loading="lazy"><figcaption>NSF_Wall_C</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NSF_Wall_D.jpg" alt="NSF_Wall_D" loading="lazy"><figcaption>NSF_Wall_D</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_01.jpg" alt="NYC_Brick_01" loading="lazy"><figcaption>NYC_Brick_01</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_01A.jpg" alt="NYC_Brick_01A" loading="lazy"><figcaption>NYC_Brick_01A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_01B.jpg" alt="NYC_Brick_01B" loading="lazy"><figcaption>NYC_Brick_01B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_01C.jpg" alt="NYC_Brick_01C" loading="lazy"><figcaption>NYC_Brick_01C</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_01D.jpg" alt="NYC_Brick_01D" loading="lazy"><figcaption>NYC_Brick_01D</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_02.jpg" alt="NYC_Brick_02" loading="lazy"><figcaption>NYC_Brick_02</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_02A.jpg" alt="NYC_Brick_02A" loading="lazy"><figcaption>NYC_Brick_02A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_02B.jpg" alt="NYC_Brick_02B" loading="lazy"><figcaption>NYC_Brick_02B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_02D.jpg" alt="NYC_Brick_02D" loading="lazy"><figcaption>NYC_Brick_02D</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_02F.jpg" alt="NYC_Brick_02F" loading="lazy"><figcaption>NYC_Brick_02F</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Brick_02G.jpg" alt="NYC_Brick_02G" loading="lazy"><figcaption>NYC_Brick_02G</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_RedBrick_A.jpg" alt="NYC_RedBrick_A" loading="lazy"><figcaption>NYC_RedBrick_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Sewer_A.jpg" alt="NYC_Sewer_A" loading="lazy"><figcaption>NYC_Sewer_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Sewer_A1.jpg" alt="NYC_Sewer_A1" loading="lazy"><figcaption>NYC_Sewer_A1</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Sewer_B.jpg" alt="NYC_Sewer_B" loading="lazy"><figcaption>NYC_Sewer_B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Sewer_C.jpg" alt="NYC_Sewer_C" loading="lazy"><figcaption>NYC_Sewer_C</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Sewer_D.jpg" alt="NYC_Sewer_D" loading="lazy"><figcaption>NYC_Sewer_D</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Sewer_D1.jpg" alt="NYC_Sewer_D1" loading="lazy"><figcaption>NYC_Sewer_D1</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_02.jpg" alt="NYC_Wall_02" loading="lazy"><figcaption>NYC_Wall_02</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_02A.jpg" alt="NYC_Wall_02A" loading="lazy"><figcaption>NYC_Wall_02A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_03.jpg" alt="NYC_Wall_03" loading="lazy"><figcaption>NYC_Wall_03</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_05.jpg" alt="NYC_Wall_05" loading="lazy"><figcaption>NYC_Wall_05</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_07.jpg" alt="NYC_Wall_07" loading="lazy"><figcaption>NYC_Wall_07</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_07A.jpg" alt="NYC_Wall_07A" loading="lazy"><figcaption>NYC_Wall_07A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_Wall_08.jpg" alt="NYC_Wall_08" loading="lazy"><figcaption>NYC_Wall_08</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_brick_A.jpg" alt="NYC_brick_A" loading="lazy"><figcaption>NYC_brick_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_brick_BS.jpg" alt="NYC_brick_BS" loading="lazy"><figcaption>NYC_brick_BS</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYC_brick_C.jpg" alt="NYC_brick_C" loading="lazy"><figcaption>NYC_brick_C</figcaption></figure>
<figure><img src="NewYorkCity/Brick/NYCstonBloc_A.jpg" alt="NYCstonBloc_A" loading="lazy"><figcaption>NYCstonBloc_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/TopBrickTrim_A.jpg" alt="TopBrickTrim_A" loading="lazy"><figcaption>TopBrickTrim_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/TopBrickTrim_AS.jpg" alt="TopBrickTrim_AS" loading="lazy"><figcaption>TopBrickTrim_AS</figcaption></figure>
<figure><img src="NewYorkCity/Brick/TowerWindow_A.jpg" alt="TowerWindow_A" loading="lazy"><figcaption>TowerWindow_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/WareHousBloc1A.jpg" alt="WareHousBloc1A" loading="lazy"><figcaption>WareHousBloc1A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/WareHousBlock1.jpg" alt="WareHousBlock1" loading="lazy"><figcaption>WareHousBlock1</figcaption></figure>
<figure><img src="NewYorkCity/Brick/WareHousBlock2.jpg" alt="WareHousBlock2" loading="lazy"><figcaption>WareHousBlock2</figcaption></figure>
<figure><img src="NewYorkCity/Brick/WareHouseBloc1A.jpg" alt="WareHouseBloc1A" loading="lazy"><figcaption>WareHouseBloc1A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/WareHouseBlock1.jpg" alt="WareHouseBlock1" loading="lazy"><figcaption>WareHouseBlock1</figcaption></figure>
<figure><img src="NewYorkCity/Brick/WareHouseBlock2.jpg" alt="WareHouseBlock2" loading="lazy"><figcaption>WareHouseBlock2</figcaption></figure>
<figure><img src="NewYorkCity/Brick/Window_A.jpg" alt="Window_A" loading="lazy"><figcaption>Window_A</figcaption></figure>
<figure><img src="NewYorkCity/Brick/Window_B.jpg" alt="Window_B" loading="lazy"><figcaption>Window_B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/Window_C.jpg" alt="Window_C" loading="lazy"><figcaption>Window_C</figcaption></figure>
<figure><img src="NewYorkCity/Brick/Wintest1B.jpg" alt="Wintest1B" loading="lazy"><figcaption>Wintest1B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/Wintest2B.jpg" alt="Wintest2B" loading="lazy"><figcaption>Wintest2B</figcaption></figure>
<figure><img src="NewYorkCity/Brick/Wintest2D.jpg" alt="Wintest2D" loading="lazy"><figcaption>Wintest2D</figcaption></figure>
<figure><img src="NewYorkCity/Brick/tanbrickwall_AS.jpg" alt="tanbrickwall_AS" loading="lazy"><figcaption>tanbrickwall_AS</figcaption></figure>
<figure><img src="NewYorkCity/Brick/tanbrk_burnt.jpg" alt="tanbrk_burnt" loading="lazy"><figcaption>tanbrk_burnt</figcaption></figure>

### Concrete  
<figure><img src="NewYorkCity/Concrete/2ndAvSub_bod.jpg" alt="2ndAvSub_bod" loading="lazy"><figcaption>2ndAvSub_bod</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/2ndAveSub_flr.jpg" alt="2ndAveSub_flr" loading="lazy"><figcaption>2ndAveSub_flr</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/2ndAveSub_flrS.jpg" alt="2ndAveSub_flrS" loading="lazy"><figcaption>2ndAveSub_flrS</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/NSF_Conc_Amark.jpg" alt="NSF_Conc_Amark" loading="lazy"><figcaption>NSF_Conc_Amark</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/NSF_Concrete_A.jpg" alt="NSF_Concrete_A" loading="lazy"><figcaption>NSF_Concrete_A</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/NSF_Concrete_B.jpg" alt="NSF_Concrete_B" loading="lazy"><figcaption>NSF_Concrete_B</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/NYC_Cement_A.jpg" alt="NYC_Cement_A" loading="lazy"><figcaption>NYC_Cement_A</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/NYC_Cement_B.jpg" alt="NYC_Cement_B" loading="lazy"><figcaption>NYC_Cement_B</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/NYC_Street_B.jpg" alt="NYC_Street_B" loading="lazy"><figcaption>NYC_Street_B</figcaption></figure>
<figure><img src="NewYorkCity/Concrete/warewallB_128.jpg" alt="warewallB_128" loading="lazy"><figcaption>warewallB_128</figcaption></figure>

### Metal  
<figure><img src="NewYorkCity/Metal/2ndAvSubcar1.jpg" alt="2ndAvSubcar1" loading="lazy"><figcaption>2ndAvSubcar1</figcaption></figure>
<figure><img src="NewYorkCity/Metal/2ndAvSubcar1b.jpg" alt="2ndAvSubcar1b" loading="lazy"><figcaption>2ndAvSubcar1b</figcaption></figure>
<figure><img src="NewYorkCity/Metal/2ndAvePh.jpg" alt="2ndAvePh" loading="lazy"><figcaption>2ndAvePh</figcaption></figure>
<figure><img src="NewYorkCity/Metal/2ndAveSub_ceil.jpg" alt="2ndAveSub_ceil" loading="lazy"><figcaption>2ndAveSub_ceil</figcaption></figure>
<figure><img src="NewYorkCity/Metal/CorgMtl_Rst.jpg" alt="CorgMtl_Rst" loading="lazy"><figcaption>CorgMtl_Rst</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_BigDoor_A.jpg" alt="NSF_BigDoor_A" loading="lazy"><figcaption>NSF_BigDoor_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_EdgeTrim_A.jpg" alt="NSF_EdgeTrim_A" loading="lazy"><figcaption>NSF_EdgeTrim_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_EdgeTrim_B.jpg" alt="NSF_EdgeTrim_B" loading="lazy"><figcaption>NSF_EdgeTrim_B</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_Metal_A.jpg" alt="NSF_Metal_A" loading="lazy"><figcaption>NSF_Metal_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_Ramp_A.jpg" alt="NSF_Ramp_A" loading="lazy"><figcaption>NSF_Ramp_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_TechCeiln.jpg" alt="NSF_TechCeiln" loading="lazy"><figcaption>NSF_TechCeiln</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NSF_TechFloor.jpg" alt="NSF_TechFloor" loading="lazy"><figcaption>NSF_TechFloor</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYCP_b.jpg" alt="NYCP_b" loading="lazy"><figcaption>NYCP_b</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_GalvMetl_A.jpg" alt="NYC_GalvMetl_A" loading="lazy"><figcaption>NYC_GalvMetl_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_GrayMetal_A.jpg" alt="NYC_GrayMetal_A" loading="lazy"><figcaption>NYC_GrayMetal_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_IronDoor.jpg" alt="NYC_IronDoor" loading="lazy"><figcaption>NYC_IronDoor</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_Ypipe.jpg" alt="NYC_Ypipe" loading="lazy"><figcaption>NYC_Ypipe</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_pipe_A.jpg" alt="NYC_pipe_A" loading="lazy"><figcaption>NYC_pipe_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_pipe_B.jpg" alt="NYC_pipe_B" loading="lazy"><figcaption>NYC_pipe_B</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_pipe_B2.jpg" alt="NYC_pipe_B2" loading="lazy"><figcaption>NYC_pipe_B2</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYC_pipe_C.jpg" alt="NYC_pipe_C" loading="lazy"><figcaption>NYC_pipe_C</figcaption></figure>
<figure><img src="NewYorkCity/Metal/NYmanholecov_B.jpg" alt="NYmanholecov_B" loading="lazy"><figcaption>NYmanholecov_B</figcaption></figure>
<figure><img src="NewYorkCity/Metal/ShowER.jpg" alt="ShowER" loading="lazy"><figcaption>ShowER</figcaption></figure>
<figure><img src="NewYorkCity/Metal/SmuglerDoor.jpg" alt="SmuglerDoor" loading="lazy"><figcaption>SmuglerDoor</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WalkwayDetl_A.jpg" alt="WalkwayDetl_A" loading="lazy"><figcaption>WalkwayDetl_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WalkwayDetl_B.jpg" alt="WalkwayDetl_B" loading="lazy"><figcaption>WalkwayDetl_B</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WareHousIron_A.jpg" alt="WareHousIron_A" loading="lazy"><figcaption>WareHousIron_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WareHousIron_B.jpg" alt="WareHousIron_B" loading="lazy"><figcaption>WareHousIron_B</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WareHousIron_C.jpg" alt="WareHousIron_C" loading="lazy"><figcaption>WareHousIron_C</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WareHousIron_D.jpg" alt="WareHousIron_D" loading="lazy"><figcaption>WareHousIron_D</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WareHousIron_E.jpg" alt="WareHousIron_E" loading="lazy"><figcaption>WareHousIron_E</figcaption></figure>
<figure><img src="NewYorkCity/Metal/WireBox_A.jpg" alt="WireBox_A" loading="lazy"><figcaption>WireBox_A</figcaption></figure>
<figure><img src="NewYorkCity/Metal/blinds1.jpg" alt="blinds1" loading="lazy"><figcaption>blinds1</figcaption></figure>
<figure><img src="NewYorkCity/Metal/duct_A2.jpg" alt="duct_A2" loading="lazy"><figcaption>duct_A2</figcaption></figure>
<figure><img src="NewYorkCity/Metal/nyc_ash_01.jpg" alt="nyc_ash_01" loading="lazy"><figcaption>nyc_ash_01</figcaption></figure>
<figure><img src="NewYorkCity/Metal/pipe_d.jpg" alt="pipe_d" loading="lazy"><figcaption>pipe_d</figcaption></figure>
<figure><img src="NewYorkCity/Metal/smugglerEntry.jpg" alt="smugglerEntry" loading="lazy"><figcaption>smugglerEntry</figcaption></figure>
<figure><img src="NewYorkCity/Metal/stall1.jpg" alt="stall1" loading="lazy"><figcaption>stall1</figcaption></figure>
<figure><img src="NewYorkCity/Metal/stall1b.jpg" alt="stall1b" loading="lazy"><figcaption>stall1b</figcaption></figure>
<figure><img src="NewYorkCity/Metal/trough1.jpg" alt="trough1" loading="lazy"><figcaption>trough1</figcaption></figure>
<figure><img src="NewYorkCity/Metal/un_MetalDoor_A.jpg" alt="un_MetalDoor_A" loading="lazy"><figcaption>un_MetalDoor_A</figcaption></figure>

### Signs  
<figure><img src="NewYorkCity/Signs/2ndAveSub_sign.jpg" alt="2ndAveSub_sign" loading="lazy"><figcaption>2ndAveSub_sign</figcaption></figure>
<figure><img src="NewYorkCity/Signs/BP_Neon1.jpg" alt="BP_Neon1" loading="lazy"><figcaption>BP_Neon1</figcaption></figure>
<figure><img src="NewYorkCity/Signs/BP_NeonBk2.jpg" alt="BP_NeonBk2" loading="lazy"><figcaption>BP_NeonBk2</figcaption></figure>
<figure><img src="NewYorkCity/Signs/BP_OSsign1.jpg" alt="BP_OSsign1" loading="lazy"><figcaption>BP_OSsign1</figcaption></figure>
<figure><img src="NewYorkCity/Signs/BP_OSsign2.jpg" alt="BP_OSsign2" loading="lazy"><figcaption>BP_OSsign2</figcaption></figure>
<figure><img src="NewYorkCity/Signs/HolySmokesBB.jpg" alt="HolySmokesBB" loading="lazy"><figcaption>HolySmokesBB</figcaption></figure>
<figure><img src="NewYorkCity/Signs/JCpaint2.jpg" alt="JCpaint2" loading="lazy"><figcaption>JCpaint2</figcaption></figure>
<figure><img src="NewYorkCity/Signs/JadeDragonBB.jpg" alt="JadeDragonBB" loading="lazy"><figcaption>JadeDragonBB</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NSFHQ_Sign_01.jpg" alt="NSFHQ_Sign_01" loading="lazy"><figcaption>NSFHQ_Sign_01</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NSF_Basemt_Sign.jpg" alt="NSF_Basemt_Sign" loading="lazy"><figcaption>NSF_Basemt_Sign</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NSF_Level1_Sign.jpg" alt="NSF_Level1_Sign" loading="lazy"><figcaption>NSF_Level1_Sign</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NSF_Level2_Sign.jpg" alt="NSF_Level2_Sign" loading="lazy"><figcaption>NSF_Level2_Sign</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NSF_Level3_Sign.jpg" alt="NSF_Level3_Sign" loading="lazy"><figcaption>NSF_Level3_Sign</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NSF_Level4_Sign.jpg" alt="NSF_Level4_Sign" loading="lazy"><figcaption>NSF_Level4_Sign</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NYC_plcetape.jpg" alt="NYC_plcetape" loading="lazy"><figcaption>NYC_plcetape</figcaption></figure>
<figure><img src="NewYorkCity/Signs/NanoBookTops.jpg" alt="NanoBookTops" loading="lazy"><figcaption>NanoBookTops</figcaption></figure>
<figure><img src="NewYorkCity/Signs/S45cigsBB.jpg" alt="S45cigsBB" loading="lazy"><figcaption>S45cigsBB</figcaption></figure>
<figure><img src="NewYorkCity/Signs/Super45BB.jpg" alt="Super45BB" loading="lazy"><figcaption>Super45BB</figcaption></figure>
<figure><img src="NewYorkCity/Signs/Super45B_BB.jpg" alt="Super45B_BB" loading="lazy"><figcaption>Super45B_BB</figcaption></figure>
<figure><img src="NewYorkCity/Signs/UWtavsignbak.jpg" alt="UWtavsignbak" loading="lazy"><figcaption>UWtavsignbak</figcaption></figure>
<figure><img src="NewYorkCity/Signs/UWtavsignne_00.jpg" alt="UWtavsignne_00" loading="lazy"><figcaption>UWtavsignne_00</figcaption></figure>
<figure><img src="NewYorkCity/Signs/UWtavsignne_01.jpg" alt="UWtavsignne_01" loading="lazy"><figcaption>UWtavsignne_01</figcaption></figure>
<figure><img src="NewYorkCity/Signs/bigtops.jpg" alt="bigtops" loading="lazy"><figcaption>bigtops</figcaption></figure>
<figure><img src="NewYorkCity/Signs/nyc_fcsign_01.jpg" alt="nyc_fcsign_01" loading="lazy"><figcaption>nyc_fcsign_01</figcaption></figure>
<figure><img src="NewYorkCity/Signs/outoforder1.jpg" alt="outoforder1" loading="lazy"><figcaption>outoforder1</figcaption></figure>
<figure><img src="NewYorkCity/Signs/sandra_picB.jpg" alt="sandra_picB" loading="lazy"><figcaption>sandra_picB</figcaption></figure>
<figure><img src="NewYorkCity/Signs/ssign18thSt.jpg" alt="ssign18thSt" loading="lazy"><figcaption>ssign18thSt</figcaption></figure>
<figure><img src="NewYorkCity/Signs/ssign2ndave.jpg" alt="ssign2ndave" loading="lazy"><figcaption>ssign2ndave</figcaption></figure>
<figure><img src="NewYorkCity/Signs/submap.jpg" alt="submap" loading="lazy"><figcaption>submap</figcaption></figure>

### Stone  
<figure><img src="NewYorkCity/Stone/nyc_grave_01.jpg" alt="nyc_grave_01" loading="lazy"><figcaption>nyc_grave_01</figcaption></figure>
<figure><img src="NewYorkCity/Stone/nyc_grave_02.jpg" alt="nyc_grave_02" loading="lazy"><figcaption>nyc_grave_02</figcaption></figure>
<figure><img src="NewYorkCity/Stone/nyc_grave_03.jpg" alt="nyc_grave_03" loading="lazy"><figcaption>nyc_grave_03</figcaption></figure>
<figure><img src="NewYorkCity/Stone/nyc_grave_04.jpg" alt="nyc_grave_04" loading="lazy"><figcaption>nyc_grave_04</figcaption></figure>
<figure><img src="NewYorkCity/Stone/nyc_grave_05.jpg" alt="nyc_grave_05" loading="lazy"><figcaption>nyc_grave_05</figcaption></figure>

### Stucco  
<figure><img src="NewYorkCity/Stucco/crackcieling.jpg" alt="crackcieling" loading="lazy"><figcaption>crackcieling</figcaption></figure>

### Textile  
<figure><img src="NewYorkCity/Textile/BarStoolSide_A.jpg" alt="BarStoolSide_A" loading="lazy"><figcaption>BarStoolSide_A</figcaption></figure>
<figure><img src="NewYorkCity/Textile/BarStoolTop_A.jpg" alt="BarStoolTop_A" loading="lazy"><figcaption>BarStoolTop_A</figcaption></figure>
<figure><img src="NewYorkCity/Textile/HotelCarpet_2.jpg" alt="HotelCarpet_2" loading="lazy"><figcaption>HotelCarpet_2</figcaption></figure>
<figure><img src="NewYorkCity/Textile/P_ClothLine_A.jpg" alt="P_ClothLine_A" loading="lazy"><figcaption>P_ClothLine_A</figcaption></figure>
<figure><img src="NewYorkCity/Textile/P_ClothLine_B.jpg" alt="P_ClothLine_B" loading="lazy"><figcaption>P_ClothLine_B</figcaption></figure>
<figure><img src="NewYorkCity/Textile/SandBag.jpg" alt="SandBag" loading="lazy"><figcaption>SandBag</figcaption></figure>

### Tiles  
<figure><img src="NewYorkCity/Tiles/BathFloor1.jpg" alt="BathFloor1" loading="lazy"><figcaption>BathFloor1</figcaption></figure>
<figure><img src="NewYorkCity/Tiles/BathWall1.jpg" alt="BathWall1" loading="lazy"><figcaption>BathWall1</figcaption></figure>
<figure><img src="NewYorkCity/Tiles/NEW_hAtriFlor.jpg" alt="NEW_hAtriFlor" loading="lazy"><figcaption>NEW_hAtriFlor</figcaption></figure>
<figure><img src="NewYorkCity/Tiles/NYC_ceilin_B.jpg" alt="NYC_ceilin_B" loading="lazy"><figcaption>NYC_ceilin_B</figcaption></figure>
<figure><img src="NewYorkCity/Tiles/NYCtileFloor_B.jpg" alt="NYCtileFloor_B" loading="lazy"><figcaption>NYCtileFloor_B</figcaption></figure>

### Wall_Objects  
<figure><img src="NewYorkCity/Wall_Objects/BarWall_A.jpg" alt="BarWall_A" loading="lazy"><figcaption>BarWall_A</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/BarWall_B.jpg" alt="BarWall_B" loading="lazy"><figcaption>BarWall_B</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/BarWall_C.jpg" alt="BarWall_C" loading="lazy"><figcaption>BarWall_C</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/BarWall_D.jpg" alt="BarWall_D" loading="lazy"><figcaption>BarWall_D</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/HotelRoom_C1.jpg" alt="HotelRoom_C1" loading="lazy"><figcaption>HotelRoom_C1</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/HotelRoom_C1S.jpg" alt="HotelRoom_C1S" loading="lazy"><figcaption>HotelRoom_C1S</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/HotelRoom_C2.jpg" alt="HotelRoom_C2" loading="lazy"><figcaption>HotelRoom_C2</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/NYC_interWallC1.jpg" alt="NYC_interWallC1" loading="lazy"><figcaption>NYC_interWallC1</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/NYC_interWallC2.jpg" alt="NYC_interWallC2" loading="lazy"><figcaption>NYC_interWallC2</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/NYC_interWallC3.jpg" alt="NYC_interWallC3" loading="lazy"><figcaption>NYC_interWallC3</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/NYC_interWallC4.jpg" alt="NYC_interWallC4" loading="lazy"><figcaption>NYC_interWallC4</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/crakwall1.jpg" alt="crakwall1" loading="lazy"><figcaption>crakwall1</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/crakwall1b.jpg" alt="crakwall1b" loading="lazy"><figcaption>crakwall1b</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/crakwall1c.jpg" alt="crakwall1c" loading="lazy"><figcaption>crakwall1c</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/crakwall1d.jpg" alt="crakwall1d" loading="lazy"><figcaption>crakwall1d</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/crakwall1dS.jpg" alt="crakwall1dS" loading="lazy"><figcaption>crakwall1dS</figcaption></figure>
<figure><img src="NewYorkCity/Wall_Objects/crakwall1e.jpg" alt="crakwall1e" loading="lazy"><figcaption>crakwall1e</figcaption></figure>

### Wood  
<figure><img src="NewYorkCity/Wood/Bar_OldDoor1.jpg" alt="Bar_OldDoor1" loading="lazy"><figcaption>Bar_OldDoor1</figcaption></figure>
<figure><img src="NewYorkCity/Wood/LockBarWDoor_B.jpg" alt="LockBarWDoor_B" loading="lazy"><figcaption>LockBarWDoor_B</figcaption></figure>
<figure><img src="NewYorkCity/Wood/NSF_WoodRail_A.jpg" alt="NSF_WoodRail_A" loading="lazy"><figcaption>NSF_WoodRail_A</figcaption></figure>
<figure><img src="NewYorkCity/Wood/NYColdwood_B.jpg" alt="NYColdwood_B" loading="lazy"><figcaption>NYColdwood_B</figcaption></figure>
<figure><img src="NewYorkCity/Wood/PoolTable_A.jpg" alt="PoolTable_A" loading="lazy"><figcaption>PoolTable_A</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_TargetBKGRD.jpg" alt="Uob_TargetBKGRD" loading="lazy"><figcaption>Uob_TargetBKGRD</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_A.jpg" alt="Uob_Target_A" loading="lazy"><figcaption>Uob_Target_A</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_B.jpg" alt="Uob_Target_B" loading="lazy"><figcaption>Uob_Target_B</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_C.jpg" alt="Uob_Target_C" loading="lazy"><figcaption>Uob_Target_C</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_D.jpg" alt="Uob_Target_D" loading="lazy"><figcaption>Uob_Target_D</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_E.jpg" alt="Uob_Target_E" loading="lazy"><figcaption>Uob_Target_E</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_F.jpg" alt="Uob_Target_F" loading="lazy"><figcaption>Uob_Target_F</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_G.jpg" alt="Uob_Target_G" loading="lazy"><figcaption>Uob_Target_G</figcaption></figure>
<figure><img src="NewYorkCity/Wood/Uob_Target_H.jpg" alt="Uob_Target_H" loading="lazy"><figcaption>Uob_Target_H</figcaption></figure>
<figure><img src="NewYorkCity/Wood/pa_OldDoorB.jpg" alt="pa_OldDoorB" loading="lazy"><figcaption>pa_OldDoorB</figcaption></figure>
<figure><img src="NewYorkCity/Wood/pa_OldDoorC.jpg" alt="pa_OldDoorC" loading="lazy"><figcaption>pa_OldDoorC</figcaption></figure>
<figure><img src="NewYorkCity/Wood/pa_OldDoorE.jpg" alt="pa_OldDoorE" loading="lazy"><figcaption>pa_OldDoorE</figcaption></figure>
<figure><img src="NewYorkCity/Wood/ton_frntdoor.jpg" alt="ton_frntdoor" loading="lazy"><figcaption>ton_frntdoor</figcaption></figure>

### background  
<figure><img src="NewYorkCity/background/NYCCmntWall__A.jpg" alt="NYCCmntWall__A" loading="lazy"><figcaption>NYCCmntWall__A</figcaption></figure>
<figure><img src="NewYorkCity/background/NYCStonWndow_A.jpg" alt="NYCStonWndow_A" loading="lazy"><figcaption>NYCStonWndow_A</figcaption></figure>
<figure><img src="NewYorkCity/background/NYCStonWndow_B.jpg" alt="NYCStonWndow_B" loading="lazy"><figcaption>NYCStonWndow_B</figcaption></figure>
<figure><img src="NewYorkCity/background/NYCVent_Wall_A.jpg" alt="NYCVent_Wall_A" loading="lazy"><figcaption>NYCVent_Wall_A</figcaption></figure>
<figure><img src="NewYorkCity/background/NYCWall_Vent_A.jpg" alt="NYCWall_Vent_A" loading="lazy"><figcaption>NYCWall_Vent_A</figcaption></figure>
<figure><img src="NewYorkCity/background/NYCWhteWall_A.jpg" alt="NYCWhteWall_A" loading="lazy"><figcaption>NYCWhteWall_A</figcaption></figure>
<figure><img src="NewYorkCity/background/NYCWhteWall_D.jpg" alt="NYCWhteWall_D" loading="lazy"><figcaption>NYCWhteWall_D</figcaption></figure>## OceanLab

### Metal  
<figure><img src="OceanLab/Metal/OL_Tredtrac.jpg" alt="OL_Tredtrac" loading="lazy"><figcaption>OL_Tredtrac</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tredwheel.jpg" alt="OL_Tredwheel" loading="lazy"><figcaption>OL_Tredwheel</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tredwheel1.jpg" alt="OL_Tredwheel1" loading="lazy"><figcaption>OL_Tredwheel1</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_TunlrDetl_A.jpg" alt="OL_TunlrDetl_A" loading="lazy"><figcaption>OL_TunlrDetl_A</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_TunlrDetl_B.jpg" alt="OL_TunlrDetl_B" loading="lazy"><figcaption>OL_TunlrDetl_B</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_TunlrDetl_C.jpg" alt="OL_TunlrDetl_C" loading="lazy"><figcaption>OL_TunlrDetl_C</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrback.jpg" alt="OL_Tunlrback" loading="lazy"><figcaption>OL_Tunlrback</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrbasa.jpg" alt="OL_Tunlrbasa" loading="lazy"><figcaption>OL_Tunlrbasa</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrbasb.jpg" alt="OL_Tunlrbasb" loading="lazy"><figcaption>OL_Tunlrbasb</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrbasc.jpg" alt="OL_Tunlrbasc" loading="lazy"><figcaption>OL_Tunlrbasc</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrbasd.jpg" alt="OL_Tunlrbasd" loading="lazy"><figcaption>OL_Tunlrbasd</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrbase.jpg" alt="OL_Tunlrbase" loading="lazy"><figcaption>OL_Tunlrbase</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrbody.jpg" alt="OL_Tunlrbody" loading="lazy"><figcaption>OL_Tunlrbody</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrdoor.jpg" alt="OL_Tunlrdoor" loading="lazy"><figcaption>OL_Tunlrdoor</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrside_A.jpg" alt="OL_Tunlrside_A" loading="lazy"><figcaption>OL_Tunlrside_A</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_Tunlrside_B.jpg" alt="OL_Tunlrside_B" loading="lazy"><figcaption>OL_Tunlrside_B</figcaption></figure>
<figure><img src="OceanLab/Metal/OL_TunnelWall.jpg" alt="OL_TunnelWall" loading="lazy"><figcaption>OL_TunnelWall</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_door_01.jpg" alt="ol_door_01" loading="lazy"><figcaption>ol_door_01</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_door_02.jpg" alt="ol_door_02" loading="lazy"><figcaption>ol_door_02</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_machine_01.jpg" alt="ol_machine_01" loading="lazy"><figcaption>ol_machine_01</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_machine_02.jpg" alt="ol_machine_02" loading="lazy"><figcaption>ol_machine_02</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_03.jpg" alt="ol_wall_03" loading="lazy"><figcaption>ol_wall_03</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_04.jpg" alt="ol_wall_04" loading="lazy"><figcaption>ol_wall_04</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_05.jpg" alt="ol_wall_05" loading="lazy"><figcaption>ol_wall_05</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_06.jpg" alt="ol_wall_06" loading="lazy"><figcaption>ol_wall_06</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_10.jpg" alt="ol_wall_10" loading="lazy"><figcaption>ol_wall_10</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_11.jpg" alt="ol_wall_11" loading="lazy"><figcaption>ol_wall_11</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_12.jpg" alt="ol_wall_12" loading="lazy"><figcaption>ol_wall_12</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_13.jpg" alt="ol_wall_13" loading="lazy"><figcaption>ol_wall_13</figcaption></figure>
<figure><img src="OceanLab/Metal/ol_wall_14.jpg" alt="ol_wall_14" loading="lazy"><figcaption>ol_wall_14</figcaption></figure>

### Misc  
<figure><img src="OceanLab/Misc/lt_gradient.jpg" alt="lt_gradient" loading="lazy"><figcaption>lt_gradient</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_light_01.jpg" alt="ol_light_01" loading="lazy"><figcaption>ol_light_01</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_light_02.jpg" alt="ol_light_02" loading="lazy"><figcaption>ol_light_02</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_panel_01.jpg" alt="ol_panel_01" loading="lazy"><figcaption>ol_panel_01</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_panel_02.jpg" alt="ol_panel_02" loading="lazy"><figcaption>ol_panel_02</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_panel_03.jpg" alt="ol_panel_03" loading="lazy"><figcaption>ol_panel_03</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_panel_04.jpg" alt="ol_panel_04" loading="lazy"><figcaption>ol_panel_04</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_sign_01.jpg" alt="ol_sign_01" loading="lazy"><figcaption>ol_sign_01</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_sign_02.jpg" alt="ol_sign_02" loading="lazy"><figcaption>ol_sign_02</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_sign_03.jpg" alt="ol_sign_03" loading="lazy"><figcaption>ol_sign_03</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_sign_04.jpg" alt="ol_sign_04" loading="lazy"><figcaption>ol_sign_04</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_sign_05.jpg" alt="ol_sign_05" loading="lazy"><figcaption>ol_sign_05</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_sign_06.jpg" alt="ol_sign_06" loading="lazy"><figcaption>ol_sign_06</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_warntape_01.jpg" alt="ol_warntape_01" loading="lazy"><figcaption>ol_warntape_01</figcaption></figure>
<figure><img src="OceanLab/Misc/ol_warntape_02.jpg" alt="ol_warntape_02" loading="lazy"><figcaption>ol_warntape_02</figcaption></figure>

### Stone  
<figure><img src="OceanLab/Stone/ol_wall_01.jpg" alt="ol_wall_01" loading="lazy"><figcaption>ol_wall_01</figcaption></figure>
<figure><img src="OceanLab/Stone/ol_wall_02.jpg" alt="ol_wall_02" loading="lazy"><figcaption>ol_wall_02</figcaption></figure>
<figure><img src="OceanLab/Stone/ol_wall_08.jpg" alt="ol_wall_08" loading="lazy"><figcaption>ol_wall_08</figcaption></figure>
<figure><img src="OceanLab/Stone/ol_wall_17.jpg" alt="ol_wall_17" loading="lazy"><figcaption>ol_wall_17</figcaption></figure>## Palettes

### Blue  
<figure><img src="Palettes/Blue/BRI_Blue.jpg" alt="BRI_Blue" loading="lazy"><figcaption>BRI_Blue</figcaption></figure>
<figure><img src="Palettes/Blue/BRI_Cyan.jpg" alt="BRI_Cyan" loading="lazy"><figcaption>BRI_Cyan</figcaption></figure>
<figure><img src="Palettes/Blue/Cwater3.jpg" alt="Cwater3" loading="lazy"><figcaption>Cwater3</figcaption></figure>
<figure><img src="Palettes/Blue/Cwater8.jpg" alt="Cwater8" loading="lazy"><figcaption>Cwater8</figcaption></figure>
<figure><img src="Palettes/Blue/Jblue.jpg" alt="Jblue" loading="lazy"><figcaption>Jblue</figcaption></figure>
<figure><img src="Palettes/Blue/Jblue2.jpg" alt="Jblue2" loading="lazy"><figcaption>Jblue2</figcaption></figure>
<figure><img src="Palettes/Blue/Jblue3.jpg" alt="Jblue3" loading="lazy"><figcaption>Jblue3</figcaption></figure>
<figure><img src="Palettes/Blue/LIN_Blue.jpg" alt="LIN_Blue" loading="lazy"><figcaption>LIN_Blue</figcaption></figure>
<figure><img src="Palettes/Cfire4.jpg" alt="Cfire4" loading="lazy"><figcaption>Cfire4</figcaption></figure>

### Green  
<figure><img src="Palettes/Green/BRI_Green.jpg" alt="BRI_Green" loading="lazy"><figcaption>BRI_Green</figcaption></figure>
<figure><img src="Palettes/Green/Cwater1.jpg" alt="Cwater1" loading="lazy"><figcaption>Cwater1</figcaption></figure>
<figure><img src="Palettes/Green/GreenLevels.jpg" alt="GreenLevels" loading="lazy"><figcaption>GreenLevels</figcaption></figure>
<figure><img src="Palettes/Green/Jgreen1.jpg" alt="Jgreen1" loading="lazy"><figcaption>Jgreen1</figcaption></figure>
<figure><img src="Palettes/Green/LIN_Green.jpg" alt="LIN_Green" loading="lazy"><figcaption>LIN_Green</figcaption></figure>

### Grey  
<figure><img src="Palettes/Grey/Cfire8.jpg" alt="Cfire8" loading="lazy"><figcaption>Cfire8</figcaption></figure>
<figure><img src="Palettes/Grey/Csteam.jpg" alt="Csteam" loading="lazy"><figcaption>Csteam</figcaption></figure>
<figure><img src="Palettes/Grey/Jsmoke1.jpg" alt="Jsmoke1" loading="lazy"><figcaption>Jsmoke1</figcaption></figure>
<figure><img src="Palettes/Grey/Jsmoke2.jpg" alt="Jsmoke2" loading="lazy"><figcaption>Jsmoke2</figcaption></figure>
<figure><img src="Palettes/Jyellow1.jpg" alt="Jyellow1" loading="lazy"><figcaption>Jyellow1</figcaption></figure>
<figure><img src="Palettes/LIN_Cyan.jpg" alt="LIN_Cyan" loading="lazy"><figcaption>LIN_Cyan</figcaption></figure>

### Misc  
<figure><img src="Palettes/Misc/Ashes.jpg" alt="Ashes" loading="lazy"><figcaption>Ashes</figcaption></figure>
<figure><img src="Palettes/Misc/Bloody.jpg" alt="Bloody" loading="lazy"><figcaption>Bloody</figcaption></figure>
<figure><img src="Palettes/Misc/Candy.jpg" alt="Candy" loading="lazy"><figcaption>Candy</figcaption></figure>
<figure><img src="Palettes/Misc/Cfire1.jpg" alt="Cfire1" loading="lazy"><figcaption>Cfire1</figcaption></figure>
<figure><img src="Palettes/Misc/Cfire2.jpg" alt="Cfire2" loading="lazy"><figcaption>Cfire2</figcaption></figure>
<figure><img src="Palettes/Misc/Cfire3.jpg" alt="Cfire3" loading="lazy"><figcaption>Cfire3</figcaption></figure>
<figure><img src="Palettes/Misc/Cfire6.jpg" alt="Cfire6" loading="lazy"><figcaption>Cfire6</figcaption></figure>
<figure><img src="Palettes/Misc/Cliche.jpg" alt="Cliche" loading="lazy"><figcaption>Cliche</figcaption></figure>
<figure><img src="Palettes/Misc/Copr_ox.jpg" alt="Copr_ox" loading="lazy"><figcaption>Copr_ox</figcaption></figure>
<figure><img src="Palettes/Misc/Crayon.jpg" alt="Crayon" loading="lazy"><figcaption>Crayon</figcaption></figure>
<figure><img src="Palettes/Misc/Cslime.jpg" alt="Cslime" loading="lazy"><figcaption>Cslime</figcaption></figure>
<figure><img src="Palettes/Misc/Cwater2.jpg" alt="Cwater2" loading="lazy"><figcaption>Cwater2</figcaption></figure>
<figure><img src="Palettes/Misc/Cwater4.jpg" alt="Cwater4" loading="lazy"><figcaption>Cwater4</figcaption></figure>
<figure><img src="Palettes/Misc/Cwaterx.jpg" alt="Cwaterx" loading="lazy"><figcaption>Cwaterx</figcaption></figure>
<figure><img src="Palettes/Misc/Earthy.jpg" alt="Earthy" loading="lazy"><figcaption>Earthy</figcaption></figure>
<figure><img src="Palettes/Misc/Glogaz.jpg" alt="Glogaz" loading="lazy"><figcaption>Glogaz</figcaption></figure>
<figure><img src="Palettes/Misc/GlowSnow.jpg" alt="GlowSnow" loading="lazy"><figcaption>GlowSnow</figcaption></figure>
<figure><img src="Palettes/Misc/Glowy.jpg" alt="Glowy" loading="lazy"><figcaption>Glowy</figcaption></figure>
<figure><img src="Palettes/Misc/Grad1eff.jpg" alt="Grad1eff" loading="lazy"><figcaption>Grad1eff</figcaption></figure>
<figure><img src="Palettes/Misc/GreenWater.jpg" alt="GreenWater" loading="lazy"><figcaption>GreenWater</figcaption></figure>
<figure><img src="Palettes/Misc/Gum.jpg" alt="Gum" loading="lazy"><figcaption>Gum</figcaption></figure>
<figure><img src="Palettes/Misc/Ice_t.jpg" alt="Ice_t" loading="lazy"><figcaption>Ice_t</figcaption></figure>
<figure><img src="Palettes/Misc/MUL_Blue.jpg" alt="MUL_Blue" loading="lazy"><figcaption>MUL_Blue</figcaption></figure>
<figure><img src="Palettes/Misc/MUL_Rain.jpg" alt="MUL_Rain" loading="lazy"><figcaption>MUL_Rain</figcaption></figure>
<figure><img src="Palettes/Misc/Orangy.jpg" alt="Orangy" loading="lazy"><figcaption>Orangy</figcaption></figure>
<figure><img src="Palettes/Misc/Organic.jpg" alt="Organic" loading="lazy"><figcaption>Organic</figcaption></figure>
<figure><img src="Palettes/Misc/Pal-fir4.jpg" alt="Pal-fir4" loading="lazy"><figcaption>Pal-fir4</figcaption></figure>
<figure><img src="Palettes/Misc/Rainy.jpg" alt="Rainy" loading="lazy"><figcaption>Rainy</figcaption></figure>
<figure><img src="Palettes/Misc/RedSpace.jpg" alt="RedSpace" loading="lazy"><figcaption>RedSpace</figcaption></figure>
<figure><img src="Palettes/Misc/Ribbed1.jpg" alt="Ribbed1" loading="lazy"><figcaption>Ribbed1</figcaption></figure>
<figure><img src="Palettes/Misc/SilkFire.jpg" alt="SilkFire" loading="lazy"><figcaption>SilkFire</figcaption></figure>
<figure><img src="Palettes/Misc/Smokey.jpg" alt="Smokey" loading="lazy"><figcaption>Smokey</figcaption></figure>
<figure><img src="Palettes/Misc/SpaceY.jpg" alt="SpaceY" loading="lazy"><figcaption>SpaceY</figcaption></figure>
<figure><img src="Palettes/Misc/Trekkie.jpg" alt="Trekkie" loading="lazy"><figcaption>Trekkie</figcaption></figure>
<figure><img src="Palettes/Pal-blue.jpg" alt="Pal-blue" loading="lazy"><figcaption>Pal-blue</figcaption></figure>

### Purple  
<figure><img src="Palettes/Purple/BRI_Pink.jpg" alt="BRI_Pink" loading="lazy"><figcaption>BRI_Pink</figcaption></figure>
<figure><img src="Palettes/Purple/Cfire5.jpg" alt="Cfire5" loading="lazy"><figcaption>Cfire5</figcaption></figure>
<figure><img src="Palettes/Purple/Jpurple.jpg" alt="Jpurple" loading="lazy"><figcaption>Jpurple</figcaption></figure>
<figure><img src="Palettes/Purple/LIN_Pink.jpg" alt="LIN_Pink" loading="lazy"><figcaption>LIN_Pink</figcaption></figure>

### Realistic  
<figure><img src="Palettes/Realistic/BRI_Oran.jpg" alt="BRI_Oran" loading="lazy"><figcaption>BRI_Oran</figcaption></figure>
<figure><img src="Palettes/Realistic/Cfire7.jpg" alt="Cfire7" loading="lazy"><figcaption>Cfire7</figcaption></figure>
<figure><img src="Palettes/Realistic/Cfire9.jpg" alt="Cfire9" loading="lazy"><figcaption>Cfire9</figcaption></figure>
<figure><img src="Palettes/Realistic/Cfirey.jpg" alt="Cfirey" loading="lazy"><figcaption>Cfirey</figcaption></figure>
<figure><img src="Palettes/Realistic/Jfire0.jpg" alt="Jfire0" loading="lazy"><figcaption>Jfire0</figcaption></figure>
<figure><img src="Palettes/Realistic/Jfire1.jpg" alt="Jfire1" loading="lazy"><figcaption>Jfire1</figcaption></figure>
<figure><img src="Palettes/Realistic/Jfire2.jpg" alt="Jfire2" loading="lazy"><figcaption>Jfire2</figcaption></figure>
<figure><img src="Palettes/Realistic/LIN_Orange.jpg" alt="LIN_Orange" loading="lazy"><figcaption>LIN_Orange</figcaption></figure>
<figure><img src="Palettes/Realistic/Pal-fir.jpg" alt="Pal-fir" loading="lazy"><figcaption>Pal-fir</figcaption></figure>
<figure><img src="Palettes/Realistic/Pal-fir2.jpg" alt="Pal-fir2" loading="lazy"><figcaption>Pal-fir2</figcaption></figure>
<figure><img src="Palettes/Realistic/Pal-fir3.jpg" alt="Pal-fir3" loading="lazy"><figcaption>Pal-fir3</figcaption></figure>
<figure><img src="Palettes/Realistic/Real_One.jpg" alt="Real_One" loading="lazy"><figcaption>Real_One</figcaption></figure>
<figure><img src="Palettes/Realistic/Real_Two.jpg" alt="Real_Two" loading="lazy"><figcaption>Real_Two</figcaption></figure>
<figure><img src="Palettes/Realistic/Real_Wavy.jpg" alt="Real_Wavy" loading="lazy"><figcaption>Real_Wavy</figcaption></figure>

### Red  
<figure><img src="Palettes/Red/BRI_Red.jpg" alt="BRI_Red" loading="lazy"><figcaption>BRI_Red</figcaption></figure>
<figure><img src="Palettes/Red/Jred.jpg" alt="Jred" loading="lazy"><figcaption>Jred</figcaption></figure>
<figure><img src="Palettes/Red/Jred2.jpg" alt="Jred2" loading="lazy"><figcaption>Jred2</figcaption></figure>
<figure><img src="Palettes/Red/LIN_Red.jpg" alt="LIN_Red" loading="lazy"><figcaption>LIN_Red</figcaption></figure>
<figure><img src="Palettes/Red/MUL_Redz.jpg" alt="MUL_Redz" loading="lazy"><figcaption>MUL_Redz</figcaption></figure>

### Yellow  
<figure><img src="Palettes/Yellow/BRI_Yell.jpg" alt="BRI_Yell" loading="lazy"><figcaption>BRI_Yell</figcaption></figure>
<figure><img src="Palettes/Yellow/LIN_Yellow.jpg" alt="LIN_Yellow" loading="lazy"><figcaption>LIN_Yellow</figcaption></figure>## Paris

### Glass  
<figure><img src="Paris/Glass/pa_bldng_c_a.jpg" alt="pa_bldng_c_a" loading="lazy"><figcaption>pa_bldng_c_a</figcaption></figure>
<figure><img src="Paris/Glass/pa_bldng_c_e.jpg" alt="pa_bldng_c_e" loading="lazy"><figcaption>pa_bldng_c_e</figcaption></figure>
<figure><img src="Paris/Glass/pa_bldng_c_j.jpg" alt="pa_bldng_c_j" loading="lazy"><figcaption>pa_bldng_c_j</figcaption></figure>
<figure><img src="Paris/Glass/pa_bluelight.jpg" alt="pa_bluelight" loading="lazy"><figcaption>pa_bluelight</figcaption></figure>
<figure><img src="Paris/Glass/pa_chteulight_a.jpg" alt="pa_chteulight_a" loading="lazy"><figcaption>pa_chteulight_a</figcaption></figure>
<figure><img src="Paris/Glass/pa_grnlight.jpg" alt="pa_grnlight" loading="lazy"><figcaption>pa_grnlight</figcaption></figure>
<figure><img src="Paris/Glass/pa_kiosque_b.jpg" alt="pa_kiosque_b" loading="lazy"><figcaption>pa_kiosque_b</figcaption></figure>
<figure><img src="Paris/Glass/pa_litwindow_a.jpg" alt="pa_litwindow_a" loading="lazy"><figcaption>pa_litwindow_a</figcaption></figure>
<figure><img src="Paris/Glass/pa_litwindow_b.jpg" alt="pa_litwindow_b" loading="lazy"><figcaption>pa_litwindow_b</figcaption></figure>
<figure><img src="Paris/Glass/pa_redlight.jpg" alt="pa_redlight" loading="lazy"><figcaption>pa_redlight</figcaption></figure>
<figure><img src="Paris/Glass/pa_streetlight_.jpg" alt="pa_streetlight_" loading="lazy"><figcaption>pa_streetlight_</figcaption></figure>
<figure><img src="Paris/Glass/pa_tnnlglass_a.jpg" alt="pa_tnnlglass_a" loading="lazy"><figcaption>pa_tnnlglass_a</figcaption></figure>
<figure><img src="Paris/Glass/pa_yllwlight.jpg" alt="pa_yllwlight" loading="lazy"><figcaption>pa_yllwlight</figcaption></figure>

### Metal  
<figure><img src="Paris/Metal/CatacomEntry_B.jpg" alt="CatacomEntry_B" loading="lazy"><figcaption>CatacomEntry_B</figcaption></figure>
<figure><img src="Paris/Metal/CatacomWall_A.jpg" alt="CatacomWall_A" loading="lazy"><figcaption>CatacomWall_A</figcaption></figure>
<figure><img src="Paris/Metal/CatacomWall_B.jpg" alt="CatacomWall_B" loading="lazy"><figcaption>CatacomWall_B</figcaption></figure>
<figure><img src="Paris/Metal/CatacomWall_C.jpg" alt="CatacomWall_C" loading="lazy"><figcaption>CatacomWall_C</figcaption></figure>
<figure><img src="Paris/Metal/pa_gate_a.jpg" alt="pa_gate_a" loading="lazy"><figcaption>pa_gate_a</figcaption></figure>
<figure><img src="Paris/Metal/pa_metalfence_a.jpg" alt="pa_metalfence_a" loading="lazy"><figcaption>pa_metalfence_a</figcaption></figure>
<figure><img src="Paris/Metal/pa_metalfence_b.jpg" alt="pa_metalfence_b" loading="lazy"><figcaption>pa_metalfence_b</figcaption></figure>
<figure><img src="Paris/Metal/pa_metalfence_c.jpg" alt="pa_metalfence_c" loading="lazy"><figcaption>pa_metalfence_c</figcaption></figure>
<figure><img src="Paris/Metal/pa_metrosign_c.jpg" alt="pa_metrosign_c" loading="lazy"><figcaption>pa_metrosign_c</figcaption></figure>
<figure><img src="Paris/Metal/pa_tnnlceilng_a.jpg" alt="pa_tnnlceilng_a" loading="lazy"><figcaption>pa_tnnlceilng_a</figcaption></figure>
<figure><img src="Paris/Metal/pa_turnstll_a.jpg" alt="pa_turnstll_a" loading="lazy"><figcaption>pa_turnstll_a</figcaption></figure>
<figure><img src="Paris/Metal/pa_turnstll_b.jpg" alt="pa_turnstll_b" loading="lazy"><figcaption>pa_turnstll_b</figcaption></figure>
<figure><img src="Paris/Metal/pa_turnstll_c.jpg" alt="pa_turnstll_c" loading="lazy"><figcaption>pa_turnstll_c</figcaption></figure>
<figure><img src="Paris/Metal/pa_turnstll_d.jpg" alt="pa_turnstll_d" loading="lazy"><figcaption>pa_turnstll_d</figcaption></figure>

### Misc  
<figure><img src="Paris/Misc/CatacomPlaq_A.jpg" alt="CatacomPlaq_A" loading="lazy"><figcaption>CatacomPlaq_A</figcaption></figure>
<figure><img src="Paris/Misc/CatacomPlaq_B.jpg" alt="CatacomPlaq_B" loading="lazy"><figcaption>CatacomPlaq_B</figcaption></figure>
<figure><img src="Paris/Misc/pa_bldng_a_a.jpg" alt="pa_bldng_a_a" loading="lazy"><figcaption>pa_bldng_a_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_bldng_b_f.jpg" alt="pa_bldng_b_f" loading="lazy"><figcaption>pa_bldng_b_f</figcaption></figure>
<figure><img src="Paris/Misc/pa_bldng_b_i.jpg" alt="pa_bldng_b_i" loading="lazy"><figcaption>pa_bldng_b_i</figcaption></figure>
<figure><img src="Paris/Misc/pa_bldng_f_b.jpg" alt="pa_bldng_f_b" loading="lazy"><figcaption>pa_bldng_f_b</figcaption></figure>
<figure><img src="Paris/Misc/pa_bldng_g_a.jpg" alt="pa_bldng_g_a" loading="lazy"><figcaption>pa_bldng_g_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_bldng_h_a.jpg" alt="pa_bldng_h_a" loading="lazy"><figcaption>pa_bldng_h_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_chteupntng_a.jpg" alt="pa_chteupntng_a" loading="lazy"><figcaption>pa_chteupntng_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_chteupntng_b.jpg" alt="pa_chteupntng_b" loading="lazy"><figcaption>pa_chteupntng_b</figcaption></figure>
<figure><img src="Paris/Misc/pa_chteustvtop.jpg" alt="pa_chteustvtop" loading="lazy"><figcaption>pa_chteustvtop</figcaption></figure>
<figure><img src="Paris/Misc/pa_interior_h_a.jpg" alt="pa_interior_h_a" loading="lazy"><figcaption>pa_interior_h_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_interior_h_b.jpg" alt="pa_interior_h_b" loading="lazy"><figcaption>pa_interior_h_b</figcaption></figure>
<figure><img src="Paris/Misc/pa_interior_h_c.jpg" alt="pa_interior_h_c" loading="lazy"><figcaption>pa_interior_h_c</figcaption></figure>
<figure><img src="Paris/Misc/pa_kiosquead_a.jpg" alt="pa_kiosquead_a" loading="lazy"><figcaption>pa_kiosquead_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_kiosquead_b.jpg" alt="pa_kiosquead_b" loading="lazy"><figcaption>pa_kiosquead_b</figcaption></figure>
<figure><img src="Paris/Misc/pa_kiosquead_d.jpg" alt="pa_kiosquead_d" loading="lazy"><figcaption>pa_kiosquead_d</figcaption></figure>
<figure><img src="Paris/Misc/pa_ksq_spec_b.jpg" alt="pa_ksq_spec_b" loading="lazy"><figcaption>pa_ksq_spec_b</figcaption></figure>
<figure><img src="Paris/Misc/pa_ksquead_A00.jpg" alt="pa_ksquead_A00" loading="lazy"><figcaption>pa_ksquead_A00</figcaption></figure>
<figure><img src="Paris/Misc/pa_ksquead_A01.jpg" alt="pa_ksquead_A01" loading="lazy"><figcaption>pa_ksquead_A01</figcaption></figure>
<figure><img src="Paris/Misc/pa_ksquead_A02.jpg" alt="pa_ksquead_A02" loading="lazy"><figcaption>pa_ksquead_A02</figcaption></figure>
<figure><img src="Paris/Misc/pa_ksquead_A03.jpg" alt="pa_ksquead_A03" loading="lazy"><figcaption>pa_ksquead_A03</figcaption></figure>
<figure><img src="Paris/Misc/pa_subsign_a.jpg" alt="pa_subsign_a" loading="lazy"><figcaption>pa_subsign_a</figcaption></figure>
<figure><img src="Paris/Misc/pa_subsign_b.jpg" alt="pa_subsign_b" loading="lazy"><figcaption>pa_subsign_b</figcaption></figure>

### Stone  
<figure><img src="Paris/Stone/CatacomWall_G2.jpg" alt="CatacomWall_G2" loading="lazy"><figcaption>CatacomWall_G2</figcaption></figure>
<figure><img src="Paris/Stone/chat_bathtile.jpg" alt="chat_bathtile" loading="lazy"><figcaption>chat_bathtile</figcaption></figure>
<figure><img src="Paris/Stone/pa_Brdgewall_E.jpg" alt="pa_Brdgewall_E" loading="lazy"><figcaption>pa_Brdgewall_E</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_10.jpg" alt="pa_Number_10" loading="lazy"><figcaption>pa_Number_10</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_11.jpg" alt="pa_Number_11" loading="lazy"><figcaption>pa_Number_11</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_12.jpg" alt="pa_Number_12" loading="lazy"><figcaption>pa_Number_12</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_13.jpg" alt="pa_Number_13" loading="lazy"><figcaption>pa_Number_13</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_14.jpg" alt="pa_Number_14" loading="lazy"><figcaption>pa_Number_14</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_15.jpg" alt="pa_Number_15" loading="lazy"><figcaption>pa_Number_15</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_16.jpg" alt="pa_Number_16" loading="lazy"><figcaption>pa_Number_16</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_17.jpg" alt="pa_Number_17" loading="lazy"><figcaption>pa_Number_17</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_18.jpg" alt="pa_Number_18" loading="lazy"><figcaption>pa_Number_18</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_19.jpg" alt="pa_Number_19" loading="lazy"><figcaption>pa_Number_19</figcaption></figure>
<figure><img src="Paris/Stone/pa_Number_20.jpg" alt="pa_Number_20" loading="lazy"><figcaption>pa_Number_20</figcaption></figure>
<figure><img src="Paris/Stone/pa_bldng_b_b.jpg" alt="pa_bldng_b_b" loading="lazy"><figcaption>pa_bldng_b_b</figcaption></figure>
<figure><img src="Paris/Stone/pa_bldng_e_c.jpg" alt="pa_bldng_e_c" loading="lazy"><figcaption>pa_bldng_e_c</figcaption></figure>
<figure><img src="Paris/Stone/pa_brickwall_b.jpg" alt="pa_brickwall_b" loading="lazy"><figcaption>pa_brickwall_b</figcaption></figure>
<figure><img src="Paris/Stone/pa_brickwall_c.jpg" alt="pa_brickwall_c" loading="lazy"><figcaption>pa_brickwall_c</figcaption></figure>
<figure><img src="Paris/Stone/pa_brickwall_d.jpg" alt="pa_brickwall_d" loading="lazy"><figcaption>pa_brickwall_d</figcaption></figure>
<figure><img src="Paris/Stone/pa_chat_a.jpg" alt="pa_chat_a" loading="lazy"><figcaption>pa_chat_a</figcaption></figure>
<figure><img src="Paris/Stone/pa_chat_c.jpg" alt="pa_chat_c" loading="lazy"><figcaption>pa_chat_c</figcaption></figure>
<figure><img src="Paris/Stone/pa_chteuktchn_b.jpg" alt="pa_chteuktchn_b" loading="lazy"><figcaption>pa_chteuktchn_b</figcaption></figure>
<figure><img src="Paris/Stone/pa_chteuwall_b.jpg" alt="pa_chteuwall_b" loading="lazy"><figcaption>pa_chteuwall_b</figcaption></figure>
<figure><img src="Paris/Stone/pa_cthdrl_03.jpg" alt="pa_cthdrl_03" loading="lazy"><figcaption>pa_cthdrl_03</figcaption></figure>
<figure><img src="Paris/Stone/pa_drainage_c.jpg" alt="pa_drainage_c" loading="lazy"><figcaption>pa_drainage_c</figcaption></figure>
<figure><img src="Paris/Stone/pa_drainage_d.jpg" alt="pa_drainage_d" loading="lazy"><figcaption>pa_drainage_d</figcaption></figure>
<figure><img src="Paris/Stone/pa_sidewalk_a.jpg" alt="pa_sidewalk_a" loading="lazy"><figcaption>pa_sidewalk_a</figcaption></figure>
<figure><img src="Paris/Stone/pa_stonewall_a.jpg" alt="pa_stonewall_a" loading="lazy"><figcaption>pa_stonewall_a</figcaption></figure>
<figure><img src="Paris/Stone/pa_street_a.jpg" alt="pa_street_a" loading="lazy"><figcaption>pa_street_a</figcaption></figure>
<figure><img src="Paris/Stone/pa_wall_a.jpg" alt="pa_wall_a" loading="lazy"><figcaption>pa_wall_a</figcaption></figure>
<figure><img src="Paris/Stone/pa_wall_c.jpg" alt="pa_wall_c" loading="lazy"><figcaption>pa_wall_c</figcaption></figure>

### Textile  
<figure><img src="Paris/Textile/pa_carpet_a.jpg" alt="pa_carpet_a" loading="lazy"><figcaption>pa_carpet_a</figcaption></figure>
<figure><img src="Paris/Textile/pa_carpet_b.jpg" alt="pa_carpet_b" loading="lazy"><figcaption>pa_carpet_b</figcaption></figure>
<figure><img src="Paris/Textile/pa_window_a.jpg" alt="pa_window_a" loading="lazy"><figcaption>pa_window_a</figcaption></figure>

### Wood  
<figure><img src="Paris/Wood/chat_frntdoor.jpg" alt="chat_frntdoor" loading="lazy"><figcaption>chat_frntdoor</figcaption></figure>
<figure><img src="Paris/Wood/pa_OldDoorA.jpg" alt="pa_OldDoorA" loading="lazy"><figcaption>pa_OldDoorA</figcaption></figure>
<figure><img src="Paris/Wood/pa_OldDoorD.jpg" alt="pa_OldDoorD" loading="lazy"><figcaption>pa_OldDoorD</figcaption></figure>
<figure><img src="Paris/Wood/pa_OldDoorD2.jpg" alt="pa_OldDoorD2" loading="lazy"><figcaption>pa_OldDoorD2</figcaption></figure>
<figure><img src="Paris/Wood/pa_awning_a.jpg" alt="pa_awning_a" loading="lazy"><figcaption>pa_awning_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_bldng_a_d.jpg" alt="pa_bldng_a_d" loading="lazy"><figcaption>pa_bldng_a_d</figcaption></figure>
<figure><img src="Paris/Wood/pa_bldng_a_g.jpg" alt="pa_bldng_a_g" loading="lazy"><figcaption>pa_bldng_a_g</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteuclng_a.jpg" alt="pa_chteuclng_a" loading="lazy"><figcaption>pa_chteuclng_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteuktchn_a.jpg" alt="pa_chteuktchn_a" loading="lazy"><figcaption>pa_chteuktchn_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteutble_a.jpg" alt="pa_chteutble_a" loading="lazy"><figcaption>pa_chteutble_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteutble_b.jpg" alt="pa_chteutble_b" loading="lazy"><figcaption>pa_chteutble_b</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteutble_c.jpg" alt="pa_chteutble_c" loading="lazy"><figcaption>pa_chteutble_c</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteuwall_a.jpg" alt="pa_chteuwall_a" loading="lazy"><figcaption>pa_chteuwall_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_chteuwall_d.jpg" alt="pa_chteuwall_d" loading="lazy"><figcaption>pa_chteuwall_d</figcaption></figure>
<figure><img src="Paris/Wood/pa_interior_d_b.jpg" alt="pa_interior_d_b" loading="lazy"><figcaption>pa_interior_d_b</figcaption></figure>
<figure><img src="Paris/Wood/pa_interior_d_d.jpg" alt="pa_interior_d_d" loading="lazy"><figcaption>pa_interior_d_d</figcaption></figure>
<figure><img src="Paris/Wood/pa_interior_f_b.jpg" alt="pa_interior_f_b" loading="lazy"><figcaption>pa_interior_f_b</figcaption></figure>
<figure><img src="Paris/Wood/pa_interior_h_f.jpg" alt="pa_interior_h_f" loading="lazy"><figcaption>pa_interior_h_f</figcaption></figure>
<figure><img src="Paris/Wood/pa_interior_h_j.jpg" alt="pa_interior_h_j" loading="lazy"><figcaption>pa_interior_h_j</figcaption></figure>
<figure><img src="Paris/Wood/pa_nazi_01.jpg" alt="pa_nazi_01" loading="lazy"><figcaption>pa_nazi_01</figcaption></figure>
<figure><img src="Paris/Wood/pa_rooftop_a.jpg" alt="pa_rooftop_a" loading="lazy"><figcaption>pa_rooftop_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_tanwin_lg_a.jpg" alt="pa_tanwin_lg_a" loading="lazy"><figcaption>pa_tanwin_lg_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_tanwin_sm_b.jpg" alt="pa_tanwin_sm_b" loading="lazy"><figcaption>pa_tanwin_sm_b</figcaption></figure>
<figure><img src="Paris/Wood/pa_tnnlceilng_c.jpg" alt="pa_tnnlceilng_c" loading="lazy"><figcaption>pa_tnnlceilng_c</figcaption></figure>
<figure><img src="Paris/Wood/pa_tnnlwall_a.jpg" alt="pa_tnnlwall_a" loading="lazy"><figcaption>pa_tnnlwall_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_tnnlwall_c.jpg" alt="pa_tnnlwall_c" loading="lazy"><figcaption>pa_tnnlwall_c</figcaption></figure>
<figure><img src="Paris/Wood/pa_tnnlwall_e.jpg" alt="pa_tnnlwall_e" loading="lazy"><figcaption>pa_tnnlwall_e</figcaption></figure>
<figure><img src="Paris/Wood/pa_woodfloor_a.jpg" alt="pa_woodfloor_a" loading="lazy"><figcaption>pa_woodfloor_a</figcaption></figure>
<figure><img src="Paris/Wood/pa_ylwwin_sm_b.jpg" alt="pa_ylwwin_sm_b" loading="lazy"><figcaption>pa_ylwwin_sm_b</figcaption></figure>## Render
<figure><img src="Render/Default.jpg" alt="Default" loading="lazy"><figcaption>Default</figcaption></figure>## Rocket

### Ceramic  
<figure><img src="Rocket/Ceramic/ClenGreyFloor.jpg" alt="ClenGreyFloor" loading="lazy"><figcaption>ClenGreyFloor</figcaption></figure>
<figure><img src="Rocket/Ceramic/DrtyTileFloor_A.jpg" alt="DrtyTileFloor_A" loading="lazy"><figcaption>DrtyTileFloor_A</figcaption></figure>
<figure><img src="Rocket/ClenRcktEngne_C.jpg" alt="ClenRcktEngne_C" loading="lazy"><figcaption>ClenRcktEngne_C</figcaption></figure>

### Concrete  
<figure><img src="Rocket/Concrete/ClenCncrtWall_A.jpg" alt="ClenCncrtWall_A" loading="lazy"><figcaption>ClenCncrtWall_A</figcaption></figure>
<figure><img src="Rocket/Concrete/DrtyCncrtWall_A.jpg" alt="DrtyCncrtWall_A" loading="lazy"><figcaption>DrtyCncrtWall_A</figcaption></figure>
<figure><img src="Rocket/Concrete/DrtyCncrtWall_E.jpg" alt="DrtyCncrtWall_E" loading="lazy"><figcaption>DrtyCncrtWall_E</figcaption></figure>

### Earth  
<figure><img src="Rocket/Earth/BrownCrckdErth_.jpg" alt="BrownCrckdErth_" loading="lazy"><figcaption>BrownCrckdErth_</figcaption></figure>

### Glass  
<figure><img src="Rocket/Glass/ClenRedLight_B.jpg" alt="ClenRedLight_B" loading="lazy"><figcaption>ClenRedLight_B</figcaption></figure>
<figure><img src="Rocket/Glass/DrtyBlueLight_A.jpg" alt="DrtyBlueLight_A" loading="lazy"><figcaption>DrtyBlueLight_A</figcaption></figure>

### Metal  
<figure><img src="Rocket/Metal/BlastDoor_A.jpg" alt="BlastDoor_A" loading="lazy"><figcaption>BlastDoor_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenCntrlPanl_A.jpg" alt="ClenCntrlPanl_A" loading="lazy"><figcaption>ClenCntrlPanl_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGreenComp_A.jpg" alt="ClenGreenComp_A" loading="lazy"><figcaption>ClenGreenComp_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenFloor_A.jpg" alt="ClenGrenFloor_A" loading="lazy"><figcaption>ClenGrenFloor_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenTube_A.jpg" alt="ClenGrenTube_A" loading="lazy"><figcaption>ClenGrenTube_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenWall_A.jpg" alt="ClenGrenWall_A" loading="lazy"><figcaption>ClenGrenWall_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenWall_B.jpg" alt="ClenGrenWall_B" loading="lazy"><figcaption>ClenGrenWall_B</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenWall_C.jpg" alt="ClenGrenWall_C" loading="lazy"><figcaption>ClenGrenWall_C</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenWall_E.jpg" alt="ClenGrenWall_E" loading="lazy"><figcaption>ClenGrenWall_E</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGrenWall_F.jpg" alt="ClenGrenWall_F" loading="lazy"><figcaption>ClenGrenWall_F</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGreyBrace.jpg" alt="ClenGreyBrace" loading="lazy"><figcaption>ClenGreyBrace</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGreyUSide_A.jpg" alt="ClenGreyUSide_A" loading="lazy"><figcaption>ClenGreyUSide_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGreyVent_A.jpg" alt="ClenGreyVent_A" loading="lazy"><figcaption>ClenGreyVent_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenGreyWall_A.jpg" alt="ClenGreyWall_A" loading="lazy"><figcaption>ClenGreyWall_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenMetlAcnt_A.jpg" alt="ClenMetlAcnt_A" loading="lazy"><figcaption>ClenMetlAcnt_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktCkpit_C.jpg" alt="ClenRcktCkpit_C" loading="lazy"><figcaption>ClenRcktCkpit_C</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktCpsle_A.jpg" alt="ClenRcktCpsle_A" loading="lazy"><figcaption>ClenRcktCpsle_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktCpsle_B.jpg" alt="ClenRcktCpsle_B" loading="lazy"><figcaption>ClenRcktCpsle_B</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktCpsle_C.jpg" alt="ClenRcktCpsle_C" loading="lazy"><figcaption>ClenRcktCpsle_C</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktEngne_A.jpg" alt="ClenRcktEngne_A" loading="lazy"><figcaption>ClenRcktEngne_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktEngne_B.jpg" alt="ClenRcktEngne_B" loading="lazy"><figcaption>ClenRcktEngne_B</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktEngne_D.jpg" alt="ClenRcktEngne_D" loading="lazy"><figcaption>ClenRcktEngne_D</figcaption></figure>
<figure><img src="Rocket/Metal/ClenRcktEngne_E.jpg" alt="ClenRcktEngne_E" loading="lazy"><figcaption>ClenRcktEngne_E</figcaption></figure>
<figure><img src="Rocket/Metal/ClenTrckSide_A.jpg" alt="ClenTrckSide_A" loading="lazy"><figcaption>ClenTrckSide_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenTruckBack_A.jpg" alt="ClenTruckBack_A" loading="lazy"><figcaption>ClenTruckBack_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenUtilVSide_A.jpg" alt="ClenUtilVSide_A" loading="lazy"><figcaption>ClenUtilVSide_A</figcaption></figure>
<figure><img src="Rocket/Metal/ClenUtilVSide_B.jpg" alt="ClenUtilVSide_B" loading="lazy"><figcaption>ClenUtilVSide_B</figcaption></figure>
<figure><img src="Rocket/Metal/ClenUtilVSide_C.jpg" alt="ClenUtilVSide_C" loading="lazy"><figcaption>ClenUtilVSide_C</figcaption></figure>
<figure><img src="Rocket/Metal/ClenWallPanel_A.jpg" alt="ClenWallPanel_A" loading="lazy"><figcaption>ClenWallPanel_A</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyBrownWall_A.jpg" alt="DrtyBrownWall_A" loading="lazy"><figcaption>DrtyBrownWall_A</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyMetalDoor_A.jpg" alt="DrtyMetalDoor_A" loading="lazy"><figcaption>DrtyMetalDoor_A</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyMetalWall_A.jpg" alt="DrtyMetalWall_A" loading="lazy"><figcaption>DrtyMetalWall_A</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyMetalWall_B.jpg" alt="DrtyMetalWall_B" loading="lazy"><figcaption>DrtyMetalWall_B</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyMetalWall_C.jpg" alt="DrtyMetalWall_C" loading="lazy"><figcaption>DrtyMetalWall_C</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyRustMetal_A.jpg" alt="DrtyRustMetal_A" loading="lazy"><figcaption>DrtyRustMetal_A</figcaption></figure>
<figure><img src="Rocket/Metal/DrtyTileRoof_A.jpg" alt="DrtyTileRoof_A" loading="lazy"><figcaption>DrtyTileRoof_A</figcaption></figure>
<figure><img src="Rocket/Metal/SiloMchne_A.jpg" alt="SiloMchne_A" loading="lazy"><figcaption>SiloMchne_A</figcaption></figure>
<figure><img src="Rocket/Metal/SiloMchne_B.jpg" alt="SiloMchne_B" loading="lazy"><figcaption>SiloMchne_B</figcaption></figure>
<figure><img src="Rocket/Metal/Slippery_Metal.jpg" alt="Slippery_Metal" loading="lazy"><figcaption>Slippery_Metal</figcaption></figure>
<figure><img src="Rocket/Metal/WirePanel_silo.jpg" alt="WirePanel_silo" loading="lazy"><figcaption>WirePanel_silo</figcaption></figure>
<figure><img src="Rocket/Metal/slo_rckt_a.jpg" alt="slo_rckt_a" loading="lazy"><figcaption>slo_rckt_a</figcaption></figure>
<figure><img src="Rocket/Metal/slo_rckt_b.jpg" alt="slo_rckt_b" loading="lazy"><figcaption>slo_rckt_b</figcaption></figure>
<figure><img src="Rocket/Metal/slo_rckt_c.jpg" alt="slo_rckt_c" loading="lazy"><figcaption>slo_rckt_c</figcaption></figure>
<figure><img src="Rocket/Metal/slo_rcktgnrc_a.jpg" alt="slo_rcktgnrc_a" loading="lazy"><figcaption>slo_rcktgnrc_a</figcaption></figure>

### Misc  
<figure><img src="Rocket/Misc/ClenGreyTire_A.jpg" alt="ClenGreyTire_A" loading="lazy"><figcaption>ClenGreyTire_A</figcaption></figure>
<figure><img src="Rocket/Misc/MS_Sign_01.jpg" alt="MS_Sign_01" loading="lazy"><figcaption>MS_Sign_01</figcaption></figure>
<figure><img src="Rocket/Misc/MS_Sign_02.jpg" alt="MS_Sign_02" loading="lazy"><figcaption>MS_Sign_02</figcaption></figure>
<figure><img src="Rocket/Misc/MS_Sign_03.jpg" alt="MS_Sign_03" loading="lazy"><figcaption>MS_Sign_03</figcaption></figure>
<figure><img src="Rocket/Misc/MS_sign_04.jpg" alt="MS_sign_04" loading="lazy"><figcaption>MS_sign_04</figcaption></figure>
<figure><img src="Rocket/Misc/MS_sign_05.jpg" alt="MS_sign_05" loading="lazy"><figcaption>MS_sign_05</figcaption></figure>
<figure><img src="Rocket/Misc/MS_sign_06.jpg" alt="MS_sign_06" loading="lazy"><figcaption>MS_sign_06</figcaption></figure>
<figure><img src="Rocket/Misc/RedLight_A01.jpg" alt="RedLight_A01" loading="lazy"><figcaption>RedLight_A01</figcaption></figure>
<figure><img src="Rocket/Misc/RedLight_A02.jpg" alt="RedLight_A02" loading="lazy"><figcaption>RedLight_A02</figcaption></figure>
<figure><img src="Rocket/Misc/RedLight_A03.jpg" alt="RedLight_A03" loading="lazy"><figcaption>RedLight_A03</figcaption></figure>
<figure><img src="Rocket/Misc/RedLight_A04.jpg" alt="RedLight_A04" loading="lazy"><figcaption>RedLight_A04</figcaption></figure>
<figure><img src="Rocket/Misc/RedLight_A05.jpg" alt="RedLight_A05" loading="lazy"><figcaption>RedLight_A05</figcaption></figure>
<figure><img src="Rocket/Misc/RedLight_A06.jpg" alt="RedLight_A06" loading="lazy"><figcaption>RedLight_A06</figcaption></figure>
<figure><img src="Rocket/Misc/msilo_sign01.jpg" alt="msilo_sign01" loading="lazy"><figcaption>msilo_sign01</figcaption></figure>
<figure><img src="Rocket/Misc/msilo_sign02.jpg" alt="msilo_sign02" loading="lazy"><figcaption>msilo_sign02</figcaption></figure>
<figure><img src="Rocket/Misc/msilo_sign03.jpg" alt="msilo_sign03" loading="lazy"><figcaption>msilo_sign03</figcaption></figure>

### Wood  
<figure><img src="Rocket/Wood/DirtyBrwnFence_.jpg" alt="DirtyBrwnFence_" loading="lazy"><figcaption>DirtyBrwnFence_</figcaption></figure>
<figure><img src="Rocket/Wood/RotnWoodPlank_A.jpg" alt="RotnWoodPlank_A" loading="lazy"><figcaption>RotnWoodPlank_A</figcaption></figure>## SubBase

### Metal  
<figure><img src="SubBase/Metal/DrtyYeloScfld_A.jpg" alt="DrtyYeloScfld_A" loading="lazy"><figcaption>DrtyYeloScfld_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_CWA_DrEdge.jpg" alt="SB_CWA_DrEdge" loading="lazy"><figcaption>SB_CWA_DrEdge</figcaption></figure>
<figure><img src="SubBase/Metal/SB_CemntWall_A.jpg" alt="SB_CemntWall_A" loading="lazy"><figcaption>SB_CemntWall_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_Grate_A.jpg" alt="SB_Grate_A" loading="lazy"><figcaption>SB_Grate_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_HallFloor.jpg" alt="SB_HallFloor" loading="lazy"><figcaption>SB_HallFloor</figcaption></figure>
<figure><img src="SubBase/Metal/SB_HallWall_A.jpg" alt="SB_HallWall_A" loading="lazy"><figcaption>SB_HallWall_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_PipeSec_B.jpg" alt="SB_PipeSec_B" loading="lazy"><figcaption>SB_PipeSec_B</figcaption></figure>
<figure><img src="SubBase/Metal/SB_PipeWall_A.jpg" alt="SB_PipeWall_A" loading="lazy"><figcaption>SB_PipeWall_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_RmFloor_A.jpg" alt="SB_RmFloor_A" loading="lazy"><figcaption>SB_RmFloor_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_RoomCeiln_A.jpg" alt="SB_RoomCeiln_A" loading="lazy"><figcaption>SB_RoomCeiln_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_RoomCeiln_B.jpg" alt="SB_RoomCeiln_B" loading="lazy"><figcaption>SB_RoomCeiln_B</figcaption></figure>
<figure><img src="SubBase/Metal/SB_RoomCeiln_C.jpg" alt="SB_RoomCeiln_C" loading="lazy"><figcaption>SB_RoomCeiln_C</figcaption></figure>
<figure><img src="SubBase/Metal/SB_RoomFloor.jpg" alt="SB_RoomFloor" loading="lazy"><figcaption>SB_RoomFloor</figcaption></figure>
<figure><img src="SubBase/Metal/SB_StairFrnt_A.jpg" alt="SB_StairFrnt_A" loading="lazy"><figcaption>SB_StairFrnt_A</figcaption></figure>
<figure><img src="SubBase/Metal/SB_TubeCeiln_A.jpg" alt="SB_TubeCeiln_A" loading="lazy"><figcaption>SB_TubeCeiln_A</figcaption></figure>
<figure><img src="SubBase/Metal/shipskin01b.jpg" alt="shipskin01b" loading="lazy"><figcaption>shipskin01b</figcaption></figure>

### Misc  
<figure><img src="SubBase/Misc/SB_Sign_01.jpg" alt="SB_Sign_01" loading="lazy"><figcaption>SB_Sign_01</figcaption></figure>## Supertanker

### Glass  
<figure><img src="Supertanker/Glass/Crane_hllrf.jpg" alt="Crane_hllrf" loading="lazy"><figcaption>Crane_hllrf</figcaption></figure>
<figure><img src="Supertanker/Glass/ShipCtrlThrotle.jpg" alt="ShipCtrlThrotle" loading="lazy"><figcaption>ShipCtrlThrotle</figcaption></figure>
<figure><img src="Supertanker/Glass/ShipNaviGauge_A.jpg" alt="ShipNaviGauge_A" loading="lazy"><figcaption>ShipNaviGauge_A</figcaption></figure>
<figure><img src="Supertanker/Glass/Tube_A01.jpg" alt="Tube_A01" loading="lazy"><figcaption>Tube_A01</figcaption></figure>
<figure><img src="Supertanker/Glass/Tube_A02.jpg" alt="Tube_A02" loading="lazy"><figcaption>Tube_A02</figcaption></figure>
<figure><img src="Supertanker/Glass/Tube_A03.jpg" alt="Tube_A03" loading="lazy"><figcaption>Tube_A03</figcaption></figure>
<figure><img src="Supertanker/Glass/Tube_A04.jpg" alt="Tube_A04" loading="lazy"><figcaption>Tube_A04</figcaption></figure>
<figure><img src="Supertanker/Glass/Tube_A05.jpg" alt="Tube_A05" loading="lazy"><figcaption>Tube_A05</figcaption></figure>
<figure><img src="Supertanker/Glass/Tube_A06.jpg" alt="Tube_A06" loading="lazy"><figcaption>Tube_A06</figcaption></figure>
<figure><img src="Supertanker/Glass/WindSpeed.jpg" alt="WindSpeed" loading="lazy"><figcaption>WindSpeed</figcaption></figure>

### Metal  
<figure><img src="Supertanker/Metal/ClenBlueGrte_A2.jpg" alt="ClenBlueGrte_A2" loading="lazy"><figcaption>ClenBlueGrte_A2</figcaption></figure>
<figure><img src="Supertanker/Metal/Crane_wall.jpg" alt="Crane_wall" loading="lazy"><figcaption>Crane_wall</figcaption></figure>
<figure><img src="Supertanker/Metal/GripMetlFloor.jpg" alt="GripMetlFloor" loading="lazy"><figcaption>GripMetlFloor</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShBeigFloor_B.jpg" alt="N_ShBeigFloor_B" loading="lazy"><figcaption>N_ShBeigFloor_B</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShBrwnFloor_A.jpg" alt="N_ShBrwnFloor_A" loading="lazy"><figcaption>N_ShBrwnFloor_A</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShBrwnPlate_B.jpg" alt="N_ShBrwnPlate_B" loading="lazy"><figcaption>N_ShBrwnPlate_B</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShBrwnPlate_C.jpg" alt="N_ShBrwnPlate_C" loading="lazy"><figcaption>N_ShBrwnPlate_C</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShFloor_A.jpg" alt="N_ShFloor_A" loading="lazy"><figcaption>N_ShFloor_A</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShFloor_B.jpg" alt="N_ShFloor_B" loading="lazy"><figcaption>N_ShFloor_B</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShFloor_C.jpg" alt="N_ShFloor_C" loading="lazy"><figcaption>N_ShFloor_C</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShHighCeiln_A.jpg" alt="N_ShHighCeiln_A" loading="lazy"><figcaption>N_ShHighCeiln_A</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShMetlWall_A.jpg" alt="N_ShMetlWall_A" loading="lazy"><figcaption>N_ShMetlWall_A</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShMetlWall_B.jpg" alt="N_ShMetlWall_B" loading="lazy"><figcaption>N_ShMetlWall_B</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShMetlWall_C.jpg" alt="N_ShMetlWall_C" loading="lazy"><figcaption>N_ShMetlWall_C</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShWall_A.jpg" alt="N_ShWall_A" loading="lazy"><figcaption>N_ShWall_A</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShWall_B.jpg" alt="N_ShWall_B" loading="lazy"><figcaption>N_ShWall_B</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShWall_C.jpg" alt="N_ShWall_C" loading="lazy"><figcaption>N_ShWall_C</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShWhMetal_C.jpg" alt="N_ShWhMetal_C" loading="lazy"><figcaption>N_ShWhMetal_C</figcaption></figure>
<figure><img src="Supertanker/Metal/N_ShWhitMetal_B.jpg" alt="N_ShWhitMetal_B" loading="lazy"><figcaption>N_ShWhitMetal_B</figcaption></figure>
<figure><img src="Supertanker/Metal/STBeigFloor_A.jpg" alt="STBeigFloor_A" loading="lazy"><figcaption>STBeigFloor_A</figcaption></figure>
<figure><img src="Supertanker/Metal/STBeigMetal_A.jpg" alt="STBeigMetal_A" loading="lazy"><figcaption>STBeigMetal_A</figcaption></figure>
<figure><img src="Supertanker/Metal/STBeigMetal_B.jpg" alt="STBeigMetal_B" loading="lazy"><figcaption>STBeigMetal_B</figcaption></figure>
<figure><img src="Supertanker/Metal/STBrwnFloor_A.jpg" alt="STBrwnFloor_A" loading="lazy"><figcaption>STBrwnFloor_A</figcaption></figure>
<figure><img src="Supertanker/Metal/STBrwnPlate_A.jpg" alt="STBrwnPlate_A" loading="lazy"><figcaption>STBrwnPlate_A</figcaption></figure>
<figure><img src="Supertanker/Metal/STBrwnPlate_B.jpg" alt="STBrwnPlate_B" loading="lazy"><figcaption>STBrwnPlate_B</figcaption></figure>
<figure><img src="Supertanker/Metal/STBrwnPlate_C.jpg" alt="STBrwnPlate_C" loading="lazy"><figcaption>STBrwnPlate_C</figcaption></figure>
<figure><img src="Supertanker/Metal/STWhitMetal_A.jpg" alt="STWhitMetal_A" loading="lazy"><figcaption>STWhitMetal_A</figcaption></figure>
<figure><img src="Supertanker/Metal/STWhitPlate_A.jpg" alt="STWhitPlate_A" loading="lazy"><figcaption>STWhitPlate_A</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_Bilgewtrsgn.jpg" alt="ST_Bilgewtrsgn" loading="lazy"><figcaption>ST_Bilgewtrsgn</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_CWA_DrEdge.jpg" alt="ST_CWA_DrEdge" loading="lazy"><figcaption>ST_CWA_DrEdge</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_PipeRed.jpg" alt="ST_PipeRed" loading="lazy"><figcaption>ST_PipeRed</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_PipeSec_A.jpg" alt="ST_PipeSec_A" loading="lazy"><figcaption>ST_PipeSec_A</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StairFrnt_A.jpg" alt="ST_StairFrnt_A" loading="lazy"><figcaption>ST_StairFrnt_A</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StorCeiln.jpg" alt="ST_StorCeiln" loading="lazy"><figcaption>ST_StorCeiln</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StorEnds_A.jpg" alt="ST_StorEnds_A" loading="lazy"><figcaption>ST_StorEnds_A</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StorEnds_B.jpg" alt="ST_StorEnds_B" loading="lazy"><figcaption>ST_StorEnds_B</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StorFloors.jpg" alt="ST_StorFloors" loading="lazy"><figcaption>ST_StorFloors</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StorSide_A.jpg" alt="ST_StorSide_A" loading="lazy"><figcaption>ST_StorSide_A</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_StorSide_B.jpg" alt="ST_StorSide_B" loading="lazy"><figcaption>ST_StorSide_B</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_wall01.jpg" alt="ST_wall01" loading="lazy"><figcaption>ST_wall01</figcaption></figure>
<figure><img src="Supertanker/Metal/ST_wall02.jpg" alt="ST_wall02" loading="lazy"><figcaption>ST_wall02</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipCablRailn_D.jpg" alt="ShipCablRailn_D" loading="lazy"><figcaption>ShipCablRailn_D</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_A.jpg" alt="ShipPipeDetal_A" loading="lazy"><figcaption>ShipPipeDetal_A</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_B.jpg" alt="ShipPipeDetal_B" loading="lazy"><figcaption>ShipPipeDetal_B</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_C.jpg" alt="ShipPipeDetal_C" loading="lazy"><figcaption>ShipPipeDetal_C</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_D.jpg" alt="ShipPipeDetal_D" loading="lazy"><figcaption>ShipPipeDetal_D</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_E.jpg" alt="ShipPipeDetal_E" loading="lazy"><figcaption>ShipPipeDetal_E</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_F.jpg" alt="ShipPipeDetal_F" loading="lazy"><figcaption>ShipPipeDetal_F</figcaption></figure>
<figure><img src="Supertanker/Metal/ShipPipeDetal_G.jpg" alt="ShipPipeDetal_G" loading="lazy"><figcaption>ShipPipeDetal_G</figcaption></figure>
<figure><img src="Supertanker/Metal/bridge01.jpg" alt="bridge01" loading="lazy"><figcaption>bridge01</figcaption></figure>
<figure><img src="Supertanker/Metal/bridge02.jpg" alt="bridge02" loading="lazy"><figcaption>bridge02</figcaption></figure>
<figure><img src="Supertanker/Metal/bridge02b.jpg" alt="bridge02b" loading="lazy"><figcaption>bridge02b</figcaption></figure>
<figure><img src="Supertanker/Metal/hall3.jpg" alt="hall3" loading="lazy"><figcaption>hall3</figcaption></figure>
<figure><img src="Supertanker/Metal/pipends01.jpg" alt="pipends01" loading="lazy"><figcaption>pipends01</figcaption></figure>
<figure><img src="Supertanker/Metal/roof1.jpg" alt="roof1" loading="lazy"><figcaption>roof1</figcaption></figure>
<figure><img src="Supertanker/Metal/shipskin01.jpg" alt="shipskin01" loading="lazy"><figcaption>shipskin01</figcaption></figure>
<figure><img src="Supertanker/Metal/shipskin02.jpg" alt="shipskin02" loading="lazy"><figcaption>shipskin02</figcaption></figure>
<figure><img src="Supertanker/Metal/shipskin02b.jpg" alt="shipskin02b" loading="lazy"><figcaption>shipskin02b</figcaption></figure>
<figure><img src="Supertanker/Metal/shipskin03.jpg" alt="shipskin03" loading="lazy"><figcaption>shipskin03</figcaption></figure>
<figure><img src="Supertanker/Metal/shipskintrans.jpg" alt="shipskintrans" loading="lazy"><figcaption>shipskintrans</figcaption></figure>
<figure><img src="Supertanker/Metal/shpwall01cgra.jpg" alt="shpwall01cgra" loading="lazy"><figcaption>shpwall01cgra</figcaption></figure>
<figure><img src="Supertanker/Metal/shpwall01dgra.jpg" alt="shpwall01dgra" loading="lazy"><figcaption>shpwall01dgra</figcaption></figure>
<figure><img src="Supertanker/Metal/shpwall01egra.jpg" alt="shpwall01egra" loading="lazy"><figcaption>shpwall01egra</figcaption></figure>
<figure><img src="Supertanker/Metal/shpwall01gra.jpg" alt="shpwall01gra" loading="lazy"><figcaption>shpwall01gra</figcaption></figure>
<figure><img src="Supertanker/Metal/shpwall01hgra.jpg" alt="shpwall01hgra" loading="lazy"><figcaption>shpwall01hgra</figcaption></figure>
<figure><img src="Supertanker/Metal/shpwall02gra.jpg" alt="shpwall02gra" loading="lazy"><figcaption>shpwall02gra</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_01.jpg" alt="st_sign_01" loading="lazy"><figcaption>st_sign_01</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_02.jpg" alt="st_sign_02" loading="lazy"><figcaption>st_sign_02</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_03.jpg" alt="st_sign_03" loading="lazy"><figcaption>st_sign_03</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_05.jpg" alt="st_sign_05" loading="lazy"><figcaption>st_sign_05</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_07.jpg" alt="st_sign_07" loading="lazy"><figcaption>st_sign_07</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_08.jpg" alt="st_sign_08" loading="lazy"><figcaption>st_sign_08</figcaption></figure>
<figure><img src="Supertanker/Metal/st_sign_09.jpg" alt="st_sign_09" loading="lazy"><figcaption>st_sign_09</figcaption></figure>
<figure><img src="Supertanker/Metal/st_wall_01.jpg" alt="st_wall_01" loading="lazy"><figcaption>st_wall_01</figcaption></figure>
<figure><img src="Supertanker/Metal/st_wall_02.jpg" alt="st_wall_02" loading="lazy"><figcaption>st_wall_02</figcaption></figure>

### Stucco  
<figure><img src="Supertanker/Stucco/Crane_tile1.jpg" alt="Crane_tile1" loading="lazy"><figcaption>Crane_tile1</figcaption></figure>

### Textile  
<figure><img src="Supertanker/Textile/Crane_crpt1.jpg" alt="Crane_crpt1" loading="lazy"><figcaption>Crane_crpt1</figcaption></figure>
<figure><img src="Supertanker/Textile/ST_NoSlipFlr_A.jpg" alt="ST_NoSlipFlr_A" loading="lazy"><figcaption>ST_NoSlipFlr_A</figcaption></figure>## UNATCO

### Earth  
<figure><img src="UNATCO/Earth/MudLake_A.jpg" alt="MudLake_A" loading="lazy"><figcaption>MudLake_A</figcaption></figure>

### Effects  
<figure><img src="UNATCO/Effects/water_a.jpg" alt="water_a" loading="lazy"><figcaption>water_a</figcaption></figure>

### Glass  
<figure><img src="UNATCO/Glass/CeilnLite_A2.jpg" alt="CeilnLite_A2" loading="lazy"><figcaption>CeilnLite_A2</figcaption></figure>
<figure><img src="UNATCO/Glass/Uob_HangnLight.jpg" alt="Uob_HangnLight" loading="lazy"><figcaption>Uob_HangnLight</figcaption></figure>
<figure><img src="UNATCO/Glass/un_insetlight_b.jpg" alt="un_insetlight_b" loading="lazy"><figcaption>un_insetlight_b</figcaption></figure>
<figure><img src="UNATCO/Glass/un_light_a.jpg" alt="un_light_a" loading="lazy"><figcaption>un_light_a</figcaption></figure>
<figure><img src="UNATCO/Glass/un_lightmdcl_a.jpg" alt="un_lightmdcl_a" loading="lazy"><figcaption>un_lightmdcl_a</figcaption></figure>

### Metal  
<figure><img src="UNATCO/Metal/Uob_Catwalk.jpg" alt="Uob_Catwalk" loading="lazy"><figcaption>Uob_Catwalk</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_ConRmCeiln.jpg" alt="Uob_ConRmCeiln" loading="lazy"><figcaption>Uob_ConRmCeiln</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_Consocab_A.jpg" alt="Uob_Consocab_A" loading="lazy"><figcaption>Uob_Consocab_A</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_Consocab_B.jpg" alt="Uob_Consocab_B" loading="lazy"><figcaption>Uob_Consocab_B</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_Consocab_D.jpg" alt="Uob_Consocab_D" loading="lazy"><figcaption>Uob_Consocab_D</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_GrenCheckr.jpg" alt="Uob_GrenCheckr" loading="lazy"><figcaption>Uob_GrenCheckr</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_HighCeiln.jpg" alt="Uob_HighCeiln" loading="lazy"><figcaption>Uob_HighCeiln</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_MorePipes.jpg" alt="Uob_MorePipes" loading="lazy"><figcaption>Uob_MorePipes</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_OvrHedLite.jpg" alt="Uob_OvrHedLite" loading="lazy"><figcaption>Uob_OvrHedLite</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_RampEdgeUP.jpg" alt="Uob_RampEdgeUP" loading="lazy"><figcaption>Uob_RampEdgeUP</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_RampEdge_A.jpg" alt="Uob_RampEdge_A" loading="lazy"><figcaption>Uob_RampEdge_A</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_RampTop.jpg" alt="Uob_RampTop" loading="lazy"><figcaption>Uob_RampTop</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_RedCheckr.jpg" alt="Uob_RedCheckr" loading="lazy"><figcaption>Uob_RedCheckr</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_SB_Ceiln_C.jpg" alt="Uob_SB_Ceiln_C" loading="lazy"><figcaption>Uob_SB_Ceiln_C</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_SB_SDetl_A.jpg" alt="Uob_SB_SDetl_A" loading="lazy"><figcaption>Uob_SB_SDetl_A</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_StairFront.jpg" alt="Uob_StairFront" loading="lazy"><figcaption>Uob_StairFront</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_StairTop.jpg" alt="Uob_StairTop" loading="lazy"><figcaption>Uob_StairTop</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_YeloCheckr.jpg" alt="Uob_YeloCheckr" loading="lazy"><figcaption>Uob_YeloCheckr</figcaption></figure>
<figure><img src="UNATCO/Metal/Uob_uprPipeWall.jpg" alt="Uob_uprPipeWall" loading="lazy"><figcaption>Uob_uprPipeWall</figcaption></figure>
<figure><img src="UNATCO/Metal/un_BlastDoor_A.jpg" alt="un_BlastDoor_A" loading="lazy"><figcaption>un_BlastDoor_A</figcaption></figure>
<figure><img src="UNATCO/Metal/un_CargoBoxMed.jpg" alt="un_CargoBoxMed" loading="lazy"><figcaption>un_CargoBoxMed</figcaption></figure>
<figure><img src="UNATCO/Metal/un_CargoBox_A.jpg" alt="un_CargoBox_A" loading="lazy"><figcaption>un_CargoBox_A</figcaption></figure>
<figure><img src="UNATCO/Metal/un_CargoBox_B.jpg" alt="un_CargoBox_B" loading="lazy"><figcaption>un_CargoBox_B</figcaption></figure>
<figure><img src="UNATCO/Metal/un_DirtyVent_A.jpg" alt="un_DirtyVent_A" loading="lazy"><figcaption>un_DirtyVent_A</figcaption></figure>
<figure><img src="UNATCO/Metal/un_EnrtyTrim_B.jpg" alt="un_EnrtyTrim_B" loading="lazy"><figcaption>un_EnrtyTrim_B</figcaption></figure>
<figure><img src="UNATCO/Metal/un_MetalDoor_A.jpg" alt="un_MetalDoor_A" loading="lazy"><figcaption>un_MetalDoor_A</figcaption></figure>
<figure><img src="UNATCO/Metal/un_TorchDetl.jpg" alt="un_TorchDetl" loading="lazy"><figcaption>un_TorchDetl</figcaption></figure>
<figure><img src="UNATCO/Metal/un_bad_doorX.jpg" alt="un_bad_doorX" loading="lazy"><figcaption>un_bad_doorX</figcaption></figure>
<figure><img src="UNATCO/Metal/un_safe.jpg" alt="un_safe" loading="lazy"><figcaption>un_safe</figcaption></figure>

### Misc  
<figure><img src="UNATCO/Misc/CompOps_sign.jpg" alt="CompOps_sign" loading="lazy"><figcaption>CompOps_sign</figcaption></figure>
<figure><img src="UNATCO/Misc/DangUnstabCond.jpg" alt="DangUnstabCond" loading="lazy"><figcaption>DangUnstabCond</figcaption></figure>
<figure><img src="UNATCO/Misc/Group_photo.jpg" alt="Group_photo" loading="lazy"><figcaption>Group_photo</figcaption></figure>
<figure><img src="UNATCO/Misc/MedLab_sign.jpg" alt="MedLab_sign" loading="lazy"><figcaption>MedLab_sign</figcaption></figure>
<figure><img src="UNATCO/Misc/MedSignLite_A.jpg" alt="MedSignLite_A" loading="lazy"><figcaption>MedSignLite_A</figcaption></figure>
<figure><img src="UNATCO/Misc/ObstSign_A.jpg" alt="ObstSign_A" loading="lazy"><figcaption>ObstSign_A</figcaption></figure>
<figure><img src="UNATCO/Misc/QuartSign_A.jpg" alt="QuartSign_A" loading="lazy"><figcaption>QuartSign_A</figcaption></figure>
<figure><img src="UNATCO/Misc/SciFiPoster_A.jpg" alt="SciFiPoster_A" loading="lazy"><figcaption>SciFiPoster_A</figcaption></figure>
<figure><img src="UNATCO/Misc/TRN_sign_01.jpg" alt="TRN_sign_01" loading="lazy"><figcaption>TRN_sign_01</figcaption></figure>
<figure><img src="UNATCO/Misc/TRN_sign_02.jpg" alt="TRN_sign_02" loading="lazy"><figcaption>TRN_sign_02</figcaption></figure>
<figure><img src="UNATCO/Misc/TRN_sign_03.jpg" alt="TRN_sign_03" loading="lazy"><figcaption>TRN_sign_03</figcaption></figure>
<figure><img src="UNATCO/Misc/TRN_sign_04.jpg" alt="TRN_sign_04" loading="lazy"><figcaption>TRN_sign_04</figcaption></figure>
<figure><img src="UNATCO/Misc/TRN_sign_05.jpg" alt="TRN_sign_05" loading="lazy"><figcaption>TRN_sign_05</figcaption></figure>
<figure><img src="UNATCO/Misc/UNATCOseal_A.jpg" alt="UNATCOseal_A" loading="lazy"><figcaption>UNATCOseal_A</figcaption></figure>
<figure><img src="UNATCO/Misc/UNAT_PersOnly.jpg" alt="UNAT_PersOnly" loading="lazy"><figcaption>UNAT_PersOnly</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_ConRmFloor.jpg" alt="Uob_ConRmFloor" loading="lazy"><figcaption>Uob_ConRmFloor</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consocab_C.jpg" alt="Uob_Consocab_C" loading="lazy"><figcaption>Uob_Consocab_C</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_A01.jpg" alt="Uob_Consol_A01" loading="lazy"><figcaption>Uob_Consol_A01</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_A02.jpg" alt="Uob_Consol_A02" loading="lazy"><figcaption>Uob_Consol_A02</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_A03.jpg" alt="Uob_Consol_A03" loading="lazy"><figcaption>Uob_Consol_A03</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_A04.jpg" alt="Uob_Consol_A04" loading="lazy"><figcaption>Uob_Consol_A04</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_D01.jpg" alt="Uob_Consol_D01" loading="lazy"><figcaption>Uob_Consol_D01</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_D02.jpg" alt="Uob_Consol_D02" loading="lazy"><figcaption>Uob_Consol_D02</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_D03.jpg" alt="Uob_Consol_D03" loading="lazy"><figcaption>Uob_Consol_D03</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Consol_D04.jpg" alt="Uob_Consol_D04" loading="lazy"><figcaption>Uob_Consol_D04</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_Floor_A.jpg" alt="Uob_Floor_A" loading="lazy"><figcaption>Uob_Floor_A</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_ObsMap_A00.jpg" alt="Uob_ObsMap_A00" loading="lazy"><figcaption>Uob_ObsMap_A00</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_ObsMap_A01.jpg" alt="Uob_ObsMap_A01" loading="lazy"><figcaption>Uob_ObsMap_A01</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_ObsMap_A02.jpg" alt="Uob_ObsMap_A02" loading="lazy"><figcaption>Uob_ObsMap_A02</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_ObsMap_A03.jpg" alt="Uob_ObsMap_A03" loading="lazy"><figcaption>Uob_ObsMap_A03</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_ObsMap_A04.jpg" alt="Uob_ObsMap_A04" loading="lazy"><figcaption>Uob_ObsMap_A04</figcaption></figure>
<figure><img src="UNATCO/Misc/Uob_TubeLight.jpg" alt="Uob_TubeLight" loading="lazy"><figcaption>Uob_TubeLight</figcaption></figure>
<figure><img src="UNATCO/Misc/WorldMap_A.jpg" alt="WorldMap_A" loading="lazy"><figcaption>WorldMap_A</figcaption></figure>
<figure><img src="UNATCO/Misc/WorldMap_A2.jpg" alt="WorldMap_A2" loading="lazy"><figcaption>WorldMap_A2</figcaption></figure>
<figure><img src="UNATCO/Misc/WorldMap_B.jpg" alt="WorldMap_B" loading="lazy"><figcaption>WorldMap_B</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_AJ.jpg" alt="sign_AJ" loading="lazy"><figcaption>sign_AJ</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_AN.jpg" alt="sign_AN" loading="lazy"><figcaption>sign_AN</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_GH.jpg" alt="sign_GH" loading="lazy"><figcaption>sign_GH</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_JCD.jpg" alt="sign_JCD" loading="lazy"><figcaption>sign_JCD</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_JM.jpg" alt="sign_JM" loading="lazy"><figcaption>sign_JM</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_JR.jpg" alt="sign_JR" loading="lazy"><figcaption>sign_JR</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_Level1.jpg" alt="sign_Level1" loading="lazy"><figcaption>sign_Level1</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_Level2.jpg" alt="sign_Level2" loading="lazy"><figcaption>sign_Level2</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_Level3.jpg" alt="sign_Level3" loading="lazy"><figcaption>sign_Level3</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_Level4.jpg" alt="sign_Level4" loading="lazy"><figcaption>sign_Level4</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_NB.jpg" alt="sign_NB" loading="lazy"><figcaption>sign_NB</figcaption></figure>
<figure><img src="UNATCO/Misc/sign_SC.jpg" alt="sign_SC" loading="lazy"><figcaption>sign_SC</figcaption></figure>
<figure><img src="UNATCO/Misc/un_1stLevFloor.jpg" alt="un_1stLevFloor" loading="lazy"><figcaption>un_1stLevFloor</figcaption></figure>
<figure><img src="UNATCO/Misc/un_LibIslandMap.jpg" alt="un_LibIslandMap" loading="lazy"><figcaption>un_LibIslandMap</figcaption></figure>
<figure><img src="UNATCO/Misc/un_PrezMeadPic.jpg" alt="un_PrezMeadPic" loading="lazy"><figcaption>un_PrezMeadPic</figcaption></figure>
<figure><img src="UNATCO/Misc/un_bboard.jpg" alt="un_bboard" loading="lazy"><figcaption>un_bboard</figcaption></figure>
<figure><img src="UNATCO/Misc/un_gunpster2.jpg" alt="un_gunpster2" loading="lazy"><figcaption>un_gunpster2</figcaption></figure>
<figure><img src="UNATCO/Misc/un_medcmptr_A00.jpg" alt="un_medcmptr_A00" loading="lazy"><figcaption>un_medcmptr_A00</figcaption></figure>
<figure><img src="UNATCO/Misc/un_medcmptr_A01.jpg" alt="un_medcmptr_A01" loading="lazy"><figcaption>un_medcmptr_A01</figcaption></figure>
<figure><img src="UNATCO/Misc/un_medcmptr_A02.jpg" alt="un_medcmptr_A02" loading="lazy"><figcaption>un_medcmptr_A02</figcaption></figure>
<figure><img src="UNATCO/Misc/un_medcmptr_A03.jpg" alt="un_medcmptr_A03" loading="lazy"><figcaption>un_medcmptr_A03</figcaption></figure>
<figure><img src="UNATCO/Misc/un_medcmptr_A04.jpg" alt="un_medcmptr_A04" loading="lazy"><figcaption>un_medcmptr_A04</figcaption></figure>
<figure><img src="UNATCO/Misc/un_medcmptr_A05.jpg" alt="un_medcmptr_A05" loading="lazy"><figcaption>un_medcmptr_A05</figcaption></figure>
<figure><img src="UNATCO/Misc/un_whiteboard_a.jpg" alt="un_whiteboard_a" loading="lazy"><figcaption>un_whiteboard_a</figcaption></figure>

### Sky  
<figure><img src="UNATCO/Sky/NYskybox1Mbs.jpg" alt="NYskybox1Mbs" loading="lazy"><figcaption>NYskybox1Mbs</figcaption></figure>
<figure><img src="UNATCO/Sky/NYskybox2Mbs.jpg" alt="NYskybox2Mbs" loading="lazy"><figcaption>NYskybox2Mbs</figcaption></figure>
<figure><img src="UNATCO/Sky/NYskybox3Mbs.jpg" alt="NYskybox3Mbs" loading="lazy"><figcaption>NYskybox3Mbs</figcaption></figure>
<figure><img src="UNATCO/Sky/NYskybox4Mbs.jpg" alt="NYskybox4Mbs" loading="lazy"><figcaption>NYskybox4Mbs</figcaption></figure>
<figure><img src="UNATCO/Sky/NYskybox5Mbs.jpg" alt="NYskybox5Mbs" loading="lazy"><figcaption>NYskybox5Mbs</figcaption></figure>
<figure><img src="UNATCO/Sky/NYskybox6Mbs.jpg" alt="NYskybox6Mbs" loading="lazy"><figcaption>NYskybox6Mbs</figcaption></figure>

### Stone  
<figure><img src="UNATCO/Stone/LowrLevCeiln_A.jpg" alt="LowrLevCeiln_A" loading="lazy"><figcaption>LowrLevCeiln_A</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevCeiln_B.jpg" alt="LowrLevCeiln_B" loading="lazy"><figcaption>LowrLevCeiln_B</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevFloor_A.jpg" alt="LowrLevFloor_A" loading="lazy"><figcaption>LowrLevFloor_A</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevFloor_B.jpg" alt="LowrLevFloor_B" loading="lazy"><figcaption>LowrLevFloor_B</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevFloor_C.jpg" alt="LowrLevFloor_C" loading="lazy"><figcaption>LowrLevFloor_C</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevWall_A.jpg" alt="LowrLevWall_A" loading="lazy"><figcaption>LowrLevWall_A</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevWall_B.jpg" alt="LowrLevWall_B" loading="lazy"><figcaption>LowrLevWall_B</figcaption></figure>
<figure><img src="UNATCO/Stone/LowrLevWall_C.jpg" alt="LowrLevWall_C" loading="lazy"><figcaption>LowrLevWall_C</figcaption></figure>
<figure><img src="UNATCO/Stone/MedAreaWall_A.jpg" alt="MedAreaWall_A" loading="lazy"><figcaption>MedAreaWall_A</figcaption></figure>
<figure><img src="UNATCO/Stone/MedAreaWall_B.jpg" alt="MedAreaWall_B" loading="lazy"><figcaption>MedAreaWall_B</figcaption></figure>
<figure><img src="UNATCO/Stone/UNAT_sign64.jpg" alt="UNAT_sign64" loading="lazy"><figcaption>UNAT_sign64</figcaption></figure>
<figure><img src="UNATCO/Stone/UN_Wall_Blue.jpg" alt="UN_Wall_Blue" loading="lazy"><figcaption>UN_Wall_Blue</figcaption></figure>
<figure><img src="UNATCO/Stone/UN_Wall_Green.jpg" alt="UN_Wall_Green" loading="lazy"><figcaption>UN_Wall_Green</figcaption></figure>
<figure><img src="UNATCO/Stone/UN_Wall_White.jpg" alt="UN_Wall_White" loading="lazy"><figcaption>UN_Wall_White</figcaption></figure>
<figure><img src="UNATCO/Stone/UNstatbrGraf_A.jpg" alt="UNstatbrGraf_A" loading="lazy"><figcaption>UNstatbrGraf_A</figcaption></figure>
<figure><img src="UNATCO/Stone/UNstatbrGraf_B.jpg" alt="UNstatbrGraf_B" loading="lazy"><figcaption>UNstatbrGraf_B</figcaption></figure>
<figure><img src="UNATCO/Stone/Uob_BlockWall_A.jpg" alt="Uob_BlockWall_A" loading="lazy"><figcaption>Uob_BlockWall_A</figcaption></figure>
<figure><img src="UNATCO/Stone/Uob_ConRmDetl_A.jpg" alt="Uob_ConRmDetl_A" loading="lazy"><figcaption>Uob_ConRmDetl_A</figcaption></figure>
<figure><img src="UNATCO/Stone/Uob_CrestWall.jpg" alt="Uob_CrestWall" loading="lazy"><figcaption>Uob_CrestWall</figcaption></figure>
<figure><img src="UNATCO/Stone/Uob_Far_Wall_B.jpg" alt="Uob_Far_Wall_B" loading="lazy"><figcaption>Uob_Far_Wall_B</figcaption></figure>
<figure><img src="UNATCO/Stone/Uob_TubeCeiln.jpg" alt="Uob_TubeCeiln" loading="lazy"><figcaption>Uob_TubeCeiln</figcaption></figure>
<figure><img src="UNATCO/Stone/un_2ndLevFloor.jpg" alt="un_2ndLevFloor" loading="lazy"><figcaption>un_2ndLevFloor</figcaption></figure>
<figure><img src="UNATCO/Stone/un_DirtyCement.jpg" alt="un_DirtyCement" loading="lazy"><figcaption>un_DirtyCement</figcaption></figure>
<figure><img src="UNATCO/Stone/un_DirtyWall_A.jpg" alt="un_DirtyWall_A" loading="lazy"><figcaption>un_DirtyWall_A</figcaption></figure>
<figure><img src="UNATCO/Stone/un_InWall_A.jpg" alt="un_InWall_A" loading="lazy"><figcaption>un_InWall_A</figcaption></figure>
<figure><img src="UNATCO/Stone/un_StairwaySide.jpg" alt="un_StairwaySide" loading="lazy"><figcaption>un_StairwaySide</figcaption></figure>
<figure><img src="UNATCO/Stone/un_cncrtwall_01.jpg" alt="un_cncrtwall_01" loading="lazy"><figcaption>un_cncrtwall_01</figcaption></figure>
<figure><img src="UNATCO/Stone/un_heli_c.jpg" alt="un_heli_c" loading="lazy"><figcaption>un_heli_c</figcaption></figure>
<figure><img src="UNATCO/Stone/un_heli_cncrt_c.jpg" alt="un_heli_cncrt_c" loading="lazy"><figcaption>un_heli_cncrt_c</figcaption></figure>
<figure><img src="UNATCO/Stone/un_sBrickwalk.jpg" alt="un_sBrickwalk" loading="lazy"><figcaption>un_sBrickwalk</figcaption></figure>
<figure><img src="UNATCO/Stone/un_sStoneWalk.jpg" alt="un_sStoneWalk" loading="lazy"><figcaption>un_sStoneWalk</figcaption></figure>
<figure><img src="UNATCO/Stone/un_seawall_aS.jpg" alt="un_seawall_aS" loading="lazy"><figcaption>un_seawall_aS</figcaption></figure>
<figure><img src="UNATCO/Stone/un_starwall_a.jpg" alt="un_starwall_a" loading="lazy"><figcaption>un_starwall_a</figcaption></figure>
<figure><img src="UNATCO/Stone/un_stat_tnwll_b.jpg" alt="un_stat_tnwll_b" loading="lazy"><figcaption>un_stat_tnwll_b</figcaption></figure>
<figure><img src="UNATCO/Stone/un_statbrickBS.jpg" alt="un_statbrickBS" loading="lazy"><figcaption>un_statbrickBS</figcaption></figure>
<figure><img src="UNATCO/Stone/un_statsdwk_aS.jpg" alt="un_statsdwk_aS" loading="lazy"><figcaption>un_statsdwk_aS</figcaption></figure>
<figure><img src="UNATCO/Stone/un_statwlkwy_a.jpg" alt="un_statwlkwy_a" loading="lazy"><figcaption>un_statwlkwy_a</figcaption></figure>
<figure><img src="UNATCO/Stone/un_statwlkwy_c.jpg" alt="un_statwlkwy_c" loading="lazy"><figcaption>un_statwlkwy_c</figcaption></figure>
<figure><img src="UNATCO/Stone/un_statwlkwy_d.jpg" alt="un_statwlkwy_d" loading="lazy"><figcaption>un_statwlkwy_d</figcaption></figure>
<figure><img src="UNATCO/Stone/un_stonsign_a.jpg" alt="un_stonsign_a" loading="lazy"><figcaption>un_stonsign_a</figcaption></figure>
<figure><img src="UNATCO/Stone/un_stonsign_b.jpg" alt="un_stonsign_b" loading="lazy"><figcaption>un_stonsign_b</figcaption></figure>

### Textile  
<figure><img src="UNATCO/Textile/un_grnyblnkt_a.jpg" alt="un_grnyblnkt_a" loading="lazy"><figcaption>un_grnyblnkt_a</figcaption></figure>
<figure><img src="UNATCO/Textile/un_grnyblnkt_b.jpg" alt="un_grnyblnkt_b" loading="lazy"><figcaption>un_grnyblnkt_b</figcaption></figure>

### Wood  
<figure><img src="UNATCO/Wood/un_dock_S.jpg" alt="un_dock_S" loading="lazy"><figcaption>un_dock_S</figcaption></figure>
<figure><img src="UNATCO/Wood/un_wallplaq_f.jpg" alt="un_wallplaq_f" loading="lazy"><figcaption>un_wallplaq_f</figcaption></figure>
<figure><img src="UNATCO/Wood/un_wallplaq_g.jpg" alt="un_wallplaq_g" loading="lazy"><figcaption>un_wallplaq_g</figcaption></figure>
<figure><img src="UNATCO/Wood/un_wallplaq_h.jpg" alt="un_wallplaq_h" loading="lazy"><figcaption>un_wallplaq_h</figcaption></figure>
<figure><img src="UNATCO/Wood/un_woodfloor_a.jpg" alt="un_woodfloor_a" loading="lazy"><figcaption>un_woodfloor_a</figcaption></figure>
<figure><img src="UNATCO/Wood/un_woodfloor_b.jpg" alt="un_woodfloor_b" loading="lazy"><figcaption>un_woodfloor_b</figcaption></figure>
<figure><img src="UNATCO/Wood/un_woodwall_d.jpg" alt="un_woodwall_d" loading="lazy"><figcaption>un_woodwall_d</figcaption></figure>## V_Com_Center

### Ceramic  
<figure><img src="V_Com_Center/Ceramic/ClenGreyStair_A.jpg" alt="ClenGreyStair_A" loading="lazy"><figcaption>ClenGreyStair_A</figcaption></figure>
<figure><img src="V_Com_Center/Ceramic/ClenGreyStair_B.jpg" alt="ClenGreyStair_B" loading="lazy"><figcaption>ClenGreyStair_B</figcaption></figure>
<figure><img src="V_Com_Center/ClenCmandWall_A.jpg" alt="ClenCmandWall_A" loading="lazy"><figcaption>ClenCmandWall_A</figcaption></figure>
<figure><img src="V_Com_Center/ClenCmandWall_B.jpg" alt="ClenCmandWall_B" loading="lazy"><figcaption>ClenCmandWall_B</figcaption></figure>
<figure><img src="V_Com_Center/ClenCncrtWall_H.jpg" alt="ClenCncrtWall_H" loading="lazy"><figcaption>ClenCncrtWall_H</figcaption></figure>
<figure><img src="V_Com_Center/ClenCncrtWall_I.jpg" alt="ClenCncrtWall_I" loading="lazy"><figcaption>ClenCncrtWall_I</figcaption></figure>

### Glass  
<figure><img src="V_Com_Center/Glass/ClenBlueLight_A.jpg" alt="ClenBlueLight_A" loading="lazy"><figcaption>ClenBlueLight_A</figcaption></figure>
<figure><img src="V_Com_Center/Glass/ClenCmandLite_A.jpg" alt="ClenCmandLite_A" loading="lazy"><figcaption>ClenCmandLite_A</figcaption></figure>
<figure><img src="V_Com_Center/Glass/ClenCmandLite_B.jpg" alt="ClenCmandLite_B" loading="lazy"><figcaption>ClenCmandLite_B</figcaption></figure>
<figure><img src="V_Com_Center/Glass/ClenGrenLight_A.jpg" alt="ClenGrenLight_A" loading="lazy"><figcaption>ClenGrenLight_A</figcaption></figure>
<figure><img src="V_Com_Center/Glass/ClenWethrPanl_A.jpg" alt="ClenWethrPanl_A" loading="lazy"><figcaption>ClenWethrPanl_A</figcaption></figure>
<figure><img src="V_Com_Center/Glass/CommMapB_A01.jpg" alt="CommMapB_A01" loading="lazy"><figcaption>CommMapB_A01</figcaption></figure>
<figure><img src="V_Com_Center/Glass/VBC_Light_01.jpg" alt="VBC_Light_01" loading="lazy"><figcaption>VBC_Light_01</figcaption></figure>

### Metal  
<figure><img src="V_Com_Center/Metal/ClenBrwnDoor_A.jpg" alt="ClenBrwnDoor_A" loading="lazy"><figcaption>ClenBrwnDoor_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/ClenCmandComp_A.jpg" alt="ClenCmandComp_A" loading="lazy"><figcaption>ClenCmandComp_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/ClenCmandComp_B.jpg" alt="ClenCmandComp_B" loading="lazy"><figcaption>ClenCmandComp_B</figcaption></figure>
<figure><img src="V_Com_Center/Metal/ClenCmandWall_F.jpg" alt="ClenCmandWall_F" loading="lazy"><figcaption>ClenCmandWall_F</figcaption></figure>
<figure><img src="V_Com_Center/Metal/ClenCommPanel_A.jpg" alt="ClenCommPanel_A" loading="lazy"><figcaption>ClenCommPanel_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/ClenGreyTile_A.jpg" alt="ClenGreyTile_A" loading="lazy"><figcaption>ClenGreyTile_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/ClenMetlWall_A.jpg" alt="ClenMetlWall_A" loading="lazy"><figcaption>ClenMetlWall_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/Computer01_A01.jpg" alt="Computer01_A01" loading="lazy"><figcaption>Computer01_A01</figcaption></figure>
<figure><img src="V_Com_Center/Metal/Computer01_A02.jpg" alt="Computer01_A02" loading="lazy"><figcaption>Computer01_A02</figcaption></figure>
<figure><img src="V_Com_Center/Metal/Computer01_A03.jpg" alt="Computer01_A03" loading="lazy"><figcaption>Computer01_A03</figcaption></figure>
<figure><img src="V_Com_Center/Metal/DrtyCmandWall_A.jpg" alt="DrtyCmandWall_A" loading="lazy"><figcaption>DrtyCmandWall_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/StopSign_A.jpg" alt="StopSign_A" loading="lazy"><figcaption>StopSign_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/VBC_Panel_01.jpg" alt="VBC_Panel_01" loading="lazy"><figcaption>VBC_Panel_01</figcaption></figure>
<figure><img src="V_Com_Center/Metal/WarningStripe_A.jpg" alt="WarningStripe_A" loading="lazy"><figcaption>WarningStripe_A</figcaption></figure>
<figure><img src="V_Com_Center/Metal/lockers.jpg" alt="lockers" loading="lazy"><figcaption>lockers</figcaption></figure>
<figure><img src="V_Com_Center/Metal/lockers_02.jpg" alt="lockers_02" loading="lazy"><figcaption>lockers_02</figcaption></figure>

### Misc  
<figure><img src="V_Com_Center/Misc/BlueLight_01.jpg" alt="BlueLight_01" loading="lazy"><figcaption>BlueLight_01</figcaption></figure>
<figure><img src="V_Com_Center/Misc/ClenWallPhoto_A.jpg" alt="ClenWallPhoto_A" loading="lazy"><figcaption>ClenWallPhoto_A</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Diagram_A.jpg" alt="Diagram_A" loading="lazy"><figcaption>Diagram_A</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Diagram_B.jpg" alt="Diagram_B" loading="lazy"><figcaption>Diagram_B</figcaption></figure>
<figure><img src="V_Com_Center/Misc/DrtyComandBed_A.jpg" alt="DrtyComandBed_A" loading="lazy"><figcaption>DrtyComandBed_A</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Sn_Comm01.jpg" alt="Sn_Comm01" loading="lazy"><figcaption>Sn_Comm01</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Sn_ScurtyBay.jpg" alt="Sn_ScurtyBay" loading="lazy"><figcaption>Sn_ScurtyBay</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Sn_vcmccntr.jpg" alt="Sn_vcmccntr" loading="lazy"><figcaption>Sn_vcmccntr</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Sn_vcmelev.jpg" alt="Sn_vcmelev" loading="lazy"><figcaption>Sn_vcmelev</figcaption></figure>
<figure><img src="V_Com_Center/Misc/Sn_vcmhlab.jpg" alt="Sn_vcmhlab" loading="lazy"><figcaption>Sn_vcmhlab</figcaption></figure>
<figure><img src="V_Com_Center/Misc/VCMDC_Sign_A.jpg" alt="VCMDC_Sign_A" loading="lazy"><figcaption>VCMDC_Sign_A</figcaption></figure>
<figure><img src="V_Com_Center/Misc/vbc_bttn_01.jpg" alt="vbc_bttn_01" loading="lazy"><figcaption>vbc_bttn_01</figcaption></figure>
<figure><img src="V_Com_Center/Misc/vbc_bttn_02.jpg" alt="vbc_bttn_02" loading="lazy"><figcaption>vbc_bttn_02</figcaption></figure>
<figure><img src="V_Com_Center/Misc/vbc_bttn_03.jpg" alt="vbc_bttn_03" loading="lazy"><figcaption>vbc_bttn_03</figcaption></figure>

### Wall_Objects  
<figure><img src="V_Com_Center/Wall_Objects/ClenCmandWall_C.jpg" alt="ClenCmandWall_C" loading="lazy"><figcaption>ClenCmandWall_C</figcaption></figure>
<figure><img src="V_Com_Center/Wall_Objects/ClenCmandWall_E.jpg" alt="ClenCmandWall_E" loading="lazy"><figcaption>ClenCmandWall_E</figcaption></figure>
<figure><img src="V_Com_Center/Wall_Objects/ClenCmandWall_J.jpg" alt="ClenCmandWall_J" loading="lazy"><figcaption>ClenCmandWall_J</figcaption></figure>
<figure><img src="V_Com_Center/Wall_Objects/ClenCmandWall_K.jpg" alt="ClenCmandWall_K" loading="lazy"><figcaption>ClenCmandWall_K</figcaption></figure>
<figure><img src="V_Com_Center/Wall_Objects/ClenCncrtWall_C.jpg" alt="ClenCncrtWall_C" loading="lazy"><figcaption>ClenCncrtWall_C</figcaption></figure>
<figure><img src="V_Com_Center/Wall_Objects/ClenCommPanel_F.jpg" alt="ClenCommPanel_F" loading="lazy"><figcaption>ClenCommPanel_F</figcaption></figure>
<figure><img src="V_Com_Center/Wall_Objects/DrtyCmandWall_B.jpg" alt="DrtyCmandWall_B" loading="lazy"><figcaption>DrtyCmandWall_B</figcaption></figure>## area51textures

### Concrete  
<figure><img src="area51textures/Concrete/A51_Sign_36.jpg" alt="A51_Sign_36" loading="lazy"><figcaption>A51_Sign_36</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Sign_37.jpg" alt="A51_Sign_37" loading="lazy"><figcaption>A51_Sign_37</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Sign_38.jpg" alt="A51_Sign_38" loading="lazy"><figcaption>A51_Sign_38</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_03.jpg" alt="A51_Wall_03" loading="lazy"><figcaption>A51_Wall_03</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_05.jpg" alt="A51_Wall_05" loading="lazy"><figcaption>A51_Wall_05</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_12.jpg" alt="A51_Wall_12" loading="lazy"><figcaption>A51_Wall_12</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_13.jpg" alt="A51_Wall_13" loading="lazy"><figcaption>A51_Wall_13</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_14.jpg" alt="A51_Wall_14" loading="lazy"><figcaption>A51_Wall_14</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_15.jpg" alt="A51_Wall_15" loading="lazy"><figcaption>A51_Wall_15</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_17.jpg" alt="A51_Wall_17" loading="lazy"><figcaption>A51_Wall_17</figcaption></figure>
<figure><img src="area51textures/Concrete/A51_Wall_18.jpg" alt="A51_Wall_18" loading="lazy"><figcaption>A51_Wall_18</figcaption></figure>

### Glass  
<figure><img src="area51textures/Glass/A51_Light_02.jpg" alt="A51_Light_02" loading="lazy"><figcaption>A51_Light_02</figcaption></figure>
<figure><img src="area51textures/Glass/A51_Light_03.jpg" alt="A51_Light_03" loading="lazy"><figcaption>A51_Light_03</figcaption></figure>
<figure><img src="area51textures/Glass/A51_Light_04.jpg" alt="A51_Light_04" loading="lazy"><figcaption>A51_Light_04</figcaption></figure>

### Ladder  
<figure><img src="area51textures/Ladder/Area51lddr_a.jpg" alt="Area51lddr_a" loading="lazy"><figcaption>Area51lddr_a</figcaption></figure>

### Metal  
<figure><img src="area51textures/Metal/A51_Comptr_01.jpg" alt="A51_Comptr_01" loading="lazy"><figcaption>A51_Comptr_01</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Comptr_02.jpg" alt="A51_Comptr_02" loading="lazy"><figcaption>A51_Comptr_02</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Door_01.jpg" alt="A51_Door_01" loading="lazy"><figcaption>A51_Door_01</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Floor_01.jpg" alt="A51_Floor_01" loading="lazy"><figcaption>A51_Floor_01</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_03.jpg" alt="A51_Sign_03" loading="lazy"><figcaption>A51_Sign_03</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_04.jpg" alt="A51_Sign_04" loading="lazy"><figcaption>A51_Sign_04</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_05.jpg" alt="A51_Sign_05" loading="lazy"><figcaption>A51_Sign_05</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_06.jpg" alt="A51_Sign_06" loading="lazy"><figcaption>A51_Sign_06</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_07.jpg" alt="A51_Sign_07" loading="lazy"><figcaption>A51_Sign_07</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_08.jpg" alt="A51_Sign_08" loading="lazy"><figcaption>A51_Sign_08</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_09.jpg" alt="A51_Sign_09" loading="lazy"><figcaption>A51_Sign_09</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_10.jpg" alt="A51_Sign_10" loading="lazy"><figcaption>A51_Sign_10</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_11.jpg" alt="A51_Sign_11" loading="lazy"><figcaption>A51_Sign_11</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_13.jpg" alt="A51_Sign_13" loading="lazy"><figcaption>A51_Sign_13</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_14.jpg" alt="A51_Sign_14" loading="lazy"><figcaption>A51_Sign_14</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_16.jpg" alt="A51_Sign_16" loading="lazy"><figcaption>A51_Sign_16</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_17.jpg" alt="A51_Sign_17" loading="lazy"><figcaption>A51_Sign_17</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_18.jpg" alt="A51_Sign_18" loading="lazy"><figcaption>A51_Sign_18</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_19.jpg" alt="A51_Sign_19" loading="lazy"><figcaption>A51_Sign_19</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_20.jpg" alt="A51_Sign_20" loading="lazy"><figcaption>A51_Sign_20</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_21.jpg" alt="A51_Sign_21" loading="lazy"><figcaption>A51_Sign_21</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_22.jpg" alt="A51_Sign_22" loading="lazy"><figcaption>A51_Sign_22</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_22A.jpg" alt="A51_Sign_22A" loading="lazy"><figcaption>A51_Sign_22A</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_23.jpg" alt="A51_Sign_23" loading="lazy"><figcaption>A51_Sign_23</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_24.jpg" alt="A51_Sign_24" loading="lazy"><figcaption>A51_Sign_24</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_25.jpg" alt="A51_Sign_25" loading="lazy"><figcaption>A51_Sign_25</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_26.jpg" alt="A51_Sign_26" loading="lazy"><figcaption>A51_Sign_26</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_28.jpg" alt="A51_Sign_28" loading="lazy"><figcaption>A51_Sign_28</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_29.jpg" alt="A51_Sign_29" loading="lazy"><figcaption>A51_Sign_29</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_30.jpg" alt="A51_Sign_30" loading="lazy"><figcaption>A51_Sign_30</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_32.jpg" alt="A51_Sign_32" loading="lazy"><figcaption>A51_Sign_32</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_39.jpg" alt="A51_Sign_39" loading="lazy"><figcaption>A51_Sign_39</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_40.jpg" alt="A51_Sign_40" loading="lazy"><figcaption>A51_Sign_40</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_41.jpg" alt="A51_Sign_41" loading="lazy"><figcaption>A51_Sign_41</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_42.jpg" alt="A51_Sign_42" loading="lazy"><figcaption>A51_Sign_42</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_43.jpg" alt="A51_Sign_43" loading="lazy"><figcaption>A51_Sign_43</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_44.jpg" alt="A51_Sign_44" loading="lazy"><figcaption>A51_Sign_44</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Sign_45.jpg" alt="A51_Sign_45" loading="lazy"><figcaption>A51_Sign_45</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Tube_01.jpg" alt="A51_Tube_01" loading="lazy"><figcaption>A51_Tube_01</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Wall_01.jpg" alt="A51_Wall_01" loading="lazy"><figcaption>A51_Wall_01</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Wall_04.jpg" alt="A51_Wall_04" loading="lazy"><figcaption>A51_Wall_04</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Wall_06.jpg" alt="A51_Wall_06" loading="lazy"><figcaption>A51_Wall_06</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Wall_07.jpg" alt="A51_Wall_07" loading="lazy"><figcaption>A51_Wall_07</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Wall_08.jpg" alt="A51_Wall_08" loading="lazy"><figcaption>A51_Wall_08</figcaption></figure>
<figure><img src="area51textures/Metal/A51_Wall_11.jpg" alt="A51_Wall_11" loading="lazy"><figcaption>A51_Wall_11</figcaption></figure>
<figure><img src="area51textures/Metal/A51_detail_01.jpg" alt="A51_detail_01" loading="lazy"><figcaption>A51_detail_01</figcaption></figure>
<figure><img src="area51textures/Metal/A51_detail_02.jpg" alt="A51_detail_02" loading="lazy"><figcaption>A51_detail_02</figcaption></figure>
<figure><img src="area51textures/Metal/A51_detail_03.jpg" alt="A51_detail_03" loading="lazy"><figcaption>A51_detail_03</figcaption></figure>
<figure><img src="area51textures/Metal/Area51Wall_A.jpg" alt="Area51Wall_A" loading="lazy"><figcaption>Area51Wall_A</figcaption></figure>
<figure><img src="area51textures/Metal/BlastDoors_A.jpg" alt="BlastDoors_A" loading="lazy"><figcaption>BlastDoors_A</figcaption></figure>
<figure><img src="area51textures/Metal/BlastDoors_B.jpg" alt="BlastDoors_B" loading="lazy"><figcaption>BlastDoors_B</figcaption></figure>
<figure><img src="area51textures/Metal/HELIOS_05.jpg" alt="HELIOS_05" loading="lazy"><figcaption>HELIOS_05</figcaption></figure>
<figure><img src="area51textures/Metal/HELIOS_06.jpg" alt="HELIOS_06" loading="lazy"><figcaption>HELIOS_06</figcaption></figure>
<figure><img src="area51textures/Metal/HELIOS_07.jpg" alt="HELIOS_07" loading="lazy"><figcaption>HELIOS_07</figcaption></figure>
<figure><img src="area51textures/Metal/area51bldng_a.jpg" alt="area51bldng_a" loading="lazy"><figcaption>area51bldng_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51bldng_c.jpg" alt="area51bldng_c" loading="lazy"><figcaption>area51bldng_c</figcaption></figure>
<figure><img src="area51textures/Metal/area51bldng_d.jpg" alt="area51bldng_d" loading="lazy"><figcaption>area51bldng_d</figcaption></figure>
<figure><img src="area51textures/Metal/area51catwalk_a.jpg" alt="area51catwalk_a" loading="lazy"><figcaption>area51catwalk_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51catwalk_b.jpg" alt="area51catwalk_b" loading="lazy"><figcaption>area51catwalk_b</figcaption></figure>
<figure><img src="area51textures/Metal/area51catwalk_c.jpg" alt="area51catwalk_c" loading="lazy"><figcaption>area51catwalk_c</figcaption></figure>
<figure><img src="area51textures/Metal/area51catwalk_d.jpg" alt="area51catwalk_d" loading="lazy"><figcaption>area51catwalk_d</figcaption></figure>
<figure><img src="area51textures/Metal/area51catwalk_f.jpg" alt="area51catwalk_f" loading="lazy"><figcaption>area51catwalk_f</figcaption></figure>
<figure><img src="area51textures/Metal/area51clnder_a.jpg" alt="area51clnder_a" loading="lazy"><figcaption>area51clnder_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51clnder_b.jpg" alt="area51clnder_b" loading="lazy"><figcaption>area51clnder_b</figcaption></figure>
<figure><img src="area51textures/Metal/area51doorway_a.jpg" alt="area51doorway_a" loading="lazy"><figcaption>area51doorway_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51gear_a.jpg" alt="area51gear_a" loading="lazy"><figcaption>area51gear_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51hllway_a.jpg" alt="area51hllway_a" loading="lazy"><figcaption>area51hllway_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51nkewste_c.jpg" alt="area51nkewste_c" loading="lazy"><figcaption>area51nkewste_c</figcaption></figure>
<figure><img src="area51textures/Metal/area51nkewste_d.jpg" alt="area51nkewste_d" loading="lazy"><figcaption>area51nkewste_d</figcaption></figure>
<figure><img src="area51textures/Metal/area51nkewste_f.jpg" alt="area51nkewste_f" loading="lazy"><figcaption>area51nkewste_f</figcaption></figure>
<figure><img src="area51textures/Metal/area51pipes_a.jpg" alt="area51pipes_a" loading="lazy"><figcaption>area51pipes_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51pipes_b.jpg" alt="area51pipes_b" loading="lazy"><figcaption>area51pipes_b</figcaption></figure>
<figure><img src="area51textures/Metal/area51shere_a.jpg" alt="area51shere_a" loading="lazy"><figcaption>area51shere_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51shere_b.jpg" alt="area51shere_b" loading="lazy"><figcaption>area51shere_b</figcaption></figure>
<figure><img src="area51textures/Metal/area51shere_c.jpg" alt="area51shere_c" loading="lazy"><figcaption>area51shere_c</figcaption></figure>
<figure><img src="area51textures/Metal/area51tank_a.jpg" alt="area51tank_a" loading="lazy"><figcaption>area51tank_a</figcaption></figure>
<figure><img src="area51textures/Metal/area51wall_b.jpg" alt="area51wall_b" loading="lazy"><figcaption>area51wall_b</figcaption></figure>
<figure><img src="area51textures/Metal/area51wall_c.jpg" alt="area51wall_c" loading="lazy"><figcaption>area51wall_c</figcaption></figure>
<figure><img src="area51textures/Metal/area51wall_d.jpg" alt="area51wall_d" loading="lazy"><figcaption>area51wall_d</figcaption></figure>
<figure><img src="area51textures/Metal/pa_nukewste_a.jpg" alt="pa_nukewste_a" loading="lazy"><figcaption>pa_nukewste_a</figcaption></figure>

### Misc  
<figure><img src="area51textures/Misc/A51_Light_01.jpg" alt="A51_Light_01" loading="lazy"><figcaption>A51_Light_01</figcaption></figure>

### Stone  
<figure><img src="area51textures/Stone/GroomLakeSign_C.jpg" alt="GroomLakeSign_C" loading="lazy"><figcaption>GroomLakeSign_C</figcaption></figure>

### animating  
<figure><img src="area51textures/animating/Area51dmint_A01.jpg" alt="Area51dmint_A01" loading="lazy"><figcaption>Area51dmint_A01</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmint_A02.jpg" alt="Area51dmint_A02" loading="lazy"><figcaption>Area51dmint_A02</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmint_A03.jpg" alt="Area51dmint_A03" loading="lazy"><figcaption>Area51dmint_A03</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmint_A04.jpg" alt="Area51dmint_A04" loading="lazy"><figcaption>Area51dmint_A04</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmpol_A01.jpg" alt="Area51dmpol_A01" loading="lazy"><figcaption>Area51dmpol_A01</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmpol_A02.jpg" alt="Area51dmpol_A02" loading="lazy"><figcaption>Area51dmpol_A02</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmpol_A03.jpg" alt="Area51dmpol_A03" loading="lazy"><figcaption>Area51dmpol_A03</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmpol_A04.jpg" alt="Area51dmpol_A04" loading="lazy"><figcaption>Area51dmpol_A04</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmtop_a.jpg" alt="Area51dmtop_a" loading="lazy"><figcaption>Area51dmtop_a</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmwll_A01.jpg" alt="Area51dmwll_A01" loading="lazy"><figcaption>Area51dmwll_A01</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmwll_A02.jpg" alt="Area51dmwll_A02" loading="lazy"><figcaption>Area51dmwll_A02</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmwll_A03.jpg" alt="Area51dmwll_A03" loading="lazy"><figcaption>Area51dmwll_A03</figcaption></figure>
<figure><img src="area51textures/animating/Area51dmwll_A04.jpg" alt="Area51dmwll_A04" loading="lazy"><figcaption>Area51dmwll_A04</figcaption></figure>


## subway

### Ceramic  
<figure><img src="subway/Ceramic/Nsub_wall_a.jpg" alt="Nsub_wall_a" loading="lazy"><figcaption>Nsub_wall_a</figcaption></figure>
<figure><img src="subway/Ceramic/Nsubwaltop.jpg" alt="Nsubwaltop" loading="lazy"><figcaption>Nsubwaltop</figcaption></figure>
<figure><img src="subway/Ceramic/subwaledg1.jpg" alt="subwaledg1" loading="lazy"><figcaption>subwaledg1</figcaption></figure>
<figure><img src="subway/Ceramic/subwaledg2.jpg" alt="subwaledg2" loading="lazy"><figcaption>subwaledg2</figcaption></figure>

### Concrete  
<figure><img src="subway/Concrete/sub_beam_a.jpg" alt="sub_beam_a" loading="lazy"><figcaption>sub_beam_a</figcaption></figure>
<figure><img src="subway/Concrete/sub_beam_b.jpg" alt="sub_beam_b" loading="lazy"><figcaption>sub_beam_b</figcaption></figure>
<figure><img src="subway/Concrete/subcieling1.jpg" alt="subcieling1" loading="lazy"><figcaption>subcieling1</figcaption></figure>
<figure><img src="subway/Concrete/subfloor1.jpg" alt="subfloor1" loading="lazy"><figcaption>subfloor1</figcaption></figure>
